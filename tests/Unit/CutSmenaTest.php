<?php

namespace App\Models;

use App\Http\Controllers\Import\CutShift;
use Illuminate\Support\Facades\File;
use Tests\TestCase;
use Jimigrunge\InvokePrivateMethods\InvokePrivateMethod;

class CutSmenaTest extends TestCase
{
    protected $data = [];
    protected $data_filtering = [];
    protected $cutSmena = null;

    protected function setUp(): void
    {
        parent::setUp();

        $file = File::get( base_path('tests/Unit/json/aps_tmp.json'), true );
        $this->data = json_decode($file);

        $this->cutSmena = new CutShift();
        $this->cutSmena->set($this->data);
        $this->data_filtering = $this->cutSmena->get();
    }

    public function testFilter()
    {
        $this->assertCount(10067, $this->data);
        $result = InvokePrivateMethod::invoke($this->cutSmena, 'filter', [$this->data]);
        $this->assertCount(4368, $result);
    }

    public function testCutByShift()
    {
        $this->assertCount(10067, $this->data);
        $result = InvokePrivateMethod::invoke($this->cutSmena, 'cutByShift', [$this->data]);
        $result = $this->cutSmena->get();
        $this->assertCount(12263, $result);
//        $this->assertCount(7807, $result);
    }

    public function testIsCrossesShift()
    {
        // начало первой смены в 7:30, начало второй смены в 19:30
        $start = '2020-01-11 07:30';
        $finish = '2020-01-11 21:48';
        $result = InvokePrivateMethod::invoke($this->cutSmena, 'isCrossesShift', [$start, $finish]);
//        echo $result . PHP_EOL;
        $this->assertTrue( $result == 1, sprintf('15 Ошибка %s - %s', $start, $finish) );
//exit();
        $start = '2020-01-10 05:56';
        $finish = '2020-01-11 01:52';
        $result = InvokePrivateMethod::invoke($this->cutSmena, 'isCrossesShift', [$start, $finish]);
//        echo $result . PHP_EOL;
        $this->assertTrue( $result == 2, sprintf('14 Ошибка %s - %s', $start, $finish) );

        $start = '2020-01-14 00:58';
        $finish = '2020-01-14 12:16';
        $result = InvokePrivateMethod::invoke($this->cutSmena, 'isCrossesShift', [$start, $finish]);
//        echo $result . PHP_EOL;
        $this->assertTrue( $result == 2, sprintf('13 Ошибка %s - %s', $start, $finish) );

        $start = '2020-01-04 19:30';
        $finish = '2020-01-05 19:30';
        $result = InvokePrivateMethod::invoke($this->cutSmena, 'isCrossesShift', [$start, $finish]);
//        echo $result . PHP_EOL;
        $this->assertTrue( $result == 2, sprintf('1 Ошибка %s - %s', $start, $finish) );

        $start = '2020-01-04 07:30';
        $finish = '2020-01-05 19:30';
        $result = InvokePrivateMethod::invoke($this->cutSmena, 'isCrossesShift', [$start, $finish]);
//        echo $result . PHP_EOL;
        $this->assertTrue( $result == 1, sprintf('2 Ошибка %s - %s', $start, $finish) );

        $start = '2019-12-01 15:30';
        $finish = '2019-12-01 20:30';
        $result = InvokePrivateMethod::invoke($this->cutSmena, 'isCrossesShift', [$start, $finish]);
//        echo $result . PHP_EOL;
        $this->assertTrue( $result == 1, sprintf('3 Ошибка %s - %s', $start, $finish) );

        $start = '2019-12-01 15:30';
        $finish = '2019-12-01 19:30';
        $result = InvokePrivateMethod::invoke($this->cutSmena, 'isCrossesShift', [$start, $finish]);
//        echo $result . PHP_EOL;
        $this->assertFalse( $result, sprintf('4 Ошибка %s - %s', $start, $finish) );

        $start = '2019-12-01 15:30';
        $finish = '2019-12-02 19:30';
        $result = InvokePrivateMethod::invoke($this->cutSmena, 'isCrossesShift', [$start, $finish]);
//        echo $result . PHP_EOL;
        $this->assertTrue( $result == 1, sprintf('5 Ошибка %s - %s', $start, $finish) );

        $start = '2019-12-01 07:45';
        $finish = '2019-12-01 11:30';
        $result = InvokePrivateMethod::invoke($this->cutSmena, 'isCrossesShift', [$start, $finish]);
//        echo $result . PHP_EOL;
        $this->assertFalse( $result, sprintf('6 Ошибка %s - %s', $start, $finish) );

        $start = '2019-12-01 19:30';
        $finish = '2019-12-02 02:30';
        $result = InvokePrivateMethod::invoke($this->cutSmena, 'isCrossesShift', [$start, $finish]);
//        echo $result . PHP_EOL;
        $this->assertFalse( $result, sprintf('7 Ошибка %s - %s', $start, $finish) );

        $start = '2019-12-01 20:45';
        $finish = '2019-12-02 07:45';
        $result = InvokePrivateMethod::invoke($this->cutSmena, 'isCrossesShift', [$start, $finish]);
//        echo $result . PHP_EOL;
        $this->assertTrue( $result == 2, sprintf('8 Ошибка %s - %s', $start, $finish) );

        $start = '2020-01-04 17:02';
        $finish = '2020-01-05 02:02';
        $result = InvokePrivateMethod::invoke($this->cutSmena, 'isCrossesShift', [$start, $finish]);
//        echo $result . PHP_EOL;
        $this->assertTrue( $result == 1, sprintf('9 Ошибка %s - %s', $start, $finish) );

        $start = '2020-01-04 20:00';
        $finish = '2020-01-05 02:02';
        $result = InvokePrivateMethod::invoke($this->cutSmena, 'isCrossesShift', [$start, $finish]);
//        echo $result . PHP_EOL;
        $this->assertFalse( $result, sprintf('10 Ошибка %s - %s', $start, $finish) );

        $start = '2020-01-04 20:00';
        $finish = '2020-01-05 09:00';
        $result = InvokePrivateMethod::invoke($this->cutSmena, 'isCrossesShift', [$start, $finish]);
//        echo $result . PHP_EOL;
        $this->assertTrue( $result == 2, sprintf('11 Ошибка %s - %s', $start, $finish) );

        $start = '2020-01-13 22:13';
        $finish = '2020-01-13 22:43';
        $result = InvokePrivateMethod::invoke($this->cutSmena, 'isCrossesShift', [$start, $finish]);
//        echo $result . PHP_EOL;
        $this->assertFalse( $result, sprintf('12 Ошибка %s - %s', $start, $finish) );
    }

    public function testCutPeriod()
    {
        $data3 = [
            'ALLOCDATE' => ' 2019-12-23 21:41',
            'INTERVAL_ID' => 1,
            'DATETIME_START' => '2019-11-23 21:41',
            'DATETIME_FINISH' => '2019-11-24 08:59',
            'DIF_M' => 418,
            'DIF_H' => 6.58,
            'KOL_PLAN' => 1.914,
            'KOL_FACT' => 1.189,
            'RESTQT' => 1.2836,
            'RESTQT_S' => 1.2836,
            'BALANCE' => 0,
            'UNAPSV' => 620,
            'SETUPTIME' => 0,
        ];
        $assetResult = false;
        $result = InvokePrivateMethod::invoke($this->cutSmena, 'cutPeriod', [$data3, '2019-11-24 18:00', 15, true]);
//        prn($result);
        if (   $result[0]['DATETIME_START'] == '2019-11-23 21:41' && $result[0]['DATETIME_FINISH'] == '2019-11-24 08:59'
            && $result[0]['DIF_M'] == 418 && $result[0]['DIF_H'] == 6.58
            && $result[0]['KOL_PLAN'] == 1.914 //&& $result[0]['KOL_FACT'] ==1.189
        ) {
            $assetResult = true;
        }
        $this->assertTrue($assetResult);

        $data1 = [
            'ALLOCDATE' => ' 2020-01-04 08:02',
            'INTERVAL_ID' => 1,
            'DATETIME_START' => '2020-01-04 08:02',
            'DATETIME_FINISH' => '2020-01-04 15:00',
            'DIF_M' => 418,
            'DIF_H' => 6.58,
            'KOL_PLAN' => 1.914,
            'KOL_FACT' => 1.189,
            'RESTQT' => 1.2836,
            'RESTQT_S' => 1.2836,
            'BALANCE' => 0,
            'UNAPSV' => 620,
            'SETUPTIME' => 0,
        ];
        $assetResult = false;
        $result = InvokePrivateMethod::invoke($this->cutSmena, 'cutPeriod', [$data1, '2020-01-04 15:00']);
//        prn($result[0], true);
        if (   $result[0]['DATETIME_START'] == '2020-01-04 08:02' && $result[0]['DATETIME_FINISH'] == '2020-01-04 15:00'
            && $result[0]['DIF_M'] == 418 && $result[0]['DIF_H'] == 6.58
            && $result[0]['KOL_PLAN'] == 1.914 //&& $result[0]['KOL_FACT'] == 1.189
        ) { $assetResult = true; }
        $this->assertTrue($assetResult);


        $data2 = [
            'ALLOCDATE' => ' 2020-01-04 08:02',
            'INTERVAL_ID' => 1,
            'DATETIME_START' => '2020-01-04 17:00',
            'DATETIME_FINISH' => '2020-01-05 02:00',
            'DIF_M' => 540,
            'DIF_H' => 9,
            'KOL_PLAN' => 2.4726,
            'KOL_FACT' => 1.2,
            'RESTQT' => 1.2836,
            'RESTQT_S' => 1.2836,
            'BALANCE' => 0,
            'UNAPSV' => 620,
            'SETUPTIME' => 0,
        ];
        $assetResult = false;
        $result = InvokePrivateMethod::invoke($this->cutSmena, 'cutPeriod', [$data2, '2020-01-04 19:00', 15, true]);
//        prn($result);
        if (   $result[0]['DATETIME_START'] == '2020-01-04 17:00' && $result[0]['DATETIME_FINISH'] == '2020-01-04 19:00' && $result[0]['ID_CUT'] = 1578157200
            && $result[0]['DIF_M'] == 120 && $result[0]['DIF_H'] == 2
            && $result[0]['KOL_PLAN'] == 0.5495 //&& $result[0]['KOL_FACT'] == 1.2

            && $result[1]['DATETIME_START'] == '2020-01-04 19:00' && $result[1]['DATETIME_FINISH'] == '2020-01-05 02:00' && $result[1]['ID_CUT'] = 1578157200
            && $result[1]['DIF_M'] == 420 && $result[1]['DIF_H'] == 7
            && $result[1]['KOL_PLAN'] == 1.9231 //&& $result[1]['KOL_FACT'] == 0
        ) { $assetResult = true; }
        $this->assertTrue($assetResult);
    }

    public function testSetTimeStamp()
    {
        $result = InvokePrivateMethod::invoke($this->cutSmena, 'setTimeStamp', [null]);
        $this->assertFalse($result);

        $result = InvokePrivateMethod::invoke($this->cutSmena, 'setTimeStamp', ['']);
        $this->assertFalse($result);

        $result = InvokePrivateMethod::invoke($this->cutSmena, 'setTimeStamp', [124]);
        $this->assertFalse($result);

        $result = InvokePrivateMethod::invoke($this->cutSmena, 'setTimeStamp', [1514764800]);
        $this->assertTrue(1514764800 == $result);

        $result = InvokePrivateMethod::invoke($this->cutSmena, 'setTimeStamp', ['2017-12-08']);
        $this->assertFalse($result >= 1514764801 );

        $result = InvokePrivateMethod::invoke($this->cutSmena, 'setTimeStamp', ['2018-12-08']);
        $this->assertTrue($result >= 1514764800 );

        $result = InvokePrivateMethod::invoke($this->cutSmena, 'setTimeStamp', ['2020-12-08']);
        $this->assertTrue($result >= 1514764800 );
    }

    public function testSavePlanInfo()
    {
        //TODO: сделать тестирования записи данных в БД mongo
        $this->assertTrue(true);
    }

    public function testCutByShiftRecursive()
    {
        $data = [
            'ALLOCDATE' => '2020-01-04 08:02',
            'INTERVAL_ID' => 1,
            'DATETIME_START' => '2020-01-04 17:02',
            'DATETIME_FINISH' => '2020-01-05 19:30',
            'DIF_M' => 1588,
            'DIF_H' => 26.28,
            'KOL_PLAN' => 2.4726,
            'KOL_FACT' => 1.4,
            'RESTQT' => 2.4726,
            'RESTQT_S' => 2.4726,
            'BALANCE' => 0,
            'UNAPSV' => 620,
            'SETUPTIME' => 620,
        ];

        $assetResult = false;

        $result = InvokePrivateMethod::invoke($this->cutSmena, 'cutByShiftRecursive', [$data]);
//        prn($result[0]);
        if (   $result[0]['DATETIME_START'] == '2020-01-04 17:02' && $result[0]['DATETIME_FINISH'] == '2020-01-04 19:30'
            && $result[0]['DIF_M'] == 148 && $result[0]['DIF_H'] == 2.28
            && $result[0]['KOL_PLAN'] == 0.2304 //&& $result[0]['KOL_FACT'] == 1.4
            ) {
            $assetResult = true;
        }
        $this->assertTrue($assetResult, 'return 0');

        $assetResult = false;
        if (   $result[1]['DATETIME_START'] == '2020-01-04 19:30' && $result[1]['DATETIME_FINISH'] == '2020-01-05 07:30'
            && $result[1]['DIF_M'] == 720 && $result[1]['DIF_H'] == 12
            && $result[1]['KOL_PLAN'] == 1.1211 //&& $result[1]['KOL_FACT'] == 0
            ) {
            $assetResult = true;
        }
        $this->assertTrue($assetResult, 'result 1');

        $assetResult = false;
        if (   $result[2]['DATETIME_START'] == '2020-01-05 07:30' && $result[2]['DATETIME_FINISH'] == '2020-01-05 19:30'
            && $result[2]['DIF_M'] == 720 && $result[2]['DIF_H'] == 12
            && $result[2]['KOL_PLAN'] == 1.1211 //&& $result[2]['KOL_FACT'] == 0
        ) {
            $assetResult = true;
        }
        $this->assertTrue($assetResult, 'result 2');
    }

    public function testCutByTimeStamp()
    {
        $this->cutSmena->trim();
        $result = InvokePrivateMethod::invoke($this->cutSmena, 'cutByTimeStamp', ['2016-01-01']);
        $this->assertFalse($result);

        $assetResult = false;
        InvokePrivateMethod::invoke($this->cutSmena, 'cutByTimeStamp', ['2020-01-04 18:00']);
        $result = $this->cutSmena->get();

//        prn($result[3775], true);
        if (   $result[3775]['DATETIME_START'] == '2020-01-04 17:40' && $result[3775]['DATETIME_FINISH'] == '2020-01-04 18:00'
            && $result[3775]['DIF_M'] == 20 && $result[3775]['DIF_H'] == 0.2 && $result[3775]['KOL_PLAN'] == 0.065) {
            $assetResult = true;
        }
        $this->assertTrue($assetResult);

//        prn($result[3605], true);
        if (   $result[4043]['DATETIME_START'] == '2020-01-04 17:40' && $result[4043]['DATETIME_FINISH'] == '2020-01-04 18:00'
            && $result[4043]['DIF_M'] == 20 && $result[4043]['DIF_H'] == 0.20 && $result[4043]['KOL_PLAN'] == 0.0634
            ) {
            $assetResult = true;
        }
        $this->assertTrue($assetResult);
    }
}
