using ITnet2.Server.Session;
using ITnet2.Server.Dialogs;

using ITnet2.Server.Data;
using ITnet2.Server.UserBusinessLogic._Load_To_Db;

public class Fact
{
    public static object getFactByMonth(string prod_id, string month, string year)
    {
        string startDate = string.Format("{0}-{1}-01", year, month);
        string endDate = string.Format("{0}-{1}-{2}", year, month
            , System.DateTime.DaysInMonth(int.Parse(year), int.Parse(month))
        );

        string tmpTableTech = Load_To_Db.getTech(DbInstance.Main);

        string sqlGetFact = @"
            SELECT
                '_SKC' AS KDMT,
                FACT.*,
                ITKSMFAS.TECH AS TECH
            FROM (
                SELECT
                    DMS.KMAT,
                    FORMAT(DMS.DDM, 'yy-MM-dd') AS DDM,
                    SUM(DMS.KOL) AS KOL,
                    KSM.NMAT,
                    PROD.SKM AS PROD_ID,
                    PROD.SKMNAIM AS PROD_NAME,
                    SKM.FGOST AS GOST,
                    SKM.FNMAT AS TECH_STR

                FROM DMS
                LEFT JOIN KSM ON KSM.KMAT = DMS.KMAT
                LEFT JOIN SKM ON SKM.SKM = KSM.SKM
                LEFT JOIN SKM PROD ON PROD.SKM = SKM.SKM_PR


                WHERE PROD.SKM = @_PROD_ID AND DMS.DDM BETWEEN CONVERT(DATE, @_STARTDATE) AND CONVERT(DATE, @_ENDDATE) AND DMS.KDMT = '_SKC'
                GROUP BY DMS.DDM, DMS.KMAT, PROD.SKM, PROD.SKMNAIM, SKM.FGOST, SKM.FNMAT, KSM.NMAT
            ) AS FACT
            LEFT JOIN " + tmpTableTech + @" AS ITKSMFAS ON ITKSMFAS.KMAT = FACT.KMAT

            ORDER BY FACT.KMAT, FACT.DDM
        ";

        var cmdGetFact = SqlClient.CreateCommand(sqlGetFact);
        cmdGetFact.Parameters["_PROD_ID"].Value = prod_id;
        cmdGetFact.Parameters["_STARTDATE"].Value = startDate;
        cmdGetFact.Parameters["_ENDDATE"].Value = endDate;

        string tmpTable = Load_To_Db.LoadToDbEx(cmdGetFact, DbInstance.Main);

        string sqlGetOrders = @"
            SELECT
                '_S3C' AS KDMT,
                DMS.*,
                ITKSMFAS.TECH AS TECH

            FROM (
                SELECT
                    SUM(DMS.KOL) AS KOL,
                    NULL AS DDM,
                    DMS.KMAT,
                    KSM.NMAT,
                    PROD.SKM AS PROD_ID,
                    PROD.SKMNAIM AS PROD_NAME,
                    SKM.FGOST AS GOST,
                    SKM.FNMAT AS TECH_STR,
                    RSS.NRSS_S

                FROM DMZ
                LEFT JOIN DMS ON DMS.UNDOC = DMZ.UNDOC
                LEFT JOIN KSM ON KSM.KMAT = DMS.KMAT
                LEFT JOIN SKM ON SKM.SKM = KSM.SKM
                LEFT JOIN SKM PROD ON PROD.SKM = SKM.SKM_PR
                LEFT JOIN RS1 ON RS1.ARSO = 'DMZ10' AND RS1.KEYVALUE = DMS.UNDOC
                LEFT JOIN RSS ON RSS.ARSO = 'DMZ10' AND RSS.ARSV = RS1.ARSV AND RSS.ARSS = RS1.ARSSCURR AND RSS.PR_DO=' '

                WHERE DMZ.DDM BETWEEN CONVERT(DATE, @_STARTDATE) AND CONVERT(DATE, @_ENDDATE) AND DMS.KDMT = '_S3C' AND PROD.SKM = @_PROD_ID
                    AND TRIM(RSS.NRSS_S) <> 'Регистрац.' AND TRIM(RSS.NRSS_S) <> 'Отмена '
                GROUP BY DMS.KMAT, KSM.NMAT, PROD.SKM, PROD.SKMNAIM, SKM.FGOST, SKM.FNMAT, RSS.NRSS_S
            ) AS DMS
            LEFT JOIN " + tmpTableTech + " AS ITKSMFAS ON ITKSMFAS.KMAT = DMS.KMAT";

        var cmdGetOrders = SqlClient.CreateCommand(sqlGetOrders);
        cmdGetOrders.Parameters["_PROD_ID"].Value = prod_id;
        cmdGetOrders.Parameters["_STARTDATE"].Value = startDate;
        cmdGetOrders.Parameters["_ENDDATE"].Value = endDate;

        Load_To_Db.addDataToTable(DbInstance.Main, tmpTable, cmdGetOrders);

        var cmdGetData = SqlClient.CreateCommand("SELECT * FROM " + tmpTable);

        //InfoManager.MessageBox(sqlGetFact); return null;
        //ITnet2.Server.Dialogs.TableViewer.Show(cmdGetOrders);

        var result = cmdGetData.LoadToLocalDb();

        SqlClient.Main.DropTempTable(tmpTableTech);
        SqlClient.Main.DropTempTable(tmpTable);

        //return null;
        return result;
    }
}