SELECT
	SUM(REG.KOL_F) AS KOL,
	SUM(REG.KOL_B) AS KOL_B,
	--REG.KPRT,
	--REG.KOL_F AS KOL,
	--REG.KOL_B AS KOL_B,
	PRT_EXT.PR_OTKL,
	PRT_EXT.PR_NOTKL,
	CASE WHEN PRT_EXT.PR_OTKL = 0 AND PRT_EXT.PR_NOTKL = 1 THEN 1 ELSE 0 END AS GODNAY,
	MKTS.NTECHPR,
	MKT.KMAT,
	KSM.N_RES,
	TTOP.NTTOP,
	--MKT.MKN,
	--MKT.MKD,
	REG.RCENTR_F, PRCR.NAME
FROM MKTSREG REG
LEFT JOIN PRCR ON PRCR.RCENTR = REG.RCENTR_F
LEFT JOIN PRT ON PRT.KPRT = REG.KPRT
LEFT JOIN PRT_EXT ON PRT_EXT.KPRT = PRT.KPRT
LEFT JOIN MKTS ON MKTS.UNMKTS = REG.UNMKTS
LEFT JOIN TTOP ON TTOP.KTTOP = MKTS.KTTOP
LEFT JOIN MKT ON MKT.UNMKT = MKTS.UNMKT
LEFT JOIN KSM KSM ON KSM.KMAT = MKT.KMAT
WHERE REG.REGTYPE = 1 AND REG.TIMEOKON_F BETWEEN CONVERT(DATE, '01-11-2019') AND CONVERT(DATE, '30-11-2019')
	--AND KSM.N_RES = 'Проволока 6.0 мм   ГОСТ 9389 Б - 2' AND PRCR.NAME = 'в/ст LZ-1200 №31'
GROUP BY REG.RCENTR_F, PRCR.NAME, MKT.KMAT, MKTS.NTECHPR, TTOP.NTTOP, KSM.N_RES --, MKT.MKN, MKT.MKD, REG.KPRT
	, PRT_EXT.PR_OTKL,
	 PRT_EXT.PR_NOTKL