using ITnet2.Server.Data;
using ITnet2.Server.UserBusinessLogic._Load_To_Db;
using ITnet2.Common.Tools;
using System.Collections.Generic;

public class Orders
{
    public static object getOrders(string prod_id, string month, string year)
    {
         // ЗАДАЕМ ВСЕ НУЖНЫЕ ЗНАЧЕНИЯ ДЛЯ ОТБОРА
        string startDate = string.Format("{0}-{1}-01", year, month); // НАЧАЛО
        string endDate = string.Format("{0}-{1}-{2}", year, month // КОНЕЦ ВЫБРАНОГО ПЕРИОДА
            , System.DateTime.DaysInMonth(int.Parse(year), int.Parse(month))
        );

        var prevMonthEnd = Date.AddPeriods(System.Convert.ToDateTime(endDate), PeriodType.Month, -1); // ПРОШЛЫЙ МЕСЯЦ
        string prevMonthStart = "2019-05-01";

        // ПОЛУЧАЕМ ВСЕ ЧТО БЫЛО ПРЕДЪЯВЛЕНО В ЭТОМ МЕСЯЦЕ - ЗАПИСЫВАЕМ ВО ВРЕМЕННУЮ ТАБЛИЦУ
        string sqlFact = @"
            SELECT
                DMS.KZAJ_CR, FORMAT(DMS.DDM, 'yy-MM-dd') AS DDM, SUM(DMS.KOL) AS KOL, DMS.KMAT,
                LEFT(DMS.KZAJ_CR, 10) AS NDM,
                PLA.UNDOC_Z AS UNDOC, PLA.NPP_Z AS NPP
            FROM DMZ
            INNER JOIN DMS ON DMS.UNDOC = DMZ.UNDOC AND LEFT(DMS.KMAT, 2) = @_PROD_ID
            LEFT JOIN PLA ON PLA.KOBJ = 'З0002' AND PLA.KPLT = '510' AND PLA.KPLV = 1 AND PLA.KZAJNPP = DMS.KZAJ_CR AND PLA.KMAT = DMS.KMAT
            WHERE DMZ.KDMT = '_SKC' AND DMZ.DDM BETWEEN CONVERT(DATE, @_START_DATE) AND CONVERT(DATE, @_END_DATE)
            GROUP BY DMS.KZAJ_CR, DMS.DDM, DMS.KMAT, PLA.KZAJ, PLA.UNDOC_Z, PLA.NPP_Z
        ";
        var cmdFact = SqlClient.CreateCommand(sqlFact);
        cmdFact.Parameters["_PROD_ID"].Value = prod_id;
        cmdFact.Parameters["_START_DATE"].Value = startDate;
        cmdFact.Parameters["_END_DATE"].Value = endDate;
        string tmpTableFact = Load_To_Db.LoadToDbEx(cmdFact, DbInstance.Main);

        // ОТБИРАЕМ ВСЕ KZAJ_CR ДЛЯ ДАЛЬНЕЙШЕГО ПОИСКА ВСЕГО ПРЕДЪЯВЛЕНИЯ ПО ЭТИМ ЗАКАЗАМ - ОПТИМИЗАЦИЯ ЗАПРОСА
        var cmdGetFactTmpTable = SqlClient.CreateCommand("SELECT DISTINCT KZAJ_CR AS KZAJNPP FROM " + tmpTableFact);

        List<string> listNDM = cmdGetFactTmpTable.ExecScalars<string>();
        //ITnet2.Server.Session.InfoManager.MessageBox( string.Join(",", listNDM.ToArray()));

        //REPLACE(REPLACE(DMS.KZAJ_CR, '  ', '/'), ' ', '') AS NUM,
        //LEFT(DMS1.NDM, 10) + '/' + DMS1.KDS1 AS LOT,
        string sqlGetFact = @"
            SELECT
                '' AS PR_IZG_F,
                FACT.NDM,
                DMS.PR_DO,
                FACT.UNDOC,
                DMS.KDS1,
                FACT.NPP,
                FACT.DDM AS DATE_DIR_Z,
                FORMAT(DMZ.DDM, 'yy-MM-dd') AS DDM,
                DATEDIFF(m, @_END_DATE, DMZ.DDM ) AS DIF,
                FACT.KMAT,
                DMS.KOL,
                FACT.KOL AS KOL_FACT,
                TOTAL.KOL AS TOTAL_FACT,
                DMZ.ORG,
                DMZ.N_KDK_M,
                '_SKC' AS KDMT

            FROM " + tmpTableFact + @" AS FACT
            LEFT JOIN DMS ON DMS.UNDOC = FACT.UNDOC AND DMS.NPP = FACT.NPP AND DMS.KDMT = '_S3C'
            LEFT JOIN DMZ ON DMZ.UNDOC = DMS.UNDOC
            LEFT JOIN (
                SELECT
                   SUM(D.KOL) AS KOL,
                   P.KZAJ AS NDM, P.NPP_Z AS NPP

                FROM DMS AS D
                LEFT JOIN PLA AS P ON P.KOBJ = 'З0002' AND P.KPLT = '510' AND P.KPLV = 1 AND P.KZAJNPP = D.KZAJ_CR AND D.KMAT = P.KMAT

                WHERE D.KDMT = '_SKC' AND D.KZAJ_CR in (@_NDM)
                GROUP BY D.KMAT, P.KZAJ, P.NPP_Z
            ) AS TOTAL ON TOTAL.NDM = FACT.NDM AND TOTAL.NPP = FACT.NPP
        ";

        var cmdGetFact = SqlClient.CreateCommand(sqlGetFact);
        cmdGetFact.Parameters["_END_DATE"].Value = endDate;
        cmdGetFact.Parameters.AddArray("_NDM", listNDM.ToArray());
        //ITnet2.Server.Dialogs.TableViewer.Show(cmdGetFact);
		string tmpTableData = Load_To_Db.LoadToDbEx(cmdGetFact, DbInstance.Main); // СОЗДАЕМ ВРЕМЕННУЮ ТАБЛИЦУ И ЗАПОЛНЯЕМ ЕЕ

        //DMS.NDM + '/' + CAST(DMS.NPP as varchar) AS NUM,
        //DMS.NDM + '/' + DMS.KDS1 AS LOT,
        string sqlGetPerehOrders = @"
            SELECT
             PLA.PR_IZG_F,
             DMZ.NDM,
             DMS.PR_DO,
             DMZ.UNDOC,
             DMS.KDS1,
             DMS.NPP,
             CASE WHEN YEAR(PLA.DATE_DIR_Z) = 9999 OR PLA.DATE_DIR_Z = 0 THEN NULL ELSE FORMAT(PLA.DATE_DIR_Z, 'yy-MM-dd') END AS DATE_DIR_Z,
             FORMAT(DMZ.DDM, 'yy-MM-dd') AS DDM,
             -1 AS DIF,
             DMS.KMAT,
             DMS.KOL,
             0 AS KOL_FACT,
             0 AS TOTAL_FACT,
             DMZ.ORG,
             DMZ.N_KDK_M,
             '_S3C' AS KDMT

            FROM DMS
            LEFT JOIN DMZ ON DMZ.UNDOC = DMS.UNDOC
            LEFT JOIN PLA ON PLA.KZAJNPP = DMS.KZAJ_CR AND PLA.KPLV = '1' AND PLA.KPLT = '510' -- СОСТОЯНИЕ ЗАКАЗА ИЗГОТОВЛЕН(+) ИЛИ НЕТ
            WHERE DMZ.DDM BETWEEN CONVERT(DATE, @_PREV_MONTH_START) AND CONVERT(DATE, @_PREV_MONTH_END) AND DMZ.KDMT = '_S3C'
             AND (PLA.PR_IZG_F <> '+' AND DMS.PR_DO <> '3' AND DMS.PR_DO <> '0') AND LEFT(DMS.KMAT, 2) = @_PROD_ID
        ";

        var cmdGetPerehOrders = SqlClient.CreateCommand(sqlGetPerehOrders);
        cmdGetPerehOrders.Parameters["_STARTDATE"].Value = startDate;
        cmdGetPerehOrders.Parameters["_PREV_MONTH_START"].Value = prevMonthStart;
        cmdGetPerehOrders.Parameters["_PREV_MONTH_END"].Value = prevMonthEnd;
        cmdGetPerehOrders.Parameters["_PROD_ID"].Value = prod_id;
		Load_To_Db.addDataToTable(DbInstance.Main, tmpTableData, cmdGetPerehOrders);

        // ПОЛУЧАЕМ ПЕРЕХОДЯЩИЕ ЗАКАЗЫ КОТОРЫЕ ИЛИ В ПЛАНЕ ИЛИ ЕЩЕ НЕ ВЫПОЛЕНЫЕ
        string sqlGetOrders = @"
            SELECT
             PLA.PR_IZG_F,
             DMZ.NDM,
             DMS.PR_DO,
             DMZ.UNDOC,
             DMS.KDS1,
             DMS.NPP,
             CASE WHEN YEAR(PLA.DATE_DIR_Z) = 9999 OR PLA.DATE_DIR_Z = 0 THEN NULL ELSE FORMAT(PLA.DATE_DIR_Z, 'yy-MM-dd') END AS DATE_DIR_Z,
             FORMAT(DMZ.DDM, 'yy-MM-dd') AS DDM,
             0 AS DIF,
             DMS.KMAT,
             DMS.KOL,
             0 AS KOL_FACT,
             0 AS TOTAL_FACT,
             DMZ.ORG,
             DMZ.N_KDK_M,
             '_S3C' AS KDMT

            FROM DMZ
            LEFT JOIN DMS ON DMS.UNDOC = DMZ.UNDOC
            LEFT JOIN PLA ON PLA.UNDOC_Z = DMS.UNDOC AND PLA.NPP_Z = DMS.NPP AND PLA.KPLV = 1 AND PLA.KPLT = '510' AND PLA.KOBJ = 'З0002'
            WHERE DMZ.DDM BETWEEN CONVERT(DATE, @_START_DATE) AND CONVERT(DATE, @_END_DATE) AND DMZ.KDMT = '_S3C' AND LEFT(DMS.KMAT, 2) = @_PROD_ID
        ";
        var cmdGetOrders = SqlClient.CreateCommand(sqlGetOrders);
        cmdGetOrders.Parameters["_PROD_ID"].Value = prod_id;
        cmdGetOrders.Parameters["_START_DATE"].Value = startDate;
        cmdGetOrders.Parameters["_END_DATE"].Value = endDate;
        Load_To_Db.addDataToTable(DbInstance.Main, tmpTableData, cmdGetOrders);

        string tmpTableTech = Load_To_Db.getTech(DbInstance.Main);

        var cmdGetData = SqlClient.Main.CreateCommand(@"
            SELECT
             TMP.PR_IZG_F,
             TMP.NDM,
             TMP.PR_DO,
             TMP.KDS1,
             TMP.NPP,
             TMP.DATE_DIR_Z,
             TMP.DDM,
             TMP.DIF,
             ISNULL(TMP.KOL, 0) AS KOL,
             TMP.KOL_FACT,
             TMP.TOTAL_FACT,
             TMP.ORG,
             TMP.KDMT,
             SKM.FGOST AS GOST,
             SKM.FNMAT AS TECH_STR,
             ITKSMFAS.TECH AS TECH,
             UPPER(KDK.FIO_OTV) AS FIO_OTV,
             ORG.NORG_S AS ORG_NAME,
             RSS.NRSS_S AS NRSS_SHOT

            FROM " + tmpTableData + @" AS TMP
            LEFT JOIN " + tmpTableTech + @" AS ITKSMFAS ON ITKSMFAS.KMAT = TMP.KMAT
            LEFT JOIN KSM ON KSM.KMAT = TMP.KMAT
            LEFT JOIN SKM ON SKM.SKM = KSM.SKM
            LEFT JOIN KDK ON KDK.N_KDK = TMP.N_KDK_M
            LEFT JOIN ORG ON ORG.ORG = TMP.ORG
            LEFT JOIN RS1 ON RS1.ARSO = 'DMZ10' AND RS1.KEYVALUE = TMP.UNDOC
            LEFT JOIN RSS ON RSS.ARSO = 'DMZ10' AND RSS.ARSV = RS1.ARSV AND RSS.ARSS = RS1.ARSSCURR AND RSS.PR_DO=' '

            ORDER BY TMP.KDMT, TMP.NDM, TMP.NPP
        ");

        //ITnet2.Server.Dialogs.TableViewer.Show(cmdGetData);

        var result = cmdGetData.LoadToLocalDb();

        SqlClient.Main.DropTempTable(tmpTableData);
        SqlClient.Main.DropTempTable(tmpTableTech);
        SqlClient.Main.DropTempTable(tmpTableFact);

        //return null;
        return result;
    }

    // получить заказ по номеру
    public static object getOrderByNdm(string ndm)
    {
        var builder = SqlClient.Main.CreateSelectBuilder();
        builder.From.Table.Name = "DMZ";
        builder.Fields.AddAllFields("DMZ");
        builder.Where.CommandText = "DMZ.NDM = @_NDM AND DMZ.KDMT = @_KDMT";
        builder.Where.Parameters.Add("_NDM", ndm);
        builder.Where.Parameters.Add("_KDMT", "_S3C");

        //ITnet2.Server.Dialogs.TableViewer.Show(builder.GetCommand());
        return builder.LoadToLocalDb();
    }

    // получить заказ по уникальному номеру документа
    public static object getOrderByUndoc(int undoc)
    {

        ITnet2.Server.Session.InfoManager.MessageBox(undoc.ToString());
        return null;
    }
}