using ITnet2.Server.Data;

public class Production
{
    public static object getProduction(string month, string year, string day = null)
    {
        string startDate = string.Empty;
        string endDate = string.Empty;
        // ЗАДАЕМ ВСЕ НУЖНЫЕ ЗНАЧЕНИЯ ДЛЯ ОТБОРА
        if ( day == null )
        {
            startDate = string.Format("{0}-{1}-01T00:00:01", year, month); // НАЧАЛО
            endDate = string.Format("{0}-{1}-{2}T23:59:59", year, month // КОНЕЦ ВЫБРАНОГО ПЕРИОДА
                , System.DateTime.DaysInMonth(int.Parse(year), int.Parse(month))
            );
        } else {
            startDate = string.Format("{0}-{1}-{2}T00:00:01", year, month, day); // НАЧАЛО
            endDate = string.Format("{0}-{1}-{2}T23:59:59", year, month, day); // КОНЕЦ ВЫБРАНОГО ПЕРИОДА
        }

        //ITnet2.Server.Session.InfoManager.MessageBox(endDate);

        string sqlProd = @"
            SELECT
                REG.DATE_YEAR,
                REG.DATE_MONTH,
                REG.DATE_DAY,
                REG.SMENA,
                --CAST(DATEDIFF(MINUTE, '1970-01-01', REG.TIMENACH_F) AS BIGINT)*60*1000 as time_start_milliseconds,
                --CAST(DATEDIFF(MINUTE, '1970-01-01', REG.TIMEOKON_F) AS BIGINT)*60*1000 as time_end_milliseconds,
                FORMAT(REG.TIMENACH_F, 'yyyy-MM-dd HH:mm') AS DATETIME_START,
                FORMAT(REG.TIMEOKON_F, 'yyyy-MM-dd HH:mm') AS DATETIME_FINISH,
                LEFT(MKT.KMAT, 2) AS PROD_GROUP,
                KSM.N_RES AS PROD_NAME,
                PRCR.KMATOB AS RESOURCE_CODE,
                --DMS.KZAJ_CR AS KZAJNPP,
                MKT.KZAJNPP,
                MKT.KMAT,
                TTOP.NTTOP AS ACTION,
                MKTS.NTECHPR AS TECH_PROCESS, -- типовая технология GTPT - строки (GTPTS.NOP - порядковый номер операции); GPR ON GPR.UNGPR = GTPTS.UNGPR
                REG.RCENTR_F AS RCENTER_ID,
                PRCR.NAME AS RCENTER_NAME,
                TAPEI.EDI,
                TAPEI.NEDI,
                KSM.KPER3_I AS KOEF_TO_TONS, -- Коэффициент перевода из метров в тонны
                REG.KOL_F AS KOL_FACT,
                REG.KOL_B AS KOL_DEFECT,
                (REG.KOL_F + REG.KOL_B) AS KOL_TOTAL,
                (REG.DIF_M * (NRMD.VNRM / 60)) AS PRODUCTIVITY, -- продуктивность
                CASE WHEN (REG.KOL_F > 0 AND NRMD.VNRM > 0 AND REG.DIF_M > 0)
                    THEN ROUND( ((REG.KOL_F + REG.KOL_B) / (REG.DIF_M * (NRMD.VNRM / 60))) * 100, 0 )
                    ELSE 0
                END AS FACT_PROD, -- факт выработки
                PRCR.KOEF AS RCENTER_KOEF,
                POD.CEH AS GUILD_ID,
                POD.NAIM_P AS GUILD,
                REG.DIF_M,
                (DATEDIFF(mi, REG.TIMENACH_F, REG.TIMEOKON_F) / 60 + (DATEDIFF(mi, REG.TIMENACH_F,REG.TIMEOKON_F) % 60) / 100.0) AS DIF_H,
                NRMD.TNRM, -- ч/т - часов на 1 т
                NRMD.VNRM -- т/ч - т за 1 час

            FROM (
                SELECT YEAR(TIMEOKON_F) AS DATE_YEAR, MONTH(TIMEOKON_F) AS DATE_MONTH, DAY(TIMEOKON_F) AS DATE_DAY,
                    CASE WHEN CAST(TIMEOKON_F AS TIME) >= TIMEFROMPARTS(7,30,0,0,0) AND CAST(TIMEOKON_F AS TIME) <= TIMEFROMPARTS(19,30,0,0,0)
                        THEN 1
                        ELSE 2
                    END AS SMENA,
                    DATEDIFF(mi, TIMENACH_F, TIMEOKON_F) AS DIF_M,
                    RCENTR_F, KOL_F, KOL_B, KPRT, UNMKTS, TIMENACH_F, TIMEOKON_F, UNDOC, NPP
                FROM MKTSREG
                WHERE REGTYPE = 1 AND TIMEOKON_F BETWEEN CONVERT(DATETIME, @_DATE_START) AND CONVERT(DATETIME, @_DATE_END)
            ) REG

            LEFT JOIN PRCR ON PRCR.RCENTR = REG.RCENTR_F -- рабочие центры
            LEFT JOIN PRT ON PRT.KPRT = REG.KPRT -- учет ТМЦ. Справочник партий
            LEFT JOIN PRT_EXT ON PRT_EXT.KPRT = PRT.KPRT -- Справочник партий
            LEFT JOIN MKTS ON MKTS.UNMKTS = REG.UNMKTS -- технологические карты
            LEFT JOIN TTOP ON TTOP.KTTOP = MKTS.KTTOP -- справочник тех. операций
            LEFT JOIN MKT ON MKT.UNMKT = MKTS.UNMKT -- маршрутные листы
            LEFT JOIN TAPEI ON TAPEI.EDI = MKT.EDI -- Справочник единиц измерения
            LEFT JOIN KSM ON KSM.KMAT = MKT.KMAT -- классификатор ресурсов
            LEFT JOIN POD ON POD.CEH = PRCR.CEH -- справочник подразделений
            LEFT JOIN NRMD ON NRMD.KMAT = PRCR.KMATOB AND NRMD.KMATGP = MKT.KMAT AND NRMD.CEH = PRCR.CEH
              AND NRMD.TIPNRM = '1SE' AND NRMD.DOPKEYM = MKTS.NTECHPR -- нормы производительности

            ORDER BY PRCR.NAME, MKTS.NTECHPR, MKT.KMAT, REG.TIMENACH_F DESC
            --ORDER BY MKT.KMAT, KSM.N_RES, MKTS.NTECHPR
        ";

        var cmdProd = SqlClient.CreateCommand(sqlProd);
        cmdProd.Parameters["_DATE_START"].Value = startDate;
        cmdProd.Parameters["_DATE_END"].Value = endDate;

        //ITnet2.Server.Dialogs.TableViewer.Show(cmdProd);

        //return null;
        return cmdProd.LoadToLocalDb();
    }
}