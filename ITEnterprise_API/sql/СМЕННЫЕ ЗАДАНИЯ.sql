SELECT

  SMZ.DIF_M,
  SMZ.DIF_M_LEFT,
  SMZ.DIF_M_RIGTH,
	SMZ.UNSMZ, -- Уникальный номер работы СЗ
	SMZ.UNSMZD, -- Уникальный номер сменного задания
	MKTS.UNMKTS, -- индет-ор операции
	MKTS.UNMKT, -- индет-ор PyG (маршрутный лист)
	SMZ.STATE, -- Состояние работы (D - Выполнено I - выполнено частично S - Создано С - убираем (?))
	MKTS.NTECHPR AS TECH_PROCESS, -- Цеховый техпроцесс
	SMZ.KOL_F, -- Количество годных
	SMZ.KOL_P, -- Количество план
	TAPEI.NEDI,
	SMZ.STATUS, -- активный план
	SMZ.DATETIME_START, -- Плановое время начала
	SMZ.DATETIME_FINISH, -- Плановое время окончания
	SMZ.TIMENACH_F, -- Фактическое время начала
	SMZ.TIMEOKON_F, -- Фактическое время окончания
	SMZ.RCENTR_P, -- Плановый рабочий центр
	SMZ.RCENTR_F, -- Фактический рабочий центр
	SMZ.RCENTER_ID, -- рабочий центр ID
	PRCR.NAME AS RCENTER_NAME, -- рабочий центр название
	PRCR.KOEF AS RCENTER_KOEF, -- коэффициент использования ресурса
	SMZ.COMM,
	SMZ.COMM_,
	SMZ.K_DOPTP_, -- Допуск
	SMZ.NPP, -- Номер по порядку
	SMZ.K_MARV_, -- Маршрут волочения
	MKT.KZAJNPP,
	MKT.KMAT,
	LEFT(MKT.KMAT, 2) AS PROD_GROUP,
	MKT.PR_SOST, -- Признак состояния
	MKT.MKD, -- Дата маршрутного листа
	MKT.MKN, -- Номер маршрутного листа
	KSM.KMAT AS PRODUCT_ID, -- код ресурса
	KSM.N_RES AS PRODUCT_NAME, -- Классификатор ресурсов
	POD.CEH AS GUILD_ID,
	POD.NAIM_P AS GUILD,
	POD.NAIM_PS AS GUILD_SHORT,
	NRMD.TNRM, -- Трудоемкость
	NRMD.VNRM, -- т/ч - т за 1 час
	SMZ.SMZD,
	SMZ.DATEFROM,
	SMZ.DATETO

FROM  (
  SELECT SMZD.STATUS, SMZ.UNSMZ, SMZ.UNSMZD, SMZ.STATE, SMZ.TNRM, SMZ.KOL_F, SMZ.KOL_P, SMZ.UNMKTS,
    FORMAT(SMZ.TIMENACH_P, 'yyyy-MM-dd HH:mm') AS DATETIME_START,
    FORMAT(SMZ.TIMEOKON_P, 'yyyy-MM-dd HH:mm') AS DATETIME_FINISH,
    FORMAT(SMZ.TIMENACH_F, 'yyyy-MM-dd HH:mm') AS TIMENACH_F,
    FORMAT(SMZ.TIMEOKON_F, 'yyyy-MM-dd HH:mm') AS TIMEOKON_F,
    SMZ.RCENTR_P, SMZ.RCENTR_F, ISNULL(SMZ.RCENTR_F, SMZ.RCENTR_P) AS RCENTER_ID,
    SMZ.COMM, SMZ.COMM_, SMZ.K_DOPTP_, SMZ.NPP, SMZ.K_MARV_,
    CASE WHEN DAY(SMZ.TIMENACH_P) = DAY(SMZ.TIMEOKON_P)
      THEN 0
      ELSE DATEDIFF(mi, RIGHT(SMZ.TIMENACH_P, 8), '23:59:59')
    END AS DIF_M_LEFT,
    CASE WHEN DAY(SMZ.TIMENACH_P) = DAY(SMZ.TIMEOKON_P)
      THEN 0
      ELSE DATEDIFF(mi, '00:00:01', RIGHT(SMZ.TIMEOKON_P, 8))
    END AS DIF_M_RIGTH,
    DATEDIFF(mi, SMZ.TIMENACH_P, SMZ.TIMEOKON_P) AS DIF_M,
    FORMAT(SMZD.SMZD, 'yyyy-MM-dd HH:mm') AS SMZD,
    FORMAT(SMZD.DATEFROM, 'yyyy-MM-dd HH:mm') AS DATEFROM,
    FORMAT(SMZD.DATETO, 'yyyy-MM-dd HH:mm') AS DATETO
  FROM SMZ
  LEFT JOIN SMZD ON SMZD.UNSMZD = SMZ.UNSMZD
  WHERE SMZD.STATUS = 1 AND -- действующее СЗ
    SMZD.KOBJ = 'З0002'
    --AND SMZ.TIMENACH_P BETWEEN CONVERT(DATETIME, '2019-12-01T00:00:01') AND CONVERT(DATETIME, '2019-12-31T23:59:59')
) AS SMZ -- Задания на производство
LEFT JOIN MKTS ON MKTS.UNMKTS = SMZ.UNMKTS --  технологические операции
LEFT JOIN PRCR ON PRCR.RCENTR = ISNULL(SMZ.RCENTR_F, SMZ.RCENTR_P) -- рабочие центры
LEFT JOIN MKT ON MKT.UNMKT = MKTS.UNMKT -- Производство. Маршрутные листы
LEFT JOIN KSM ON KSM.KMAT = MKT.KMAT -- Классификатор ресурсов
LEFT JOIN TAPEI ON TAPEI.EDI = MKT.EDI
LEFT JOIN POD ON POD.CEH = PRCR.CEH -- справочник подразделений
LEFT JOIN NRMD ON NRMD.KMAT = PRCR.KMATOB AND NRMD.KMATGP = MKT.KMAT AND NRMD.CEH = PRCR.CEH
  AND NRMD.TIPNRM = '1SE' AND NRMD.DOPKEYM = MKTS.NTECHPR -- нормы производительности

ORDER BY PRCR.RCENTR, SMZ.DATETIME_START