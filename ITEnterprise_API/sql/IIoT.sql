select val.*, sen.name as sensors, sen.ORDERNUM, thi.name as things, tp.name as sensortp, tp.measure, tp.charttype
	
from sensval val
left join sensors sen on sen.id = val.sensorid
left join things thi on thi.id = sen.thingid
left join sensortp tp on tp.id = sen.typeid
where val.TIMESTAMP BETWEEN CONVERT(DATE, '01-07-2019', 103) AND CONVERT(DATE, '31-07-2019', 103)
  and val.sensorid not in (4, 5)
order by val.timestamp asc

/** ----------------------- **/

select val.*, sen.name as sensors, sen.ORDERNUM, thi.name as things, tp.name as sensortp, tp.measure, tp.charttype

from sensval val
left join sensors sen on sen.id = val.sensorid
left join things thi on thi.id = sen.thingid
left join sensortp tp on tp.id = sen.typeid
where val.TIMESTAMP BETWEEN CONVERT(DATETIME, '2019-07-01 09:35:57', 120) AND CONVERT(DATETIME, '2019-07-01 20:33:52', 120)
  and val.sensorid not in (4, 5)
order by val.timestamp asc