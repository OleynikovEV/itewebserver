SELECT
    FORMAT(APSV.ALLOCDATE, 'yyyy-MM-dd HH:mm') AS ALLOCDATE, -- дата начала плана
    FORMAT(APSVS.DATE_D, 'yyyy-MM-dd') AS DATE_D, -- дата добавления
    APSVS.FIO_D, -- кто добавил
    APSVS.RTYPE AS INTERVAL_ID, -- интервал: 4 -простой; 3 - перерыв; 2 - переналадка; 1 - работа;
    CASE WHEN APSVS.RTYPE = 1
        THEN 'работа'
        ELSE
            CASE WHEN APSVS.RTYPE = 2
            THEN 'переналадка'
            ELSE
                CASE WHEN APSVS.RTYPE = 3
                    THEN 'перерыв'
                    ELSE
                        CASE WHEN APSVS.RTYPE = 4
                            THEN 'простой'
                            ELSE ''
                        END
                END
            END
    END AS INTERVAL_NAME,
    APSVS.KMATOB AS RCENTER_KMAT, -- оборудование
    PRCRWCCUR.RCENTR AS RCENTER_ID,
    PRCRWCCUR.NAME AS RCENTER_NAME, -- рабочий центр
    PRCRWCCUR.KOEF AS RCENTER_KOEF, -- коэффициент использования ресурса
    YEAR(APSVS.TSTART_S) AS YEAR_START,
    MONTH(APSVS.TSTART_S) AS MONTH_START,
    FORMAT(APSVS.TSTART_S, 'yyyy-MM-dd HH:mm') AS DATETIME_START, -- начало производства продукции на оборудовании - переналадка
    FORMAT(APSVS.TFINISH_S, 'yyyy-MM-dd HH:mm') AS DATETIME_FINISH, -- окончание производства продукции на оборудовании
    DATEDIFF(mi, APSVS.TSTART_S, APSVS.TFINISH_S) AS DIF_M,
    (DATEDIFF(mi, APSVS.TSTART_S, APSVS.TFINISH_S) / 60 + (DATEDIFF(mi, APSVS.TSTART_S, APSVS.TFINISH_S) % 60) / 100.0) AS DIF_H,
    KSM.KMAT AS PRODUCT_ID, -- код ресурса
    KSM.N_RES AS PRODUCT_NAME, -- название изделия
    APSVS.PLANQT AS KOL_PLAN, -- план
    APSVS.DONEQT AS KOL_FACT, -- факт
    TAPEI.NEDI, -- ЕИ
    APSVS.RESTQT, -- осталось
    APSVS.RESTQT_S, -- остаток изготовления интервала
    APSVS.RESTQT_S - APSVS.RESTQT AS BALANCE, -- остаток
    APSVS.NOP AS TECH_PROCESS_ID, -- № техоперации
    APSVS.NTECHPR AS TECH_PROCESS, -- техмаршрут
    TTOP.NTTOP AS ACTION, -- операция
    APSVS.KZAJNPP, -- заказ
    APSVS.KZAJGP, -- заказ
    APSVS.ISDONE, -- выполнен (+)
    APSVS.P1 AS PRIORITY, -- приоритет
    FORMAT(APSVS.DIRDATE, 'yyyy-MM-dd HH:mm') AS DIRDATE, -- дерективный срок (+ 5 дней)
    APSVS.SETUPTIME, -- переналадка (мин)
    POD.CEH AS GUILD_ID, -- подразделение
    POD.NAIM_P AS GUILD, -- подразделение

    APSV.UNAPSV, -- номер версии плана
    APSVS.UNAPSVS, -- идент-р позиции
    APSVS.UNMKT, -- ЗнП
    APSVS.UNMKTS, -- операция ЗнП
    APSVS.KZAP, -- идентификатор позиции плана
    APSVS.BATCHID, -- номер партии
    FORMAT(APSVS.DATE_IN, 'yyyy-MM-dd HH:mm') AS DATE_IN -- дата включения в план

FROM APSV
LEFT JOIN APSVS ON APSV.UNAPSV = APSVS.UNAPSV
LEFT JOIN KSM ON KSM.KMAT = APSVS.KMAT -- нп=аименование продукции
LEFT JOIN POD ON POD.CEH = APSVS.CEH -- подразделения
LEFT JOIN PRCR PRCRWCCUR ON PRCRWCCUR.RCENTR = APSVS.RCENTR
--LEFT JOIN PRCR PRCRWCNEX ON PRCRWCNEX.RCENTR = APSVS.RCENTR_K
LEFT JOIN TAPEI ON TAPEI.EDI = APSVS.MEASUREID -- единицы измерений
LEFT JOIN TTOP ON TTOP.KTTOP = APSVS.KTTOP

WHERE APSV.STATUS = 'A' --(APSVS.UNAPSV=575)
ORDER BY APSVS.TSTART_S