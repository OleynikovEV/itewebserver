using ITnet2.Server.Data;

public class Plan
{
    /** ПОЛУЧИТЬ ДЕЙСТВУЮЩИЕ СМЕННЫЕ ЗАДАНИЯ */
    public static object getActionSmen()
    {
        string sql = @"
            SELECT
                SMZ.DIF_M,
                SMZ.DIF_M_LEFT,
                SMZ.DIF_M_RIGTH,
	            SMZ.UNSMZ, -- Уникальный номер работы СЗ
	            SMZ.UNSMZD, -- Уникальный номер сменного задания
	            MKTS.UNMKTS, -- индет-ор операции
	            MKTS.UNMKT, -- индет-ор PyG (маршрутный лист)
	            SMZ.STATE, -- Состояние работы (D - Выполнено I - выполнено частично S - Создано С - убираем (?))
	            MKTS.NTECHPR AS TECH_PROCESS, -- Цеховый техпроцесс
	            SMZ.KOL_F, -- Количество годных
	            SMZ.KOL_P, -- Количество план
	            TAPEI.NEDI,
	            SMZ.STATUS, -- активный план
	            SMZ.DATETIME_START, -- Плановое время начала
	            SMZ.DATETIME_FINISH, -- Плановое время окончания
	            SMZ.TIMENACH_F, -- Фактическое время начала
	            SMZ.TIMEOKON_F, -- Фактическое время окончания
	            SMZ.RCENTR_P, -- Плановый рабочий центр
	            SMZ.RCENTR_F, -- Фактический рабочий центр
	            SMZ.RCENTER_ID, -- рабочий центр ID
	            PRCR.NAME AS RCENTER_NAME, -- рабочий центр название
	            PRCR.KOEF AS RCENTER_KOEF, -- коэффициент использования ресурса
	            SMZ.COMM,
	            SMZ.COMM_,
	            SMZ.K_DOPTP_, -- Допуск
	            SMZ.NPP, -- Номер по порядку
	            SMZ.K_MARV_, -- Маршрут волочения
	            MKT.KZAJNPP,
	            MKT.KMAT,
	            LEFT(MKT.KMAT, 2) AS PROD_GROUP,
	            MKT.PR_SOST, -- Признак состояния
	            MKT.MKD, -- Дата маршрутного листа
	            MKT.MKN, -- Номер маршрутного листа
	            KSM.KMAT AS PRODUCT_ID, -- код ресурса
	            KSM.N_RES AS PRODUCT_NAME, -- Классификатор ресурсов
	            POD.CEH AS GUILD_ID,
	            POD.NAIM_P AS GUILD,
	            POD.NAIM_PS AS GUILD_SHORT,
	            NRMD.TNRM, -- Трудоемкость
	            NRMD.VNRM -- т/ч - т за 1 час

            FROM  (
              SELECT SMZD.STATUS, SMZ.UNSMZ, SMZ.UNSMZD, SMZ.STATE, SMZ.TNRM, SMZ.KOL_F, SMZ.KOL_P, SMZ.UNMKTS,
                FORMAT(SMZ.TIMENACH_P, 'yyyy-MM-dd HH:mm') AS DATETIME_START,
                FORMAT(SMZ.TIMEOKON_P, 'yyyy-MM-dd HH:mm') AS DATETIME_FINISH,
                FORMAT(SMZ.TIMENACH_F, 'yyyy-MM-dd HH:mm') AS TIMENACH_F,
                FORMAT(SMZ.TIMEOKON_F, 'yyyy-MM-dd HH:mm') AS TIMEOKON_F,
                SMZ.RCENTR_P, SMZ.RCENTR_F, ISNULL(SMZ.RCENTR_F, SMZ.RCENTR_P) AS RCENTER_ID,
                SMZ.COMM, SMZ.COMM_, SMZ.K_DOPTP_, SMZ.NPP, SMZ.K_MARV_,
                CASE WHEN DAY(SMZ.TIMENACH_P) = DAY(SMZ.TIMEOKON_P)
                  THEN 0
                  ELSE DATEDIFF(mi, RIGHT(SMZ.TIMENACH_P, 8), '23:59:59')
                END AS DIF_M_LEFT,
                CASE WHEN DAY(SMZ.TIMENACH_P) = DAY(SMZ.TIMEOKON_P)
                  THEN 0
                  ELSE DATEDIFF(mi, '00:00:01', RIGHT(SMZ.TIMEOKON_P, 8))
                END AS DIF_M_RIGTH,
                DATEDIFF(mi, SMZ.TIMENACH_P, SMZ.TIMEOKON_P) AS DIF_M
              FROM SMZ
              LEFT JOIN SMZD ON SMZD.UNSMZD = SMZ.UNSMZD
              WHERE --SMZD.STATUS = 1 -- действующее СЗ
                SMZD.KOBJ = 'З0002'
                AND SMZ.TIMENACH_P BETWEEN CONVERT(DATETIME, '2019-12-01T00:00:01') AND CONVERT(DATETIME, '2019-12-31T23:59:59')
            ) AS SMZ --Задания на производство
            LEFT JOIN MKTS ON MKTS.UNMKTS = SMZ.UNMKTS--  технологические операции
            LEFT JOIN PRCR ON PRCR.RCENTR = ISNULL(SMZ.RCENTR_F, SMZ.RCENTR_P)-- рабочие центры
            LEFT JOIN MKT ON MKT.UNMKT = MKTS.UNMKT-- Производство.Маршрутные листы
            LEFT JOIN KSM ON KSM.KMAT = MKT.KMAT-- Классификатор ресурсов
            LEFT JOIN TAPEI ON TAPEI.EDI = MKT.EDI
            LEFT JOIN POD ON POD.CEH = PRCR.CEH-- справочник подразделений
            LEFT JOIN NRMD ON NRMD.KMAT = PRCR.KMATOB AND NRMD.KMATGP = MKT.KMAT AND NRMD.CEH = PRCR.CEH
              AND NRMD.TIPNRM = '1SE' AND NRMD.DOPKEYM = MKTS.NTECHPR-- нормы производительности

            ORDER BY PRCR.RCENTR, SMZ.DATETIME_START
        ";

        var cmd = SqlClient.CreateCommand(sql);

        //ITnet2.Server.Dialogs.TableViewer.Show(cmd);

        //return null;
        return cmd.LoadToLocalDb();
    }

    /** ПОЛУЧИТЬ ДЕЙСТВУЮЩИЙ APS-планирование присутствуют запланированные простои и перезарядки*/
    public static object getActionAPS()
    {
        string sql = @"
            SELECT
                FORMAT(APSV.ALLOCDATE, 'yyyy-MM-dd HH:mm') AS ALLOCDATE, -- дата начала плана
                FORMAT(APSVS.DATE_D, 'yyyy-MM-dd') AS DATE_D, -- дата добавления
                APSVS.FIO_D, -- кто добавил
                APSVS.RTYPE AS INTERVAL_ID, -- интервал: 4 -простой; 3 - перерыв; 2 - переналадка; 1 - работа;
                CASE WHEN APSVS.RTYPE = 1
                    THEN 'работа'
                    ELSE
                        CASE WHEN APSVS.RTYPE = 2
                        THEN 'переналадка'
                        ELSE
                            CASE WHEN APSVS.RTYPE = 3
                                THEN 'перерыв'
                                ELSE
                                    CASE WHEN APSVS.RTYPE = 4
                                        THEN 'простой'
                                        ELSE ''
                                    END
                            END
                        END
                END AS INTERVAL_NAME,
                APSVS.KMATOB AS RCENTER_KMAT, -- оборудование
                PRCRWCCUR.RCENTR AS RCENTER_ID,
                PRCRWCCUR.NAME AS RCENTER_NAME, -- рабочий центр
                PRCRWCCUR.KOEF AS RCENTER_KOEF, -- коэффициент использования ресурса
                YEAR(APSVS.TSTART_S) AS YEAR_START,
                MONTH(APSVS.TSTART_S) AS MONTH_START,
                FORMAT(APSVS.TSTART_S, 'yyyy-MM-dd HH:mm') AS DATETIME_START, -- начало производства продукции на оборудовании - переналадка
                FORMAT(APSVS.TFINISH_S, 'yyyy-MM-dd HH:mm') AS DATETIME_FINISH, -- окончание производства продукции на оборудовании
                DATEDIFF(mi, APSVS.TSTART_S, APSVS.TFINISH_S) AS DIF_M,
                (DATEDIFF(mi, APSVS.TSTART_S, APSVS.TFINISH_S) / 60 + (DATEDIFF(mi, APSVS.TSTART_S, APSVS.TFINISH_S) % 60) / 100.0) AS DIF_H,
                KSM.KMAT AS PRODUCT_ID, -- код ресурса
                KSM.N_RES AS PRODUCT_NAME, -- название изделия
                CASE WHEN APSVS.RTYPE = 1
                    THEN
                        CASE WHEN APSVS.MEASUREID = 168
                            THEN APSVS.PLANQT
                            ELSE APSVS.PLANQT2
                        END
                    ELSE 0
                END AS KOL_PLAN, -- план (т.)
                --CASE WHEN APSVS.RTYPE = 1
                --    THEN
                --        CASE WHEN APSVS.MEASUREID = 168
                --            THEN APSVS.DONEQT
                --            ELSE (APSVS.DONEQT / (APSVS.PLANQT / APSVS.PLANQT2))
                --        END
                --    ELSE 0
                --END AS KOL_FACT, -- факт (т.)
                APSVS.RESTQT, -- осталось
                APSVS.RESTQT_S, -- остаток изготовления интервала
                APSVS.RESTQT_S - APSVS.RESTQT AS BALANCE, -- остаток
                APSVS.NOP AS TECH_PROCESS_ID, -- № техоперации
                APSVS.NTECHPR AS TECH_PROCESS, -- техмаршрут
                TTOP.NTTOP AS ACTION, -- операция
                APSVS.KZAJNPP, -- заказ
                APSVS.KZAJGP, -- заказ
                APSVS.ISDONE, -- выполнен (+)
                APSVS.P1 AS PRIORITY, -- приоритет
                FORMAT(APSVS.DIRDATE, 'yyyy-MM-dd HH:mm') AS DIRDATE, -- дерективный срок (+ 5 дней)
                APSVS.SETUPTIME, -- переналадка (мин)
                POD.CEH AS GUILD_ID, -- подразделение
                POD.NAIM_P AS GUILD, -- подразделение

                APSV.UNAPSV, -- номер версии плана
                APSVS.UNAPSVS, -- идент-р позиции
                APSVS.UNMKT, -- ЗнП
                APSVS.UNMKTS, -- операция ЗнП
                APSVS.KZAP, -- идентификатор позиции плана
                APSVS.BATCHID, -- номер партии
                FORMAT(APSVS.DATE_IN, 'yyyy-MM-dd HH:mm') AS DATE_IN -- дата включения в план

            FROM APSV
            LEFT JOIN APSVS ON APSV.UNAPSV = APSVS.UNAPSV
            LEFT JOIN KSM ON KSM.KMAT = APSVS.KMAT -- нп=аименование продукции
            LEFT JOIN POD ON POD.CEH = APSVS.CEH -- подразделения
            LEFT JOIN PRCR PRCRWCCUR ON PRCRWCCUR.RCENTR = APSVS.RCENTR
            --LEFT JOIN PRCR PRCRWCNEX ON PRCRWCNEX.RCENTR = APSVS.RCENTR_K
            LEFT JOIN TAPEI ON TAPEI.EDI = APSVS.MEASUREID -- единицы измерений
            LEFT JOIN TTOP ON TTOP.KTTOP = APSVS.KTTOP

            WHERE APSV.STATUS = 'A' --(APSVS.UNAPSV=575)
            ORDER BY APSVS.TSTART_S
        ";

        var cmd = SqlClient.CreateCommand(sql);

        //ITnet2.Server.Dialogs.TableViewer.Show(cmd);

        //return null;
        return cmd.LoadToLocalDb();
    }

    /** ПОЛУЧИТЬ APS-плана по номеру */
    public static object getAPSByNumber(string num)
    {
        string sql = @"
            SELECT
                FORMAT(APSV.ALLOCDATE, 'yyyy-MM-dd HH:mm') AS ALLOCDATE, -- дата начала плана
                FORMAT(APSVS.DATE_D, 'yyyy-MM-dd') AS DATE_D, -- дата добавления
                APSVS.FIO_D, -- кто добавил
                APSVS.RTYPE AS INTERVAL_ID, -- интервал: 4 -простой; 3 - перерыв; 2 - переналадка; 1 - работа;
                CASE WHEN APSVS.RTYPE = 1
                    THEN 'работа'
                    ELSE
                        CASE WHEN APSVS.RTYPE = 2
                        THEN 'переналадка'
                        ELSE
                            CASE WHEN APSVS.RTYPE = 3
                                THEN 'перерыв'
                                ELSE
                                    CASE WHEN APSVS.RTYPE = 4
                                        THEN 'простой'
                                        ELSE ''
                                    END
                            END
                        END
                END AS INTERVAL_NAME,
                APSVS.KMATOB AS RCENTER_KMAT, -- оборудование
                PRCRWCCUR.RCENTR AS RCENTER_ID,
                PRCRWCCUR.NAME AS RCENTER_NAME, -- рабочий центр
                PRCRWCCUR.KOEF AS RCENTER_KOEF, -- коэффициент использования ресурса
                YEAR(APSVS.TSTART_S) AS YEAR_START,
                MONTH(APSVS.TSTART_S) AS MONTH_START,
                FORMAT(APSVS.TSTART_S, 'yyyy-MM-dd HH:mm') AS DATETIME_START, -- начало производства продукции на оборудовании - переналадка
                FORMAT(APSVS.TFINISH_S, 'yyyy-MM-dd HH:mm') AS DATETIME_FINISH, -- окончание производства продукции на оборудовании
                DATEDIFF(mi, APSVS.TSTART_S, APSVS.TFINISH_S) AS DIF_M,
                (DATEDIFF(mi, APSVS.TSTART_S, APSVS.TFINISH_S) / 60 + (DATEDIFF(mi, APSVS.TSTART_S, APSVS.TFINISH_S) % 60) / 100.0) AS DIF_H,
                KSM.KMAT AS PRODUCT_ID, -- код ресурса
                KSM.N_RES AS PRODUCT_NAME, -- название изделия
                CASE WHEN APSVS.RTYPE = 1
                    THEN
                        CASE WHEN APSVS.MEASUREID = 168
                            THEN APSVS.PLANQT
                            ELSE APSVS.PLANQT2
                        END
                    ELSE 0
                END AS KOL_PLAN, -- план (т.)
                --CASE WHEN APSVS.RTYPE = 1
                --    THEN
                --        CASE WHEN APSVS.MEASUREID = 168
                --            THEN APSVS.DONEQT
                --            ELSE (APSVS.DONEQT / (APSVS.PLANQT / APSVS.PLANQT2))
                --        END
                --    ELSE 0
                --END AS KOL_FACT, -- факт (т.)
                APSVS.RESTQT, -- осталось
                APSVS.RESTQT_S, -- остаток изготовления интервала
                APSVS.RESTQT_S - APSVS.RESTQT AS BALANCE, -- остаток
                APSVS.NOP AS TECH_PROCESS_ID, -- № техоперации
                APSVS.NTECHPR AS TECH_PROCESS, -- техмаршрут
                TTOP.NTTOP AS ACTION, -- операция
                APSVS.KZAJNPP, -- заказ
                APSVS.KZAJGP, -- заказ
                APSVS.ISDONE, -- выполнен (+)
                APSVS.P1 AS PRIORITY, -- приоритет
                FORMAT(APSVS.DIRDATE, 'yyyy-MM-dd HH:mm') AS DIRDATE, -- дерективный срок (+ 5 дней)
                APSVS.SETUPTIME, -- переналадка (мин)
                POD.CEH AS GUILD_ID, -- подразделение
                POD.NAIM_P AS GUILD, -- подразделение

                APSV.UNAPSV, -- ?
                APSVS.UNAPSVS, -- идент-р позиции
                APSVS.UNMKT, -- ЗнП
                APSVS.UNMKTS, -- операция ЗнП
                APSVS.KZAP, -- идентификатор позиции плана
                APSVS.BATCHID, -- номер партии
                FORMAT(APSVS.DATE_IN, 'yyyy-MM-dd HH:mm') AS DATE_IN -- дата включения в план

            FROM APSV
            LEFT JOIN APSVS ON APSV.UNAPSV = APSVS.UNAPSV
            LEFT JOIN KSM ON KSM.KMAT = APSVS.KMAT -- нп=аименование продукции
            LEFT JOIN POD ON POD.CEH = APSVS.CEH -- подразделения
            LEFT JOIN PRCR PRCRWCCUR ON PRCRWCCUR.RCENTR = APSVS.RCENTR
            --LEFT JOIN PRCR PRCRWCNEX ON PRCRWCNEX.RCENTR = APSVS.RCENTR_K
            LEFT JOIN TAPEI ON TAPEI.EDI = APSVS.MEASUREID -- единицы измерений
            LEFT JOIN TTOP ON TTOP.KTTOP = APSVS.KTTOP

            WHERE APSV.UNAPSV = @_UNAPSV
            ORDER BY APSVS.TSTART_S
        ";

        var cmd = SqlClient.CreateCommand(sql);
        cmd.Parameters["_UNAPSV"].Value = num;
        //ITnet2.Server.Dialogs.TableViewer.Show(cmd);

        //return null;
        return cmd.LoadToLocalDb();
    }

    /** Получаем список всех утвержденных планов и актуальный */
    public static object getApsv(string unapsv)
    {
        var builder = SqlClient.Main.CreateSelectBuilder();

        builder.From.Table.Name = "APSV";
        builder.Where.CommandText = "(STATUS = @_E OR STATUS = @_A)";
        builder.Where.Parameters.Add("_E", "E");
        builder.Where.Parameters.Add("_A", "A");
        if (unapsv.Length > 0)
        {
            builder.Where.CommandText += " AND UNAPSV > @_UNAPSV";
            builder.Where.Parameters.Add("_UNAPSV", unapsv);
        }
        builder.OrderBy.Add("APSV", "UNAPSV", true);
        builder.Fields.AddRange("APSV", new[] { "UNAPSV", "STATUS" });
        builder.Fields.AddExpression("FORMAT(ALLOCDATE, 'yyyy-MM-dd HH:mm')", "ALLOCDATE");
        builder.Fields.AddExpression("FORMAT(DATEFROM, 'yyyy-MM-dd HH:mm')", "DATEFROM");
        builder.Fields.AddExpression("FORMAT(DATETO, 'yyyy-MM-dd HH:mm')", "DATETO");

        //ITnet2.Server.Dialogs.TableViewer.Show(builder.GetCommand());
        //return null;

        return builder.GetCommand().LoadToLocalDb();
    }

    /** Получаем ALLOCDATE по номеру плана */
    public static object getAllocDate(string unapsv)
    {
        var builder = SqlClient.Main.CreateSelectBuilder();

        builder.From.Table.Name = "APSV";
        builder.Where.CommandText = "UNAPSV = @_UNAPSV";
        builder.Where.Parameters.Add("_UNAPSV", unapsv);
        builder.Fields.AddExpression("FORMAT(ALLOCDATE, 'yyyy-MM-dd HH:mm')", "ALLOCDATE");

        //ITnet2.Server.Session.InfoManager.MessageBox(builder.GetCommand().ExecScalar<string>());
        //ITnet2.Server.Dialogs.TableViewer.Show(builder.GetCommand());

        return builder.GetCommand().LoadToLocalDb();
    }
}