using ITnet2.Server.Data;
using ITnet2.Server.UserBusinessLogic._Load_To_Db;

public class Aps_Plan
{
    public static object MPSPlan(string prod_id)
    {

        var RESULT = SqlClient.Main.CreateCommand("select UNAPSV from apsv where status = @status",
            new SqlParam("status", "A")).ExecObject(new { UNAPSV = 0 });
        //ITnet2.Server.Session.InfoManager.MessageBox(RESULT.ToString()); return null;

        var sqlCmd = SqlClient.CreateCommand(
            @"SELECT DMS.UNDOC, DMS.KMAT, MAX(DMS.FIO_D) AS FIO_D, DMS.KZAJ_DB, DMS.KDMT, DMS.DDM, SUM(DMS.KOL) AS KOL
            FROM APSVMPS
            INNER JOIN DMS ON DMS.KZAJ_CR = APSVMPS.ORDERCODE
            WHERE APSVMPS.UNAPSV = @_UNAPSV AND DMS.KDMT = '_SKC' AND DMS.KZAJ_DB <> ''
            GROUP BY DMS.UNDOC, DMS.KMAT, DMS.KZAJ_DB, DMS.DDM, DMS.KDMT");

        sqlCmd.Parameters["_UNAPSV"].Value = RESULT.UNAPSV;

        string tmpTableName = Load_To_Db.LoadToDbEx(sqlCmd, DbInstance.Main);
        string tmpTableTech = Load_To_Db.getTech(DbInstance.Main);

        // в первом SELECT  получаем всю информацию о заказе + план производства
        // во втором SELECT получаем информацию по сдаче на СГП + Упаковка
        var cmd = SqlClient.CreateCommand(@"
            SELECT
                PLA.KZAJNPP,
                DMS.NOM_SERT,
                DMS.PR_DO,
                DMS.NDM,
                DMZ.KDMT,
                DMS.NPP,
                FORMAT(DMS.DLIM2, 'yyyy-MM-dd') AS TRANSPORT_DATE, --дата заказа транспорта
                FORMAT(DMS.DLIM3, 'yy-MM-dd') AS PREFERABLE_DATE, --желаемая дата в заказах
                DMS.KMAT,
                SKM.FNMAT AS TECH_STR,
                PROD.SKM AS PROD_ID,
                PROD.SKMNAIM AS PROD_NAME,
                ITKSMFAS.TECH AS TECH,
                DMS.NDM + '/' + DMS.KDS1 as LOT,
                ROUND(DMS.KOL, 2) AS KOL,
                ROUND((PLA.PL_M * KSM.KPER_N_I), 2) AS KOL_PLA,
                PLA.PL_M,
                KSM.NMAT,
                FORMAT(APSVMPS.FINISHTIME, 'yy-MM-dd') AS FINISHTIME,
                FORMAT( DMS.DLIM3, 'yy-MM-dd') AS DDM,
                CASE WHEN YEAR(PLA.DATE_DIR_Z) = 9999 THEN NULL ELSE FORMAT(PLA.DATE_DIR_Z, 'yy-MM-dd') END AS DATE_DIR_Z,
                CASE WHEN YEAR(PLA.DATE_DIR) = 9999 THEN NULL ELSE DATEDIFF(dd, PLA.DATE_DIR_Z, (PLA.D_PRPR2) ) END AS DIFF,
                PLA.PR_IZG_F,
                ORG.NORG_S AS ORG_NAME,
                KDK.FIO_OTV,
                RSS.NRSS_S AS NRSS_SHOT

            FROM APSVMPS
            LEFT JOIN PLA ON PLA.KOBJ = APSVMPS.KOBJ AND PLA.KPLT = APSVMPS.KPLT
                AND PLA.KPLV = APSVMPS.KPLV AND PLA.KZAJNPP = APSVMPS.ORDERCODE
            LEFT JOIN DMS ON DMS.UNDOC = PLA.UNDOC_Z AND DMS.NPP = PLA.NPP_Z
            LEFT JOIN DMZ ON DMZ.UNDOC = DMS.UNDOC
            LEFT JOIN KSM ON KSM.KMAT = PLA.KMAT
            LEFT JOIN SKM ON SKM.SKM = KSM.SKM
            LEFT JOIN SKM PROD ON PROD.SKM = SKM.SKM_PR
            LEFT JOIN " + tmpTableTech + @" AS ITKSMFAS ON ITKSMFAS.KMAT = DMS.KMAT
            LEFT JOIN KDK ON KDK.USERID = DMS.FIO_D
            LEFT JOIN ORG ON ORG.ORG = PLA.ORG
            LEFT JOIN RS1 ON RS1.ARSO = 'DMZ10' AND RS1.KEYVALUE = DMS.UNDOC
            LEFT JOIN RSS ON RSS.ARSO = 'DMZ10' AND RSS.ARSV = RS1.ARSV AND RSS.ARSS = RS1.ARSSCURR
                AND RSS.PR_DO=' '
            WHERE PROD.SKM = @_PROD_ID AND APSVMPS.UNAPSV = @_UNAPSV AND DMS.KDMT = '_S3C'

            UNION ALL

            SELECT
                PLA.KZAJNPP,
                DMS.NOM_SERT,
                DMS.PR_DO,
                DMS.NDM,
                STOCK.KDMT,
                DMS.NPP,
                FORMAT(DMS.DLIM2, 'yyyy-MM-dd') AS TRANSPORT_DATE, --дата заказа транспорта
                FORMAT(DMS.DLIM3, 'yy-MM-dd') AS PREFERABLE_DATE, --желаемая дата в заказах
                STOCK.KMAT,
                SKM.FNMAT AS TECH_STR,
                PROD.SKM AS PROD_ID,
                PROD.SKMNAIM AS PROD_NAME,
                NULL AS TECH,
                DMS.NDM + '/' + DMS.KDS1 as LOT,
                ROUND(STOCK.KOL, 2) AS KOL,
                ROUND((PLA.PL_M * KSM.KPER_N_I), 2) AS KOL_PLA,
                PLA.PL_M,
                KSM.NMAT,
                FORMAT(APSVMPS.FINISHTIME, 'yy-MM-dd') AS FINISHTIME,
                FORMAT(STOCK.DDM, 'yy-MM-dd') AS DDM,
                CASE WHEN YEAR(PLA.DATE_DIR_Z) = 9999 THEN NULL ELSE FORMAT(PLA.DATE_DIR_Z, 'yy-MM-dd')
                    END AS DATE_DIR_Z,
                CASE WHEN YEAR(PLA.DATE_DIR) = 9999 THEN NULL ELSE DATEDIFF(dd, PLA.DATE_DIR_Z, (PLA.D_PRPR2) )
                    END AS DIFF,
                PLA.PR_IZG_F,
                ORG.NORG_S AS ORG_NAME,
                KDK.FIO_OTV,
                RSS.NRSS_S AS NRSS_SHOT

            FROM APSVMPS
            LEFT JOIN PLA ON PLA.KOBJ = APSVMPS.KOBJ AND PLA.KPLT = APSVMPS.KPLT AND PLA.KPLV = APSVMPS.KPLV AND PLA.KZAJNPP = APSVMPS.ORDERCODE
            LEFT JOIN DMS ON DMS.UNDOC = PLA.UNDOC_Z AND DMS.NPP = PLA.NPP_Z
            LEFT JOIN DMZ ON DMZ.UNDOC = DMS.UNDOC
            LEFT JOIN " + tmpTableName + @" AS STOCK ON STOCK.KZAJ_DB = PLA.KZAJNPP
            LEFT JOIN KSM ON KSM.KMAT = STOCK.KMAT
            LEFT JOIN SKM ON SKM.SKM = KSM.SKM
            LEFT JOIN SKM PROD ON PROD.SKM = SKM.SKM_PR
            LEFT JOIN KDK ON KDK.USERID = DMS.FIO_D
            LEFT JOIN ORG ON ORG.ORG = PLA.ORG
            LEFT JOIN RS1 ON RS1.ARSO = 'DMZ10' AND RS1.KEYVALUE = DMS.UNDOC
            LEFT JOIN RSS ON RSS.ARSO = 'DMZ10' AND RSS.ARSV = RS1.ARSV AND RSS.ARSS = RS1.ARSSCURR AND RSS.PR_DO=' '
            WHERE PROD.SKM = @_PROD_ID AND APSVMPS.UNAPSV = @_UNAPSV AND DMS.KDMT = '_S3C'

            ORDER BY ORG_NAME DESC, KDMT, NDM ASC, NPP ASC, TECH DESC
        ");

        cmd.Parameters["_UNAPSV"].Value = RESULT.UNAPSV;
        cmd.Parameters["_PROD_ID"].Value = prod_id;

        //InfoManager.MessageBox(cmd.CommandText);
        //ITnet2.Server.Dialogs.TableViewer.Show(cmd); return null;

        var result = cmd.LoadToLocalDb();

        SqlClient.Main.DropTempTable(tmpTableName);
        SqlClient.Main.DropTempTable(tmpTableTech);

        return result;
    }
}
