using ITnet2.Server.Session;
using ITnet2.Server.Dialogs;
using ITnet2.Server.Data;

public class Load_To_Db
{
	// Создание временной таблицы и наполнение по запросу
	// возвращает имя временной таблицы
    public static string LoadToDbEx(SqlCmd sqlCmd, DbInstance instance)
    {
        string tableName = string.Empty;

        using (var reader = sqlCmd.ExecReader())
        {
            SqlMultiInsertBuilder builder = null;

            switch (instance)
            {
                case DbInstance.Main:
                    SqlClient.Main.CreateTempTable(out tableName, reader.ColumnsInfo);
                    builder = SqlClient.Main.CreateMultiInsertBuilder(tableName, reader.ColumnsInfo);
                    break;
                case DbInstance.Local:
                    SqlClient.Main.CreateTempTable(out tableName, reader.ColumnsInfo);
                    builder = SqlClient.Local.CreateMultiInsertBuilder(tableName, reader.ColumnsInfo);
                    break;
                default:
                    return null;
            }

            while (reader.Read())
            {
                object[] values = new object[reader.FieldCount];
                reader.GetValues(values);
                builder.AddRow(values);
            }

            builder.Exec();
        }

        return tableName;
    }

    public static bool addDataToTable(DbInstance instance, string tableName, SqlCmd sqlCmd)
    {
        using (var reader = sqlCmd.ExecReader())
        {
            SqlMultiInsertBuilder builder = null;

            switch (instance)
            {
                case DbInstance.Main:
                    builder = SqlClient.Main.CreateMultiInsertBuilder(tableName, reader.ColumnsInfo);
                    break;
                case DbInstance.Local:
                    builder = SqlClient.Local.CreateMultiInsertBuilder(tableName, reader.ColumnsInfo);
                    break;
                default:
                    return false;
            }

            while (reader.Read())
            {
                object[] values = new object[reader.FieldCount];
                reader.GetValues(values);
                builder.AddRow(values);
            }

            return builder.Exec();
        }
    }

	public static string getTech(DbInstance instance)
	{
		string sql = @"
			SELECT ITKSMFAS.KMAT, '{' + STRING_AGG('""' + ITKSMFAS.KKSMF + '"": ""' + CASE WHEN ITKSMFAS.KKSMF = 'КОНС' THEN KSMZ.NKSMZ1 ELSE KSMZ.NAIMKSMF END + '""', ', ') + '}' AS TECH 
			FROM ITKSMFAS
			LEFT JOIN KSMZ ON KSMZ.KKSMF = ITKSMFAS.KKSMF AND KSMZ.KSMZ = ITKSMFAS.KSMZ
			GROUP BY ITKSMFAS.KMAT		
		";
		
		var sqlCmd = SqlClient.CreateCommand(sql);
		string tmpTableName = Load_To_Db.LoadToDbEx(sqlCmd, instance);
		
		//ITnet2.Server.Dialogs.TableViewer.Show(SqlClient.CreateCommand("SELECT * FROM " + tmpTableName));
		
		return tmpTableName;
	}	
}