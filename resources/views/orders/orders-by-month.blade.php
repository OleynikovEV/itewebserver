@extends('skeleton.skeleton')

@section('content')
  <h2 class="text-center">{{ $title }}</h2>

  <div>
    @if ( isset($showCalendar) && $showCalendar)
      @include('skeleton.elements.filterByMonth', [
        'flTitle' => 'Дата заказов',
        'flDateTime' => $date_time
      ])
    @endif

    @if ( empty($data) )
      <h1 class="text-center mt-4">
        @if ( isset($empty) )
          {{ $empty }}
        @else
          Данные отсутствуют
        @endif
      </h1>
    @else

      <div class="mb-15" style="font-size: 12px; position: absolute; top: 0;">
        <div>21 - желаемая дата выпуска</div>
        <div class="font-bold">21 - выпуск по APS</div>
        <div class="font-bold success">21 - APS-выпуск меньше даты заказа транспорта на 2 и более дня</div>
        <div class="font-bold warning">21 - APS-выпуск меньше на 1 день или равен дате заказа транспорта</div>
        <div class="font-bold danger">21 - APS-выпуск больше даты заказа транспорта (срыв отгрузки)</div>
      </div>

      <div id="reports">
        @include('skeleton.elements.filterPanel')

        @include('skeleton.elements.navigationPanel')

        <div>
          <span class="btn-link bg_violet col-md-1" @click="filter_PREV = (filter_PREV) ? '' : '-'">Переходящие</span>
          <span class="btn-link bg-warning col-md-1" @click="filter_error_fact = !filter_error_fact">Ошибки в предъявлении</span>
          <span class="btn-link bg_success_light col-md-1" @click="filter_plan_future = !filter_plan_future">В плане на след. мес.</span>
        </div>

        <table id="data-report" class="table table-striped table-bordered table-hover table-sm">
          <thead>
            <tr>
              <th style="width: 5px">#</th>
              <th style="width: 5px">Пр</th>
              <th style="width: 95px" class="text-center">ПРО/Лот</th>
              <th style="width: 10px">№</th>
              <th style="width: 400px" class="text-center">Стандарт + характеристики</th>
              <th style="width: 50px">Стадия</th>
              <th style="width: 10px">Заказ (т)</th>
              <th style="width: 5px">План<br>Сдано</th>

              @foreach ($calendar as $day)
                @if ($day == '')
                  <th style="width: 50px" class="text-center">Нет даты выпуска по APS</th>
                @else
                  <th class="text-center">{{ $day }}</th>
                @endif
              @endforeach
            </tr>
          </thead>

          <tbody>
          <template v-for="(firm_row, firm) in filterData">
            <tr>
              <th colspan="{{ $amountDays + 8 }}" class="group">
                <span class="btn-link font-weight-bold" @click="filter_OGR_NAME = firm_row.info.ORG_NAME">
                  @{{ firm_row.info.ORG_NAME }}
                </span>
                <span style="float: right">
                  <span class="btn-link" @click="filter_FIO_OTV = firm_row.info.FIO_OTV">
                    @{{ firm_row.info.FIO_OTV.toUpperCase() }}
                  </span>
                </span>
              </th>
            </tr>

            <template v-for="(order, NDM, index) in firm_row.orders">
              <tr>
                <td rowspan="2" class="border-bottom">@{{ index + 1 }}</td>
                <td rowspan="2" class="border-bottom btn-link" @click="filter_PREV = order.PREV">
                  <span v-if="order.PREV == '-'">Пр</span>
                  <span v-else>Н</span>
                </td>

                <td rowspan="2" class="border-bottom"
                    :title="`${order.DDM} #${order.NOM_SERT}`"
                    :class="[order.DIF < 0 ? 'bg_violet' : (order.DIF > 0 ? 'bg_success_light' : '')]">
                  <span class="btn-link" @click="filter_NDM = order.NDM">@{{ order.LOT }}</span>
                </td>
                <td rowspan="2" class="border-bottom">@{{ order.NPP }}</td>
                <td rowspan="2" class="border-bottom">
                  <template v-for="(tech, key) in order.TECH1">
                    <span :class="{'text-danger': (key == 'ПОКР'), 'text-success': (key == 'ГРП1'), 'text-violet': (key == 'ДИА1'), 'btn-link': true}"
                          v-if="tech.trim() != '/' && tech.length > 0"
                          @click="link_filter(tech, ( parseInt(key) || key == 0 ? 'NMAT' : key))">
                      @{{ tech }}
                    </span>
                    <span v-else>@{{ tech }}</span>
                  </template>
                </td>
                <td rowspan="2" class="text-center border-bottom" :title="`${order.NRSS_SHOT} - ${order.PLAN}`">
                  <span class="btn-link" @click="filter_DONE = order.DONE">@{{ order.DONE }}</span>
                </td>

                <td rowspan="2" class="text-center border-bottom"
                    :title="order.TOTAL_FACT"
                    :class="[ (order.INCORRECT_FACT == true) ? 'bg-warning' : (order.PLAN_MONTH_FUTURE == true) ? 'bg_success_light' : '']">
                  @{{ order.KOL.toFixed(2).replace('.', ',') }}
                </td>

                <td>План</td>
                <template v-for="(day, date) in calendar">
                  <td class="text-center">
                    <span class="font-bold"
                      :class="order.FINISHTIME | dateNotification(order.TRANSPORT_DATE)"
                      :title="order.TRANSPORT_DATE | formatDate"
                      v-if="_.has(order.CAL, '_S3C') && _.has(order.CAL._S3C, date)">
                      @{{ order.CAL._S3C[date].toFixed(2).replace('.', ',') }}
                    </span>
                    <div v-if="_.has(order.CAL, 'PREFERABLE_DATE') && _.has(order.CAL.PREFERABLE_DATE, date)">
                      @{{ order.CAL.PREFERABLE_DATE[date].toFixed(2).replace('.', ',') }}
                    </div>
                  </td>
                </template>
              </tr>
              <tr>
                <td class="text-info border-bottom">Сдано</td>
                <template v-for="(day, date) in calendar">
                  <td class="text-info border-bottom">
                    <span v-if="_.has(order.CAL, '_SKC') && _.has(order.CAL._SKC, date)">
                      @{{ order.CAL._SKC[date].toFixed(2).replace('.', ',') }}
                    </span>
                  </td>
                </template>
              </tr>
            </template>
          </template>
          </tbody>

          <tfoot>
            <tr class="bg_black text-white">
              <th colspan="6" rowspan="2" class="border-bottom align-middle">
                <div class="pl-4">
                  Итого заказов @{{ total_order.toFixed(2).replace('.', ',') }} <br>
                  Итого сдано @{{ total_fakt.toFixed(2).replace('.', ',') }}<br>
                  Остаток к пр-ву @{{ (total_order - total_fakt).toFixed(2).replace('.', ',') }}<br>
                  Итого включено в план @{{ (total_plan - total_neobrab).toFixed(2).replace('.', ',') }} <br>
                  Итого без даты выпуска по APS @{{ total_neobrab.toFixed(2).replace('.', ',') }} <br>
                  План на след. мес. @{{ total_plan_future.toFixed(2).replace('.', ',') }} <br>
                </div>
              </th>
              <th colspan="1" rowspan="2" class="border-bottom align-middle text-center">
                @{{ total_order.toFixed(2).replace('.', ',') }}
              </th>
              <th class="border-bottom text-center">План</th>
              <template v-for="(data, day) in calendar">
                <th class="text-center" v-if="_.has(total, '_S3C') && _.has(total._S3C, day)">
                  @{{ total._S3C[day].toFixed(2).replace('.', ',') }}
                </th>
                <th v-else></th>
              </template>
            </tr>

            <tr class="bg_black">
              <th  class="text-info_light border-bottom">Сдано</th>
              <template v-for="(data, day) in calendar">
                <th class="text-info_light"
                    v-if="_.has(total, '_SKC') && _.has(total._SKC, day)">
                  @{{ total._SKC[day].toFixed(2).replace('.', ',') }}
                </th>
                <th v-else></th>
              </template>
            </tr>
          </tfoot>

        </table>
      </div>
    @endif
  </div>

  @if (! empty($data) )
    <script>
      'use strict'

      const vm = new Vue({
        el: '#reports',
        data() {
          return {
            dt: @json($data),
            calendar: @json($calendar),
            total: {},
            total_order: 0,
            total_plan: 0,
            total_fakt: 0,
            total_neobrab: 0,
            total_plan_future: 0,

            filter_OGR_NAME: '',
            filter_NDM: '',
            filter_FIO_OTV: '',
            filter_DONE: '',
            filter_PREV: '',
            filter_month_num: '',
            filters_num_client: '',
            filter_plan_future: false,
            filter_error_fact: false,
            filters: {},
          }
        },
        computed: {
          filterData: function() {
            let tmpTotal = {
              '_S3C': {},
              '_SKC': {},
            }
            this.total_order = 0
            this.total_plan = 0
            this.total_fakt = 0
            this.total_neobrab = 0
            this.total_plan_future = 0

            let data = _.map( this.dt, (firm, fname) => {

              let ffirm = _.filter( firm.orders, (order) => {
                let tech = [order.TECH1]
                return (_.findIndex(tech, this.filters) > -1)
                  && (this.filter_error_fact == false || (this.filter_error_fact == true && order.INCORRECT_FACT == true))
                  && (this.filter_plan_future == false || (this.filter_plan_future == true && order.PLAN_MONTH_FUTURE == true))
                  && (this.filter_OGR_NAME.length == '' || this.filter_OGR_NAME == order.ORG_NAME)
                  && (this.filter_NDM.length == '' || this.filter_NDM == order.NDM)
                  && (this.filter_FIO_OTV.length == '' || this.filter_FIO_OTV == order.FIO_OTV)
                  && (this.filter_DONE.length == '' || this.filter_DONE == order.DONE)
                  && (this.filter_PREV.length == '' || this.filter_PREV == order.PREV)
                  && (this.filters_num_client.length == '' || order.NOM_SERT.indexOf(this.filters_num_client) > -1)
              })

              let tmp = {
                'info': firm.info,
              }

              if ( _.isUndefined(tmp.orders) ) {
                tmp.orders = {}
                _.forEach(ffirm, lot => {
                  this.total_order += lot.KOL
                  if ( lot.PLAN_MONTH_FUTURE ) {
                    this.total_plan_future += lot.KOL
                  }
                  _.forEach(lot.CAL, (item, key) => { // расчет ИТОГО
                    if (key != 'PREFERABLE_DATE') {
                      _.forEach(item, (kol, day) => {
                        if (!_.has(tmpTotal[key], day)) {
                          tmpTotal[key][day] = 0
                        }
                        tmpTotal[key][day] += kol
                        if (key == '_S3C') {
                          this.total_plan += kol
                        } else if (key == '_SKC') {
                          this.total_fakt += kol
                        }
                      })
                    }
                  })
                  tmp.orders[lot.NUM] = lot
                })
              }
              return tmp
            })

            data = _.filter(data, item => {
              return ! _.isEmpty(item.orders)
            })

            this.total = tmpTotal
            this.total_neobrab = (this.total._S3C.hasOwnProperty('')) ? this.total._S3C[''] : 0
            return data
          },
          amount: function() {
            return _.sumBy( this.filterData, item => _.size(item.orders) )
          }
        },
        filters: {
          formatDate: function(value) {
            if (value.length > 0) {
              const d = new Date(value);
              return 'Заказ транспорта на ' + d.toLocaleDateString('ru-RU');
            }

            return '';
          },

          dateNotification: function(finish, transport) {
            const dFinish = new Date(finish);
            const dTransport = new Date(transport);
            const diferent = (dTransport - dFinish) / (1000 * 60 *60 * 24);
            let classNotifi = '';

            switch (true) {
              case diferent >= 2:
                classNotifi = 'success';
                break;
              case diferent < 2 && diferent >= -1:
                classNotifi = 'warning';
                break;
              case diferent < -1:
                classNotifi = 'danger';
                break;
              default:
                classNotifi = '';
            }

            return classNotifi;
          },
        },
        methods: {
          clearFilter(key) {
            Vue.delete(this.filters, key)
          },
          link_filter(val, field) {
            this.$set( this.filters, field, val )
          },
          exportToExcel() {
            $('#reports').tableExport({
              type:'excel',
              bootstrap: true,
              headings: true,
              footers: true,
              formats: ["xlsx", "csv", "txt"],
              fileName: "Report",
            })
          },
          getMonthName(num) {
            let month = {
              1: 'Январь',
              2: 'Февраль',
              3: 'Март',
              4: 'Апрель',
              5: 'Май',
              6: 'Июнь',
              7: 'Июль',
              8: 'Август',
              9: 'Сентябрь',
              10: 'Октябрь',
              11: 'Ноябрь',
              12: 'Декабрь',
            }
            return month[ parseInt(num) ]
          },
        }
      })
    </script>
  @endif
@endsection