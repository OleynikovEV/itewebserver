@extends('skeleton.skeleton')

@section('content')
<main id="main2" class="row col-12 pb-4">
    {{-- панель с версиями планов --}}
    <div class="col-2 position-fixed overflow-auto" style="height: 850px">
        <div class="text-center text-uppercase font-weight-bold">план</div>
        <table class="table-sm table-bordered table-hover table-striped w-100">
            <tr v-for="v, index in apsv" :class="[(v.upload) ? 'text-danger' : '']">
                <th class="text-center">@{{ v.UNAPSV }}</th>
                <td class="text-center">@{{ v.ALLOCDATE }}</td>
                <td class="text-center"><template v-if="v.STATUS == 'A'">A</template></td>
                <td class="text-center">
                    <button class="btn btn-link btn-outline-light" :disabled="disabled"
                            @click="savePlan(v.UNAPSV, index)" :hidden="(v.upload) ? true : false">Перекачать</button>
                </td>
            </tr>
        </table>
    </div>

    {{-- информационная планов --}}
    <div class="col-8 col offset-2 overflow-auto">
        <div class="text-center text-uppercase font-weight-bold">событие</div>
        <div v-for="row in info">@{{ row }}</div>
    </div>

    {{-- факт планов --}}
    <div class="col-2 col overflow-auto">
        <div class="text-center text-uppercase font-weight-bold">факт</div>
        <table class="table-sm table-bordered table-hover table-striped w-100">
            <tr v-for="m, ind in dateLoaded">
                <th>@{{ ind }}</th>
                <td>@{{ (m.loaded) ? 1 : 0 }}</td>
                <td>
                    <button class="btn btn-link btn-outline-light"
                            :disabled="m.loaded" @click="saveFact(ind)">Перекачать</button>
                </td>
            </tr>
        </table>
    </div>
</main>

<script>
    // arrayReverseObj = (obj) => Object.keys(obj).reverse().map(key=> ({
    //         UNAPSV: obj[key].UNAPSV,
    //         STATUS: obj[key].STATUS,
    //         ALLOCDATE: obj[key].ALLOCDATE,
    //         DATEFROM: obj[key].DATEFROM,
    //         DATETO: obj[key].DATETO,
    //         LOADED: obj[key].LOADED
    //     }) );

    const vm = new Vue({
        el: '#main2',
        data() {
            return {
                apsv: @json($unapsv),
                {{--nonorm: @json($nonorm),--}}
                dl: @json($factLoaded),
                url: '{{ url()->to('/') }}',
                disabled: false,
                info: [],
                dateLoaded: {},
            }
        },
        beforeMount() {
            for (m = 1; m <= 12; m++) {
                this.$set(this.dateLoaded, m, {
                    loaded: _.has(this.dl, m),
                });
            }
        },
        mounted() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        },
        methods: {
            savePlan(unapsv, index) {
                this.disabled = true;
                let n = new Date();
                this.info.push(`${n.getHours()}:${n.getMinutes()}:${n.getSeconds()} - Начало загрузки плана, версии ${unapsv} - ${this.apsv[index].ALLOCDATE}`);
                vm.$set(vm.apsv[index], 'upload', true);
                $.post(`${this.url}/import/importAPSPlan/${unapsv}`, function(data) {
                    let result = '';
                    if (data[0] == true) {
                        result = 'успешно';
                    } else {
                        result = 'с ошибками';
                    }
                    vm.disabled = false;
                    n = new Date();
                    vm.info.push(`${n.getHours()}:${n.getMinutes()}:${n.getSeconds()} - Загрузка палана завершена ${result}`);
                });
            },
            saveFact(m) {
                let n = new Date();
                this.info.push(`${n.getHours()}:${n.getMinutes()}:${n.getSeconds()} - Начало загрузки факта за месяц ${m}`);
                $.post(`${this.url}/import/importFact/2019/${m}`, function(data) {
                    if (data[0] > 0) {
                        result = 'успешно. Перекачено строк ' + data[0];
                        vm.dateLoaded[m].loaded = true;
                    } else {
                        result = 'с ошибками';
                        vm.dateLoaded[m].loaded = false;
                    }
                    n = new Date();
                    vm.info.push(`${n.getHours()}:${n.getMinutes()}:${n.getSeconds()} - Загрузка факта завершена ${result}`);
                });
            }
        }
    });
</script>
@endsection