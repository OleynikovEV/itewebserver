<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="csrf-token" content="{{ Session::token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>Стальканат</title>

    <!-- Favicon -->
    <link rel="icon" href="{{ url('favicon.ico') }}">

    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="{{ url('style.css?1.0.4') }}">

    <!-- Fullscreen menu -->
    <link rel="stylesheet" href="{{ url('css/fullscreen-menu.css?1.0.1') }}">

    <!-- jQuery-3.4.1 js -->
    <script src="{{ url('js/jquery.min.js') }}"></script>
    <!-- Bootstrap js -->
    <script src="{{ url('js/bootstrap.min.js') }}"></script>
    <!-- jQuery DataTable js -->
    <script src="{{ url('js/jquery.dataTables.min.js') }}"></script>
    <!-- Vue js -->
    @if ( env('APP_ENV') == 'dev' || env('APP_ENV') == 'local')
        <script src="{{ url('js/vue.js') }}"></script>
    @endif
    @if ( env('APP_ENV') == 'prod')
        <script src="{{ url('js/vue.min.js') }}"></script>
    @endif
    <!-- Lodash js -->
    <script src="{{ url('js/lodash.min.js') }}"></script>
    <!-- Air-DatePicker js -->
    <script src="{{ url('js/datepicker.min.js') }}"></script>
    <!-- Export to Excel js -->
    {{--<script src="{{ url('js/toExcel.min.js') }}"></script>--}}
    <!-- Active js -->
    <script src="{{ url('js/active.js?1.0.7') }}"></script>
    <script src="{{ url('js/function.js?1.0.0') }}"></script>

    @stack('scripts')

    <style>
        @media all and (max-width: 750px) {
            #main {
                margin-top: 130px;
            }
        }
        @media all and (max-width: 700px) {
            #main {
                margin-top: 50px;
            }
        }
    </style>
</head>

<body>

@include('skeleton.header')

<div class="container-fluid pt-md-5">
    <div id="row" class="row show justify-content-md-center">

        @include('skeleton.menu-left')

        @if ( Route::currentRouteName() == 'home' )
            <main id="main" class="col-lg-10 px-4">
        @elseif (Request()->route()->getPrefix() == '/maps')
            <main id="main" class="col-lg-11 px-4 h-75">
        @elseif (Request()->route()->getPrefix() == '/dashboard' || Request()->route()->getPrefix() == '/import')
            <main id="main" class="col-lg-12 px-4">
        @else
            <main id="main" class="col-lg-auto px-4">
        @endif
            @yield('content')
        </main>

    </div>
</div>

</body>

</html>
