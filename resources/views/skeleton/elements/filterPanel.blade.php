<div class="col-md-12 alert alert-secondary font-weight-bold mb-2" style="margin: 0px">
    <span>Фильтр: </span>

    <span class="alert alert-success" v-if="this.$data.hasOwnProperty('filter_CEH') && filter_CEH.length > 0">
        @{{ filter_CEH }}
        <i class="fa fa-times btn-link" @click="filter_CEH = ''"></i>
    </span>

    {{-- фильтруем заказы где ошибки  --}}
    <span class="alert alert-success" v-if="this.$data.hasOwnProperty('filter_error_fact') && filter_error_fact == true">
        Ошибки в предъявлении
        <i class="fa fa-times btn-link" @click="filter_error_fact = false"></i>
    </span>

    {{-- фильтруем заказы запланированные на след. месяц --}}
    <span class="alert alert-success" v-if="this.$data.hasOwnProperty('filter_plan_future') && filter_plan_future == true">
        В плане на след. мес.
        <i class="fa fa-times btn-link" @click="filter_plan_future = false"></i>
    </span>

    <span class="alert alert-success" v-if="this.$data.hasOwnProperty('filter_month_num') && filter_month_num.length > 0">
        @{{ getMonthName(filter_month_num) }}
        <i class="fa fa-times btn-link" @click="filter_month_num = ''"></i>
    </span>

    <span class="alert alert-success" v-if="this.$data.hasOwnProperty('filter_PREV') && filter_PREV.length > 0">
        <span v-if="filter_PREV == '-'">Переходящие</span>
        <span v-else>Заказы месяца</span>
        <i class="fa fa-times btn-link" @click="filter_PREV = ''"></i>
    </span>

    <span class="alert alert-success" v-if="this.$data.hasOwnProperty('filter_OGR_NAME') && filter_OGR_NAME.length > 0">
        @{{ filter_OGR_NAME }}
        <i class="fa fa-times btn-link" @click="filter_OGR_NAME = ''"></i>
    </span>

    <span class="alert alert-success" v-if="this.$data.hasOwnProperty('filter_NDM') && filter_NDM.length > 0">
        @{{ filter_NDM }}
        <i class="fa fa-times btn-link" @click="filter_NDM = ''"></i>
    </span>

    <span class="alert alert-success" v-if="this.$data.hasOwnProperty('filter_DONE') && filter_DONE.length > 0">
        @{{ filter_DONE }}
        <i class="fa fa-times btn-link" @click="filter_DONE = ''"></i>
    </span>

    <span class="alert alert-success" v-if="this.$data.hasOwnProperty('filter_FIO_OTV') && filter_FIO_OTV.length > 0">
        @{{ filter_FIO_OTV }}
        <i class="fa fa-times btn-link" @click="filter_FIO_OTV = ''"></i>
    </span>
    {{-- кнопка очистки фильтра --}}
    <template v-if="this.$data.hasOwnProperty('filters')">
        <span v-for="(item, key) in filters" v-if="item.length > 0" class="alert alert-success mr-1">
            @{{item}} <i class="fa fa-times btn-link" @click="clearFilter(key)"></i>
        </span>
    </template>
    {{-- поиск по клиенту --}}
    <template v-if="this.$data.hasOwnProperty('filters_num_client')">
        <div class="input-group" style="width: 250px; float: right; top:-8px; right: -15px;">
            <input type="text" class="form-control" placeholder="номер заказа клиента" v-model="filters_num_client">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">#</span>
            </div>
        </div>
    </template>
</div>
