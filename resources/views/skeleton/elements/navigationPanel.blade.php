@push('scripts')
    <script src="{{ url('js/libs/FileSaver/FileSaver.min.js') }}"></script>
    <script src="{{ url('js/libs/js-xlsx/xlsx.core.min.js') }}"></script>
    <script src="{{ url('js/libs/tableExport.min.js') }}"></script>
@endpush

<div class="col-md-12 font-weight-bold">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        Количество записей: @{{ amount }}
        <div class="btn-toolbar mb-2 mb-md-0">
            <button class="btn btn-outline-success" @click="exportToExcel">Excel</button>
        </div>
    </div>
</div>