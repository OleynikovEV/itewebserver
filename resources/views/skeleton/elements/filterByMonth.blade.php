<div class="row mb-2 justify-content-md-center">
    <h4 class="mr-2">{{ $flTitle }}</h4>
    <input id="dpmonth"
           type="text"
           class="datepicker-here form-control col-md-2"
           data-min-view="months"
           data-view="months"
           data-date-format="MM yyyy" />
    <a href="#" id="btnSelect" class="btn btn-secondary ml-2">Выбрать</a>
    <a href="#" id="btnReload" style="position: absolute; right: 30px;" class="btn btn-outline-danger">Reload</a>
</div>

<script>
    $(function() { // устанавливаем дату
        let now = new Date('{{ $flDateTime }}') // устанавливаем дату
        window.dpMonth.data('datepicker').selectDate(now)
    })
</script>