@extends('skeleton.skeleton')

@section('content')
    <h2 class="text-center">{{ $title }}</h2>

    <div>
        @if ( isset($showCalendar) && $showCalendar)
            @include('skeleton.elements.filterByMonth', [
                'flTitle' => 'Дата заказов',
                'flDateTime' => $date_time
            ])
        @endif

        @if ( empty($data) )
            <h1 class="text-center mt-4">
                @if ( isset($empty) )
                    {{ $empty }}
                @else
                    Данные отсутствуют
                @endif
            </h1>
        @else
            <div id="reports">
                @include('skeleton.elements.filterPanel')

                @include('skeleton.elements.navigationPanel')

                <table id="data-report" class="table table-striped table-bordered table-hover table-sm">
                    <thead>
                        <tr>
                            <th rowspan="2" style="width: 10px">#</th>
                            <th rowspan="2" style="width: 95px" class="text-center">ПРО/Лот</th>
                            <th rowspan="2" style="width: 10px">№</th>
                            <th rowspan="2" style="width: 400px" class="text-center">Стандарт + характеристики</th>
                            <th rowspan="2" style="width: 10px">&nbsp;</th>
                            <th rowspan="2" style="width: 50px">&nbsp;</th>

                            @foreach ($month as $m)
                                <th colspan="{{ $m['colspan']  }}" class="text-center">{{ $m['name'] }}</th>
                            @endforeach
                        </tr>
                        <tr>
                            @foreach ($calendar as $day)
                                @if ($day == '')
                                    <th>Не обраб.</th>
                                @else
                                    <th class="text-center">{{ $day['day'] }}</th>
                                @endif
                            @endforeach
                        </tr>
                    </thead>

                    <tbody>
                        <template v-for="(firm_row, firm) in filterData">
                            <tr>
                                <th colspan="50" class="group">
                                    <span class="btn-link font-weight-bold" @click="filter_OGR_NAME = firm_row.info.ORG_NAME">@{{ firm_row.info.ORG_NAME }}</span>
                                    <span style="float: right">
                                            <span class="btn-link" @click="filter_FIO_OTV = firm_row.info.FIO_OTV">@{{ firm_row.info.FIO_OTV.toUpperCase() }}</span>
                                        </span>
                                </th>
                            </tr>

                            <template v-for="(order, NDM, index) in firm_row.orders">
                                <tr>
                                    <td rowspan="1" class="border-bottom">@{{ index + 1 }}</td>
                                    <td rowspan="1" class="border-bottom">
                                        <span class="btn-link" @click="filter_NDM = order.NDM">@{{ order.LOT }}</span>
                                    </td>
                                    <td rowspan="1" class="border-bottom">@{{ order.NPP }}</td>
                                    <td rowspan="1" class="border-bottom">
                                        <template v-for="(tech, key) in order.TECH1">
                                                <span :class="{'text-danger': (key == 'ПОКР'), 'text-success': (key == 'ГРП1'), 'text-violet': (key == 'ДИА1'), 'btn-link': true}"
                                                      v-if="tech.trim() != '/' && tech.length > 0"
                                                      @click="link_filter(tech, ( parseInt(key) || key == 0 ? 'NMAT' : key))">
                                                    @{{ tech }}
                                                </span>
                                            <span v-else>@{{ tech }}</span>
                                        </template>
                                    </td>
                                </tr>
                            </template>
                        </template>
                    </tbody>

                    <tfoot>
                        <tr>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        @endif
    </div>

    @if (! empty($data) )
    <script>
        // КОД  tmp.orders[lot.NUM] = lot - по какому полю должны грппироваться заказы
        // это поле должно быть идентицным полю в php $id = $row->NUM;
        'use strict'

        const vm = new Vue({
            el: '#reports',
            data() {
                return {
                    dt: @json($data),
                    total: {},

                    filter_OGR_NAME: '',
                    filter_NDM: '',
                    filter_FIO_OTV: '',
                    filters: {},
                }
            },
            computed: {
                filterData: function() {
                    let tmpTotal = {
                        '_S3C': {},
                        '_SKC': {},
                        '_WEIGHQ': {}
                    }
                    let data = _.map( this.dt, (firm, fname) => {

                        let ffirm = _.filter( firm.orders, (order) => {
                            let tech = [order.TECH1]
                            return (_.findIndex(tech, this.filters) > -1)
                                && (this.filter_OGR_NAME.length == '' || this.filter_OGR_NAME == order.ORG_NAME)
                                && (this.filter_NDM.length == '' || this.filter_NDM == order.NDM)
                                && (this.filter_FIO_OTV.length == '' || this.filter_FIO_OTV == order.FIO_OTV)
                        })

                        let tmp = {
                            'info': firm.info,
                        }

                        if ( _.isUndefined(tmp.orders) ) {
                            tmp.orders = {}
                            _.forEach(ffirm, lot => {
                                _.forEach(lot.CAL, (item, key) => { // расчет ИТОГО
                                    _.forEach(item, (kol, day) => {
                                        if ( ! _.has(tmpTotal[key], day)) {
                                            tmpTotal[key][day] = 0
                                        }
                                        tmpTotal[key][day] += kol
                                    })
                                })
                                tmp.orders[lot.NUM] = lot
                            })
                        }
                        return tmp
                    })

                    data = _.filter(data, item => {
                        return ! _.isEmpty(item.orders)
                    })

                    this.total = tmpTotal
                    return data
                },
                amount: function() {
                    return _.sumBy( this.filterData, item => _.size(item.orders) )
                }
            },
            methods: {
                clearFilter(key) {
                    Vue.delete(this.filters, key)
                },
                link_filter(val, field) {
                    this.$set( this.filters, field, val )
                },
                exportToExcel() {
                    $('#mpsplan').tableExport({
                        type:'excel',
                        bootstrap: true,
                        headings: true,
                        footers: true,
                        formats: ["xlsx", "csv", "txt"],
                        fileName: "Report",
                    })
                }
            }
        })
    </script>
    @endif
@endsection