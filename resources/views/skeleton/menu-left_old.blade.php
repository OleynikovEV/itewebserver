<div id="nav-menu" class="nav-menu m-close col-12">
    <h2 class="text-center"> Отчеты </h2>
    <div class="index-menu"> {!! $menu !!} </div>
    <style>
        .index-menu ul {
            margin-top: 5px;
        }
        .index-menu > ul {
            float: left;
        }
        .index-menu .parent { font-weight: bold; }
        .index-menu > ul li span:before,
        .index-menu > ul li a:before {
            content: "-";
            text-indent: -5px;
            padding-right: 10px;
        }
        .index-menu > ul a {
            margin: 20px;
            color: #007bff;
        }
    </style>
</div>
