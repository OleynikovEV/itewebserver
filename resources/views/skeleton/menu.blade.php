<div class="index-menu">
    <ul>
        <li>
            <span class="parent">Производство</span>
            <ul>
                <li>
                    <span>График предъявления (MPS)</span>
                    <ul>
                        <li>
                            <span>
                                <a href="{{ route('production.aps-plan.report', 'PF') }}">Фибра (PF)</a>
                                (<a href="{{ route('production.aps-plan.report', ['PF', 10500]) }}">ЦСП</a>
                                <a href="{{ route('production.aps-plan.report', ['PF', 10400]) }}">СПКЦ</a>)
                            </span>
                        </li>
                        <li>
                            <span>
                                <a href="{{ route('production.aps-plan.report', 'PS') }}">Пряди арматурные (PS)</a>
                                (<a href="{{ route('production.aps-plan.report', ['PS', 10500]) }}">ЦСП</a>
                                <a href="{{ route('production.aps-plan.report', ['PS', 10400]) }}">СПКЦ</a>)
                            </span>
                        </li>
                        <li>
                            <span>
                                <a href="{{ route('production.aps-plan.report', 'PW') }}">Проволока (PW)</a>
                                (<a href="{{ route('production.aps-plan.report', ['PW', 10500]) }}">ЦСП</a>
                                <a href="{{ route('production.aps-plan.report', ['PW', 10400]) }}">СПКЦ</a>)
                            </span>
                        </li>
                    </ul>
                </li>
                <li>
                    <span>Факт предъявления (по месяцам)</span>
                    <ul>
                        <li>
                            <span>
                                <a href="{{ route('production.fact.report', 'PF') }}">Фибра (PF)</a>
                            </span>
                        </li>
                        <li>
                            <span>
                                <a href="{{ route('production.fact.report', 'PS') }}">Пряди арматурные (PS)</a>
                            </span>
                        </li>
                        <li>
                            <span>
                                <a href="{{ route('production.fact.report', 'PW') }}">Проволока (PW)</a>
                            </span>
                        </li>
                    </ul>
                </li>
                <li>
                    <span>Список заказов по месяцам (+факт)</span>
                    <ul>
                        <li>
                            <span>
                                <a href="{{ route('orders.ordersByMonth', 'PF') }}">Фибра (PF)</a>
                            </span>
                        </li>
                        <li>
                            <span>
                                <a href="{{ route('orders.ordersByMonth', 'PS') }}">Пряди арматурные (PS)</a>
                            </span>
                        </li>
                        <li>
                            <span>
                                <a href="{{ route('orders.ordersByMonth', 'PW') }}">Проволока (PW)</a>
                            </span>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
    </ul>
</div>

<style>
    .index-menu ul {
        margin-top: 5px;
    }
    .index-menu > ul {
        float: left;
    }
    .index-menu .parent { font-weight: bold; }
    .index-menu > ul li span:before {
        content: "-";
        text-indent: -5px;
        padding-right: 10px;
    }
    .index-menu > ul a {
        margin: 20px;
        color: #007bff;
    }
</style>
