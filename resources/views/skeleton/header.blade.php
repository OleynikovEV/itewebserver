<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
    <div class="col-lg-2 col-md-11 col-sm-11 d-flex justify-content-lg-start justify-content-md-between justify-content-sm-between">
        <a class="navbar-brand mr-lg-4" href="{{ route('home') }}">Стальканат</a>
        <a rel="#" id="toggle" class="bars mt-2"> <i class="fa fa-bars fa-2x"></i> </a>
    </div>
</nav>
