<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ url('css/app.css') }}">
        <title>Laravel</title>

    </head>
    <body>
        <div id="app"></div>
        <script src="{{ url('js/manifest.js') }}" type="text/javascript"></script>
        <script src="{{ url('js/vendor.js') }}" type="text/javascript"></script>
        <script src="{{ url('js/app.js') }}" type="text/javascript"></script>
    </body>
</html>
