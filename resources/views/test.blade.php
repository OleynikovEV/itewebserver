@extends('skeleton.skeleton')

@section('content')
    </div>

    <table class="table table-bordered table-striped table-hover">
    @foreach ($data as $machine)
        @foreach ($machine as $row)
        <tr>
            <td>{{  isset($row['RCENTER_NAME']) ? $row['RCENTER_NAME'] : null }}</td>
            <td>{{  isset($row['INTERVAL_NAME']) ? $row['INTERVAL_NAME'] : null }}</td>
            <td>{{  isset($row['DATETIME_START']) ? $row['DATETIME_START'] : null }}</td>
            <td>{{  isset($row['DATETIME_FINISH']) ? $row['DATETIME_FINISH'] : null }}</td>
            <td>{{  $row['KZAJNPP'] }}</td>
        </tr>
        @endforeach
    @endforeach
    </table>

@endsection
