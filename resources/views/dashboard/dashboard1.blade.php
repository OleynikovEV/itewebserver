@extends('skeleton.skeleton')

@section('content')

    <div class="row" id="navigation">
        <div class="col-lg-3 col-sm-12 col-md-12" style="margin-top: 6px;">
            <div class="btn-toolbar text-uppercase" role="toolbar">
                <div class="btn-group mr-2 font-weight-bold text-info" role="group">
                    <span style="vertical-align: -7px;margin-top: 5px;">период:</span>
                </div>

                <div class="btn-group btn-group-sm" role="group" style="margin-right: 5px;">
                    {{--<input type="button" class="btn btn-outline-info" id="dp_year" data-min-view="years" data-view="years" placeholder="&#61447;">--}}
                    <input type="hidden" class="btn btn-outline-info" id="dp_year" data-min-view="years" data-view="years" placeholder="&#61447;">
                    <button type="button" class="btn btn-outline-info" id="btn_select_year"><i class="fa fa-calendar"></i></button>
                    <button type="button" class="btn btn-outline-info" id="sel_year"
                            @click="selectPeriod('sel_year', '{{ $sel_year }}')"
                            :class="{ 'btn-outline-info-hover': (select_period == 'sel_year') }">{{ $sel_year }}</button>
                </div>

                <div class="btn-group btn-group-sm" role="group" style="margin-right: 5px;">
                    <input type="hidden" id="dp_month" data-min-view="months" data-view="months">
                    <button type="button" class="btn btn-outline-success" id="btn_select_month"><i class="fa fa-calendar"></i></button>
                    <button type="button" class="btn btn-outline-success" id="sel_month"
                            @click="selectPeriod('sel_month', '{{ $sel_month }}')"
                            :class="{ 'btn-outline-success-hover': (select_period == 'sel_month') }">{{ $sel_month }}</button>
                </div>

                <div class="btn-group btn-group-sm" role="group">
                    <input type="hidden" id="dp_day">
                    <button type="button" class="btn btn-outline-dark" id="btn_select_day"><i class="fa fa-calendar"></i></button>
                    <button type="button" class="btn btn-outline-dark" id="sel_day"
                            @click="selectPeriod('sel_day', '{{ $sel_day }} (с 07:30 - 07:30)')"
                            :class="{ 'btn-outline-dark-hover': (select_period == 'sel_day') }">{{ $sel_day }} (с 07:30 - 07:30)</button>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-sm-12 col-md-12">
            <h2 class="text-uppercase" v-if="bad_param == false">анализ производства за <span class="font-weight-bold">@{{ text_period }}</span></h2>
            <h2 class="text-uppercase" v-else>период отчетности указан неверно <span class="font-weight-bold">@{{ text_period }}</span></h2>
        </div>
    </div>

    <div class="row">
        {{-- ЛЕВАЯ ЧАСТЬ --}}
        <div class="col-lg-10 col-sm-12 col-md-12">

            <div class="row">

                {{-- ВЫПОЛНЕНИЕ ПЛАНА --}}
                <div class="col-lg-6 col-sm-12 border">
                    <div class="text-center  font-weight-bolder text-uppercase">выполнение плана</div>

                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>стан</th>
                                <th>план</th>
                                <th>факт</th>
                                <th>брак</th>
                                <th>Откл абс</th>
                                <th>Откл относит</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $num => $row)
                            <tr>
                                <td>{{ ++$num }}</td>
                                <td>{{ $row['NAME'] }}</td>
                                <td class="text-center">{{ round($row['KOL_PLAN'], 2) }}</td>
                                <td class="text-center">{{ round($row['KOL_FACT'], 2) }}</td>
                                <td class="text-center">{{ round($row['KOL_DEFECT'], 2) }}</td>
                                <td class="text-center">{{ round($row['KOL_PLAN'] - $row['KOL_FACT'], 2) }}</td>
                                <td class="text-center">{{ round(($row['KOL_PLAN'] - $row['KOL_FACT'])/$row['KOL_PLAN'], 2) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-6 col-sm-12 border">
                    <div class="text-center  font-weight-bolder text-uppercase">работа оборудования</div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-sm-12 border">
                    <div class="text-center  font-weight-bolder text-uppercase">другое</div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-sm-12 border">
                    <div class="text-center  font-weight-bolder text-uppercase">топ простоев</div>
                </div>
            </div>

        </div>

        {{-- ПРАВАЯ ЧАСТЬ --}}
        <div class="col-lg-2 col-sm-12 col-md-12 border">
            <div class="text-center  font-weight-bolder text-uppercase">забраковка</div>
        </div>
    </div>

    <style>
        .table > thead > tr > th { text-align: center; }
        .visibility { display: none; }
        /*.input-datepicker*/
        .datepicker-here {
            outline-width: 1px;
            border: 0;
            outline-style: outset;
            padding: 0px 5px;
            width: 100px;
            cursor: pointer;
        }
        .-from-bottom-, .-from-bottom- {
            top: 95px!important;
        }
        .btn-outline-info-hover {
            color: #fff;
            background-color: #17a2b8;
            border-color: #17a2b8;
        }
        .btn-outline-success-hover {
            color: #fff;
            background-color: #28a745;
            border-color: #28a745;
        }
        .btn-outline-dark-hover {
            color: #fff;
            background-color: #343a40;
            border-color: #343a40;
        }
    </style>
    <script>
        $(function() {
            let dp_year = $('#dp_year').datepicker({
                class: 'btn btn-outline-info',
                autoClose: true,
                toggleSelected: false,
                language: 'ru',
                view: 'years',
                dateFormat: 'yyyy',
                // range: true,
                onSelect: function onSelect(fd, date) {
                    let year = date.getFullYear();
                    window.location.href = `http://${window.location.hostname}/dashboard/dashboard1/${year}`;
                },
                onShow: function(dp, animationCompleted){
                    if (! animationCompleted) {
                        action = dp.$datepicker;
                        action.css('z-index', '0');
                        action.css('left', '93px');
                        // action.css('top', '95px');
                    }
                },
                onHide: function(dp, animationCompleted) {
                    if (! animationCompleted) {
                        action = dp.$datepicker;
                        action.css('z-index', '-1');
                    }
                },
            }).data('datepicker');

            $('#btn_select_year').on('click', function() {
                dp_year.show();
            });

            let action_month = null;
            let dp_month = $('#dp_month').datepicker({
                class: 'btn btn-outline-success',
                autoClose: true,
                toggleSelected: false,
                language: 'ru',
                view: 'months',
                dateFormat: 'M yy',
                onSelect: function(fd, date) {
                    let year = date.getFullYear(),
                        month = date.getMonth() + 1;
                    window.location.href = `http://${window.location.hostname}/dashboard/dashboard1/${year}/${month}`;
                },
                onShow: function(dp, animationCompleted){
                    if (! animationCompleted) {
                        action_month = dp.$datepicker;
                        action_month.css('z-index', '0');
                        action_month.css('left', '177px');
                        // action_month.css('top', '95px');
                    }
                },
                onHide: function(dp, animationCompleted) {
                    if (! animationCompleted) {
                        action_month = dp.$datepicker;
                        action_month.css('z-index', '-1');
                    }
                },
                onChangeYear(year) {
                    if (action_month != null) {
                        action_month = dp_month.$datepicker;
                        action_month.css('left', '177px');
                        // action_month.css('top', '95px');
                    }
                },
                onChangeMonth(month) {
                    if (action_month != null) {
                        action_month = dp_month.$datepicker;
                        action_month.css('left', '177px');
                        // action_month.css('top', '95px');
                    }
                },
            }).data('datepicker');

            $('#btn_select_month').on('click', function() {
                dp_month.show();
            });

            let action_day = null;
            let dp_day = $('#dp_day').datepicker({
                class: 'btn btn-outline-dark',
                autoClose: true,
                toggleSelected: false,
                language: 'ru',
                view: 'days',
                dateFormat: 'dd M yy',
                onSelect: function onSelect(fd, date) {
                    let year = date.getFullYear(),
                        month = date.getMonth() + 1,
                        day = date.getDate();
                    window.location.href = `http://${window.location.hostname}/dashboard/dashboard1/${year}/${month}/${day}`;
                },
                onShow: function(dp, animationCompleted){
                    if (! animationCompleted) {
                        action_day = dp.$datepicker;
                        action_day.css('z-index', '0');
                        action_day.css('left', '250px');
                        // action_day.css('top', '95px');
                    }
                },
                onHide: function(dp, animationCompleted) {
                    if (! animationCompleted) {
                        action_day = dp.$datepicker;
                        action_day.css('z-index', '-1');
                    }
                },
                onChangeYear(year) {
                    if (action_day != null) {
                        action_day = dp_day.$datepicker;
                        action_day.css('left', '250px');
                        // action_day.css('top', '95px');
                    }
                },
                onChangeMonth(month) {
                    if (action_day != null) {
                        action_day = dp_day.$datepicker;
                        action_day.css('left', '250px');
                        // action_day.css('top', '95px');
                    }
                }
            }).data('datepicker');

            $('#btn_select_day').on('click', function() {
                console.log('day');
                dp_day.show();
            });
        });

        const vm = new Vue({
            el: '#navigation',
            data() {
                return {
                    select_period: '{{ $select_period }}',
                    text_period: '{{ $text_period }}',
                    bad_param: {{ $bad_param }},
                }
            },
            methods: {
                selectPeriod(period_name, period_text) {
                    this.select_period = period_name;
                    this.text_period = period_text;
                }
            },
        });
    </script>
@endsection