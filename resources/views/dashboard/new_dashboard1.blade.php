@extends('skeleton.skeleton')

@section('content')
    <script type="text/javascript" src="https://momentjs.com/downloads/moment.min.js"></script>
    <script type="text/javascript" src="https://momentjs.com/downloads/moment-with-locales.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />

    <div class="row" id="navigation">
        <div class="col-lg-3 col-sm-12 col-md-12" style="margin-top: 6px;">
            <div class="btn-toolbar text-uppercase" role="toolbar">
                <div class="btn-group mr-2 font-weight-bold text-info" role="group">
                    <span style="vertical-align: -7px;margin-top: 5px;">период:</span>
                </div>

                <div class="form-group">
                    <div class="input-group date" id="datetimepicker" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker"/>
                        <div class="input-group-append" data-target="#datetimepicker" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-sm-12 col-md-12">
            <h2 class="text-uppercase" v-if="bad_param == false">анализ производства за <span class="font-weight-bold">@{{ text_period }}</span></h2>
            <h2 class="text-uppercase" v-else>период отчетности указан неверно <span class="font-weight-bold">@{{ text_period }}</span></h2>
        </div>
    </div>

    <div class="row">
        {{-- ЛЕВАЯ ЧАСТЬ --}}
        <div class="col-lg-10 col-sm-12 col-md-12">

            <div class="row">

                {{-- ВЫПОЛНЕНИЕ ПЛАНА --}}
                <div class="col-lg-6 col-sm-12 border">
                    <div class="text-center  font-weight-bolder text-uppercase">выполнение плана</div>

                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>стан</th>
                                <th>план</th>
                                <th>факт</th>
                                <th>брак</th>
                                <th>тонн</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $num => $row)
                            <tr>
                                <td>{{ ++$num }}</td>
                                <td>{{ $row['NAME'] }}</td>
                                <td>{{ round($row['KOL_PLAN'], 2) }}</td>
                                <td>{{ round($row['KOL_FACT'], 2) }}</td>
                                <td>{{ round($row['KOL_DEFECT'], 2) }}</td>
                                <td>{{ $row['ID_NAME'] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-6 col-sm-12 border">
                    <div class="text-center  font-weight-bolder text-uppercase">работа оборудования</div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-sm-12 border">
                    <div class="text-center  font-weight-bolder text-uppercase">другое</div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-sm-12 border">
                    <div class="text-center  font-weight-bolder text-uppercase">топ простоев</div>
                </div>
            </div>

        </div>

        {{-- ПРАВАЯ ЧАСТЬ --}}
        <div class="col-lg-2 col-sm-12 col-md-12 border">
            <div class="text-center  font-weight-bolder text-uppercase">забраковка</div>
        </div>
    </div>

    <style>
        .table > thead > tr > th { text-align: center; }
    </style>
    <script>
        $(function() {
            $('#datetimepicker').datetimepicker({
                viewMode: 'years',
                format: 'YYYY'
            });
        });

        const vm = new Vue({
            el: '#navigation',
            data() {
                return {
                    select_period: '{{ $select_period }}',
                    text_period: '{{ $text_period }}',
                    bad_param: {{ $bad_param }},
                }
            },
            methods: {
                selectPeriod(period_name, period_text) {
                    this.select_period = period_name;
                    this.text_period = period_text;
                }
            },
        });
    </script>
@endsection