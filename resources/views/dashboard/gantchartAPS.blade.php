@extends('skeleton.skeleton')

@section('content')
    </div>

    <ul class="nav nav-tabs" id="nav-tab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="table-tab" data-toggle="tab" href="#table" role="tab" aria-controls="table" aria-selected="true">Таблица</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="graph-tab" data-toggle="tab" href="#graph" role="tab" aria-controls="graph" aria-selected="false">График</a>
        </li>
    </ul>

    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="table" role="tabpanel" aria-labelledby="table-tab">
            <button class="btn btn-outline-secondary" id="zoomInButton" onclick="zoomIn()">+</button>
            <button class="btn btn-outline-secondary" id="zoomOutButton" onclick="zoomOut()">–</button>

            <div id="gant" style="height: 800px"></div>
        </div>
        <div class="tab-pane fade show" id="graph" role="tabpanel" aria-labelledby="graph-tab">
            <table class="table table-hover table-striped table-bordered table-sm">
                @foreach ($total as $row)
                <tr>
                    <th>{{ $row['machine'] }}</th>
                    <td>{{ $row['total'] }}</td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>


<script src="https://cdn.anychart.com/releases/8.7.1/js/anychart-core.min.js" type="text/javascript"></script>
<script src="https://cdn.anychart.com/releases/8.7.1/js/anychart-gantt.min.js" type="text/javascript"></script>
<script>
    let chart;

    anychart.onDocumentReady(function () {
        // create data
        let data = @json($data);

        // Set current timezone
        anychart.format.outputTimezone(-120);
        let treeData = anychart.data.tree(data, "as-table");
        chart = anychart.ganttResource();
        chart.data(treeData);

        let marks = @json($marks);
        marks.forEach(function(mark, index) {
            let marker = chart.getTimeline().lineMarker(index);
            marker.value(mark.datestar);
            marker.stroke('#dd2c00');
        });

        //set DataGrid and TimeLine visual settings
        chart.dataGrid().rowOddFill('#fff');
        chart.dataGrid().rowEvenFill('#fff');
        chart.getTimeline().rowOddFill('#fff');
        chart.getTimeline().rowEvenFill('#fff');

        chart.rowSelectedFill('#D4DFE8');
        chart.rowHoverFill('#EAEFF3');

        // set start splitter position settings
        chart.splitterPosition(230);

        // configure tooltips of the timeline
        chart.getTimeline().tooltip().useHtml(true);
        chart.getTimeline().tooltip().format(
            "Начало: {%start}<br>" +
            "Окончание: {%end}<br>" +
            "План: {%plan}<br>" +
            "Факт: {%fact}"
        );

        // get chart data grid link to set column settings
        let dataGrid = chart.dataGrid();

        chart.container("gant");
        chart.draw();

        // chart.zoomTo(1171036800000, 1176908400000);
        chart.fitAll();
    });

    // zoom the timeline in
    function zoomIn() {
        chart.zoomIn(2);
    }

    // zoom the timeline out
    function zoomOut() {
        chart.zoomOut(2);
    }
</script>
@endsection