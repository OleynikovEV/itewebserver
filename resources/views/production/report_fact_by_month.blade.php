@extends('skeleton.skeleton')

@section('content')
    <h2 class="text-center">{{ $title }}</h2>

    <div>
        @if ( isset($showCalendar) && $showCalendar)
            @include('skeleton.elements.filterByMonth', [
                'flTitle' => 'Дата предъявления',
                'flDateTime' => $date_time
            ])
        @endif

        @if ( empty($data) )
            <h1 class="text-center mt-4">
                @if ( isset($empty) )
                    {{ $empty }}
                @else
                    Данные отсутствуют
                @endif
            </h1>
        @else
            <div id="reports">
                @include('skeleton.elements.filterPanel')

                @include('skeleton.elements.navigationPanel')

                <table id="data-report" class="table table-striped table-bordered table-hover table-sm">
                    <thead>
                        <tr>
                            <th style="width: 5px">#</th>
                            <th style="width: 400px" class="text-center">Стандарт + характеристики</th>
                            <th style="width: 10px">Заказ (т)</th>

                            @foreach ($calendar as $day)
                                @if ($day == '')
                                    <th>Нет даты выпуска по APS</th>
                                @else
                                    <th class="text-center">{{ $day['day'] }}</th>
                                @endif
                            @endforeach
                        </tr>
                    </thead>

                    <tbody>
                        <template v-for="(order, ind) in filterData">
                            <tr>
                                <td>@{{ ind + 1 }}</td>
                                <td>
                                    <template v-for="(tech, key) in order.TECH1">
                                            <span :class="{'text-danger': (key == 'ПОКР'), 'text-success': (key == 'ГРП1'), 'text-violet': (key == 'ДИА1'), 'btn-link': true}"
                                                  v-if="tech.trim() != '/' && tech.length > 0"
                                                  @click="link_filter(tech, ( parseInt(key) || key == 0 ? 'NMAT' : key))">
                                                @{{ tech }}
                                            </span>
                                        <span v-else>@{{ tech }}</span>
                                    </template>
                                </td>
                                <td>
                                    <span v-if="order.KOL_ORDER > 0">@{{ order.KOL_ORDER.toFixed(2).replace('.', ',') }}</span>
                                </td>

                                <template v-for="(date, day) in calendar">
                                    <td class="text-center text-info">
                                        <span v-if="_.has(order.CAL, day)">@{{ order.CAL[day].toFixed(2).replace('.', ',') }}</span>
                                    </td>
                                </template>
                            </tr>
                        </template>
                    </tbody>

                    <tfoot>
                        <tr class="bg-secondary text-white">
                            <th rowspan="2" class="border-bottom align-middle">ИТОГО</th>
                            <th class="border-bottom align-middle text-right pr-2">Всего заказано</th>
                            <th>@{{ total_order.toFixed(2).replace('.', ',')  }}</th>
                            <th v-for="(date, day) in calendar"></th>
                        </tr>

                        <tr class="bg-secondary text-info_light">
                            <th class="border-bottom align-middle text-right">Всего предъявлено</th>
                            <th>@{{ total_fact.toFixed(2).replace('.', ',') }}</th>
                            <th class="text-center" v-for="(date, day) in calendar">
                                <span v-if="_.has(total, day)">@{{ total[day].toFixed(2).replace('.', ',') }}</span>
                            </th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        @endif
    </div>

    @if (! empty($data) )
        <script>
            // КОД  tmp.orders[lot.NUM] = lot - по какому полю должны грппироваться заказы
            // это поле должно быть идентицным полю в php $id = $row->NUM;
            'use strict'

            const vm = new Vue({
                el: '#reports',
                data() {
                    return {
                        dt: @json($data),
                        calendar: @json($calendar),
                        total: {},
                        total_fact: 0,
                        total_order: 0,

                        filter_NRSS_SHOT: '',
                        filter_OGR_NAME: '',
                        filter_NDM: '',
                        filter_FIO_OTV: '',
                        filter_month_num: '',
                        filter_PREV: '',
                        filter_DONE: '',
                        filters: {},
                    }
                },
                computed: {
                    filterData: function() {
                        let tmpTotal = {
                            '_SKC': {}
                        }
                        this.total_order = 0
                        this.total_fact = 0
                        this.total = {}

                        let data =  _.filter( this.dt, (order) => {
                            let tech = [order.TECH1]
                            return (_.findIndex(tech, this.filters) > -1)
                                && (this.filter_OGR_NAME.length == '' || this.filter_OGR_NAME == order.ORG_NAME)
                                && (this.filter_NDM.length == '' || this.filter_NDM == order.NDM)
                                && (this.filter_FIO_OTV.length == '' || this.filter_FIO_OTV == order.FIO_OTV)
                                && (this.filter_NRSS_SHOT.length == '' || this.filter_NRSS_SHOT == order.NRSS_SHOT)
                        })

                        _.forEach(data, row => {
                            this.total_fact += row.KOL_FACT
                            this.total_order += row.KOL_ORDER
                            _.forEach(row.CAL, (item, day) => {
                                if (! _.has(this.total, day) ) {
                                    this.total[day] = 0
                                }
                                this.total[day] += item
                            })
                        })

                        return data
                    },
                    amount: function() {
                        return _.size(this.filterData)
                    }
                },
                methods: {
                    clearFilter(key) {
                        Vue.delete(this.filters, key)
                    },
                    link_filter(val, field) {
                        this.$set( this.filters, field, val )
                    },
                    exportToExcel() {},
                }
            })
        </script>
    @endif
@endsection