@extends('skeleton.skeleton')

@section('content')
  <h2 class="text-center">MPS план - {!! $product !!}</h2>
  <a href="#" id="btnReload" style="position: absolute; right: 30px;" class="btn btn-outline-danger">Reload</a>

  <div id="body">
    @if ( empty($orders) )
      <h1 class="text-center mt-4">Нет данных по MPS плану</h1>
    @else

    <div class="mb-15" style="font-size: 12px;">
      <div>21 - желаемая дата выпуска</div>
      <div class="font-bold">21 - выпуск по APS</div>
      <div class="font-bold success">21 - APS-выпуск меньше даты заказа транспорта на 2 и более дня</div>
      <div class="font-bold warning">21 - APS-выпуск меньше на 1 день или равен дате заказа транспорта</div>
      <div class="font-bold danger">21 - APS-выпуск больше даты заказа транспорта (срыв отгрузки)</div>
    </div>

    @include('skeleton.elements.filterPanel')
    @include('skeleton.elements.navigationPanel')

    <table id="mpsplan" class="table table-striped table-bordered table-sm">
      <thead>
        <tr>
          <th rowspan="2">#</th>
          <th rowspan="2">Цех</th>
          <th rowspan="2" class="text-center" style="width: 95px">ПРО/Лот</th>
          <th rowspan="2" style="width: 10px">№</th>
          <th rowspan="2" class="text-center">Стандарт + характеристики</th>
          <th rowspan="2">&nbsp;</th>
          <th rowspan="2">&nbsp;</th>
            <th class="text-center text-danger" rowspan="2">Нет<br>желаемой<br>даты</th>

          @foreach ($month as $m)
            @if ($m['name'] == '')

            @else
              <th colspan="{{ $m['colspan']  }}" class="text-center">{{ $m['name'] }}</th>
            @endif
          @endforeach
        </tr>
        <tr>
        @foreach ($calendar as $day => $date)
          @if ($day != '')
            <th class="text-center">{{ date("d", strtotime($day)) }}</th>
          @endif
        @endforeach
        </tr>
      </thead>

      <tbody>
        <template v-for="(firm_row, firm) in filterData">
          <tr>
            <th colspan="{{ $amount + 7 }}" class="group">
              <span class="btn-link font-weight-bold" @click="filter_OGR_NAME = firm_row.info.ORG_NAME">@{{ firm_row.info.ORG_NAME }}</span>
              <span style="float: right">
                <span class="btn-link" @click="filter_FIO_OTV = firm_row.info.FIO_OTV">@{{ firm_row.info.FIO_OTV.toUpperCase() }}</span>
              </span>
            </th>
          </tr>

          <template v-for="(order, NDM, index) in firm_row.orders">
            <tr>
              <td rowspan="2" class="border-bottom">@{{ index + 1 }}</td>
              <td rowspan="2" class="border-bottom btn-link"
                @click="filter_CEH = order.CEH">@{{ order.CEH }}</td>
              <td rowspan="2" class="border-bottom">
                <span class="btn-link"
                  :class="{'text-success': (order.GROUP_LIST.length > 1)}"
                  :title="`${order.GROUP_LIST} #${order.NOM_SERT}`"
                  @click="filter_NDM = order.NDM">
                    @{{ order.LOT }}
                </span>
              </td>
              <td rowspan="2" class="border-bottom">@{{ order.NPP }}</td>
              <td rowspan="2" class="border-bottom">
              <template v-for="(tech, key) in order.TECH1">
                <span :class="{'text-danger': (key == 'ПОКР'), 'text-success': (key == 'ГРП1'),
                    'text-violet': (key == 'ДИА1'), 'btn-link': true}"
                  v-if="tech.trim() != '/' && tech.length > 0"
                  @click="link_filter(tech, ( parseInt(key) || key == 0 ? 'NMAT' : key))">
                    @{{ tech }}
                </span>
                <span v-else>@{{ tech }}</span>
              </template>
              <div v-if="prodId == 'PF'">@{{ order.TARA }}</div>
              </td>
              <td rowspan="2" class="border-bottom">
                <span class="btn-link" @click="filter_DONE = order.NRSS_SHOT">@{{ order.NRSS_SHOT }}</span>
              </td>
              <td>План</td>
              <template v-for="(data, day) in calendar">
                <td :class="{'text-danger': day == ''}" :title="order.TRANSPORT_DATE_NEW | formatDate">
                  <span v-if="_.has(order.CAL, '_S3C') && _.has(order.CAL._S3C, day)">
                    @{{ order.CAL._S3C[day].toFixed(2).replace('.', ',') }}
                  </span>
                  <div class="font-bold"
                       :class="order.FINISHTIME | dateNotification(order.TRANSPORT_DATE_NEW)"
                       v-if="_.has(order.CAL, '_FINISH') &&
                        _.has(order.CAL._FINISH, day)&& order.CAL._FINISH[day] > 0 && day != ''">
                    @{{ order.CAL._FINISH[day].toFixed(2).replace('.', ',') }}
                  </div>
                </td>
              </template>
            </tr>
            <tr>
              <td class="text-info border-bottom">Сдано</td>
              <template v-for="(data, day) in calendar">
                <td class="text-info border-bottom">
                  <span v-if="_.has(order.CAL, '_SKC') && _.has(order.CAL._SKC, day)">
                    @{{ order.CAL._SKC[day].toFixed(2).replace('.', ',') }}
                  </span>
                  <span v-else></span>
                </td>
              </template>
            </tr>
          </template>
        </template>
      </tbody>

      <tfoot>
        <tr class="bg-secondary text-white">
          <th colspan="6" rowspan="2" class="border-bottom align-middle">
            <div class="">
              Итого нет желаемой даты вупуска @{{ total_neobrab.toFixed(2).replace('.', ',') }} <br>
              Итого включено в план @{{ (total_plan - total_neobrab).toFixed(2).replace('.', ',') }} <br>
              Итого заказов в плане @{{ total_plan.toFixed(2).replace('.', ',') }} <br>
              Итого сдано @{{ total_fakt.toFixed(2).replace('.', ',') }}
            </div>
          </th>
          <th  class="border-bottom">ЖД</th>
            <template v-for="(data, day) in calendar">
              <th v-if="_.has(total, '_S3C') && _.has(total._S3C, day)">@{{ total._S3C[day].toFixed(2).replace('.', ',') }}</th>
              <th v-else></th>
            </template>
          </tr>
          <tr class="bg-secondary">
            <th  class="text-info_light">Сдано</th>
            <template v-for="(data, day) in calendar">
              <th class="text-info_light"
                  v-if="_.has(total, '_SKC') && _.has(total._SKC, day)">
                @{{ total._SKC[day].toFixed(2).replace('.', ',') }}
              </th>
              <th v-else></th>
            </template>
          </tr>

        <tr>
            <th rowspan="2" colspan="7"></th>
            <th class="text-center text-danger" rowspan="2">Нет<br>желаемой<br>даты</th>
            @foreach ($calendar as $day => $date)
                @if ($day != '')
                    <th class="text-center">{{ date("d", strtotime($day)) }}</th>
                @endif
            @endforeach
        </tr>
        <tr>
            @foreach ($month as $m)
                @if ($m['name'] == '')

                @else
                    <th colspan="{{ $m['colspan']  }}" class="text-center">{{ $m['name'] }}</th>
                @endif
            @endforeach
        </tr>
      </tfoot>
    </table>
  </div>

    <script>
      'use strict'

      const vm = new Vue({
        el: '#body',
        data() {
          return {
            prodId: @json($prodId),
            dt: @json($orders),
            calendar: @json($calendar),
            total: {},
            total_plan: 0,
            // total_aps: 0,
            total_fakt: 0,
            total_neobrab: 0,
            filters_num_client: '',
            filter_OGR_NAME: '',
            filter_NDM: '',
            filter_CEH: '',
            filter_FIO_OTV: '',
            filter_DONE: '',
            filter_PREV: '',
            filter_month_num: '',

            filters: {},
          }
        },
        filters: {
          formatDate: function(value) {
            if (value.length > 0) {
              const d = new Date(value);
              return 'Заказ транспорта на ' + d.toLocaleDateString('ru-RU');
            }

            return '';
          },

          dateNotification: function(finish, transport) {
            const dFinish = new Date(finish);
            const dTransport = new Date(transport);
            const diferent = (dTransport - dFinish) / (1000 * 60 *60 * 24);
            let classNotifi = '';

            switch (true) {
              case diferent >= 2:
                classNotifi = 'success';
                break;
              case diferent < 2 && diferent >= -1:
                classNotifi = 'warning';
                break;
              case diferent < -1:
                classNotifi = 'danger';
                break;
              default:
                classNotifi = '';
            }

            return classNotifi;
          },
        },
        computed: {
          filterData: function() {
            let tmpTotal = {
              '_S3C': {},
              '_SKC': {},
            }

            this.total_plan = 0
            this.total_fakt = 0
            this.total_neobrab = 0

            let data = _.map( this.dt, (firm, fname) => {

              let ffirm = _.filter( firm.orders, (order) => {
                let tech = [order.TECH1]
                return (_.findIndex(tech, this.filters) > -1)
                  && (this.filter_OGR_NAME.length == '' || this.filter_OGR_NAME == order.ORG_NAME)
                  && (this.filter_NDM.length == '' || this.filter_NDM == order.NDM)
                  && (this.filter_CEH.length == '' || this.filter_CEH == order.CEH)
                  && (this.filter_FIO_OTV.length == '' || this.filter_FIO_OTV == order.FIO_OTV)
                  && (this.filter_DONE.length == '' || this.filter_DONE == order.NRSS_SHOT)
                  && (this.filters_num_client.length == '' || order.NOM_SERT.indexOf(this.filters_num_client) > -1)
                })

                let tmp = {
                  'info': firm.info,
                }

                if ( _.isUndefined(tmp.orders) ) {
                    tmp.orders = {}
                    _.forEach(ffirm, lot => {
                         _.forEach(lot.CAL, (item, key) => { // расчет ИТОГО
                             _.forEach(item, (kol, day) => {
                                 if ( key != '_FINISH') {
                                     if (!_.has(tmpTotal[key], day)) {
                                         tmpTotal[key][day] = 0
                                     }
                                     tmpTotal[key][day] += kol
                                 }
                                 if (key == '_S3C') {
                                     this.total_plan += kol
                                 } else if (key == '_SKC') {
                                     this.total_fakt += kol
                                 }
                             })
                        })
                        tmp.orders[lot.NDM + '-' + lot.NPP] = lot
                    })
                }
                return tmp
                })

                    data = _.filter(data, item => {
                        return ! _.isEmpty(item.orders)
                    })

                    this.total = tmpTotal
                    this.total_neobrab = (this.total._S3C.hasOwnProperty('')) ? this.total._S3C[''] : 0
                    return data
                },
                amount: function() {
                    return _.sumBy( this.filterData, item => _.size(item.orders) )
                }
            },
        methods: {
          clearFilter(key) {
            Vue.delete(this.filters, key)
          },
          link_filter(val, field) {
            this.$set( this.filters, field, val )
          },
          exportToExcel() {
            $('#mpsplan').tableExport({
              type:'excel',
              bootstrap: true,
              headings: true,
              footers: true,
              formats: ["xlsx", "csv", "txt"],
              fileName: "MPSPlan",
            })
          }
        }
      })

      $(function() {
        // fixedHeader('mpsplan')
      })
    </script>

    @endif
@endsection

{{--Есть запас - Оклонение < 0--}}
{{--Угроза срыва- Оклонение = 0--}}
{{--Сорвано  = Оклонение > 0--}}
