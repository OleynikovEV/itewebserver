<?php

Route::get('/', function () {
    prn( base_path() );
    return view('index'); // работает
})->name('home');

/** production group */
Route::group(['prefix' => 'production'], function() {
    Route::get('/aps-plan/report/{prod_id}/{ceh?}', [\App\Http\Controllers\ProductionController::class, 'apsPlan'])->name('production.aps-plan.report'); // работает
    Route::get('/fact/report/{prod_id}', 'ProductionController@factbyMonth')->name('production.fact.report'); // работает
});

Route::group(['prefix' => 'orders'], function() {
    Route::get('/orders-by-month/{prod_id}', 'OrdersController@gerOredersByMonth')->name('orders.ordersByMonth'); // работает
});

/** ANALYTICS DASHBOARD */
Route::group(['prefix' => 'dashboard'], function() {
    Route::get('dashboard1/{year?}/{month?}/{day?}', 'Dashboard\Dashboard@dashboard1')->name('dashboard.dashboard1'); // первый дашборд
    Route::get('gantchartAPS', 'Dashboard\Dashboard@gantchartAPS')->name('dashboard.gantchartAPS');
});

/** IMPORT */
Route::group(['prefix' => 'import'], function() {
    Route::get('importAPSPlan', 'Dashboard\Dashboard@import')->name('import.APSPlan'); // отображение диаграмы ганта -  работает
    Route::post('importAPSPlan/{unapsv}', 'Import\ImportITE@saveAPSPlan')->name('import.saveAPSPlan'); // перекачиваем план из ITE в хранилище - работает

    Route::get('importFact/{year}/{month}', 'Import\ImportITE@saveFactProduction')->name('import.get.saveFact');
    Route::post('importFact/{year}/{month}', 'Import\ImportITE@saveFactProduction')->name('import.post.saveFact');

//    Route::get('orders/{year}/{month}', 'Import\ImportProduction@orders')->name('import.orders');
//    Route::get('plan-action-smena', 'Import\ImportProduction@plan_action_smena')->name('import.plan.action_smena');
//    Route::get('plan-action-smena-aps/{num}', 'Import\ImportProduction@plan_action_smenaAPS')->name('import.plan.smena_aps_by_num');
//    Route::get('plan-action-smena-aps', 'Import\ImportProduction@plan_action_smenaAPS')->name('import.plan.action_smena_aps');
});

/** TEST */
Route::get('test', 'Dashboard\Dashboard@importAPS_test')->name('test.importAPS_test');

Route::get('iiot', function() {
    echo '<h1>IIoT</h1>';

    $sql = '
        select val.*, sen.name as sensors, sen.ORDERNUM, thi.name as things, tp.name as sensortp, tp.measure, tp.charttype

        from sensval val
        left join sensors sen on sen.id = val.sensorid
        left join things thi on thi.id = sen.thingid
        left join sensortp tp on tp.id = sen.typeid
        where val.TIMESTAMP BETWEEN CONVERT(DATETIME, \'2019-07-01 09:35:57\', 120) AND CONVERT(DATETIME, \'2019-07-01 20:33:52\', 120)
          and val.sensorid not in (4, 5)
        order by val.timestamp asc
    ';

    $data = DB::table('sensval')
        ->join('sensors', 'sensors.id', '=', 'sensval.sensorid')
        ->join('things', 'things.id', '=', 'sensors.thingid')
//        ->whereBetween('timestamp', ['2019-07-01 09:35:57', '2019-07-01 20:33:52'])
        ->whereBetween('timestamp', ['2019-10-01', '2019-10-02'])
        ->whereNotIn('sensorid', [4, 5])->orderBy('timestamp')
        ->select(
            'sensval.sensorid',
            'sensval.timestamp',
            DB::raw('FORMAT(sensval.timestamp, \'yy-MM-dd hh:mm:ss\') as date'),
            'sensval.value',
            'sensors.name as warning',
            'things.name as stan'
        )
        ->get();

//    prn($data, true);

//    $label = [];
//    $speed_val = [];
//    $meters_val = [];
//    $tmp_label = null;
//    $tmp_speed_val = null;
//    $tmp_meters_val = null;
//    foreach ($data as $id => $row) {
//        if ( $row->sensorid == 1 ) {
//            $tmp_label = $row->timestamp;
//            $tmp_speed_val = (float)$row->value;
//        } elseif ( $row->sensorid == 2 && $data[$id - 1]->sensorid ==1 ) {
//            $tmp_label = $row->timestamp;
//            $tmp_meters_val = $row->value;
//        }
//
//        $label[] = $tmp_label;
//        $meters_val[] = $tmp_meters_val;
//        $speed_val[] = $tmp_speed_val;
//    }

//    return view('dashboard.analytics.machines', [
//        'speed_val' => $speed_val,
//        'meters_val' => $meters_val,
//        'label' => $label,
//    ]);

    $val = [];
    $id = 0;
    foreach ($data as $row) {
        if ( $row->sensorid == 1 ) {
            $val[$id] = [
                'id' => $row->sensorid,
                'timestamp' => $row->timestamp,
                'date' => $row->date,
                'speed' => (float)$row->value,
                'lng' => null,
                'warning' => null,
                'stan' => $row->stan,
            ];
            $id++;
        } elseif ( $row->sensorid == 2 && isset($val[$id - 1]) && $val[$id - 1]['id'] == 1 ) {
            $val[$id - 1]['lng'] = (float)$row->value;
        } elseif ( $row->sensorid == 7 || $row->sensorid == 9 || $row->sensorid == 10 || $row->sensorid == 11 ) {
            $val[$id] = [
                'id' => $row->sensorid,
                'timestamp' => $row->timestamp,
                'date' => $row->date,
                'speed' => null,
                'lng' => null,
                'warning' => $row->warning,
                'stan' => $row->stan,
            ];
            $id++;
        }
    }

//    prn($val, true);

    return view('dashboard.analytics.machines', [
        'data' => $val
    ]);

//    Server: 10.0.1.5
//    Port: 49742
//    Base: STALKANAT
//    Login: testuser
//    Password: testuser1290

//    $tables = DB::connection()->getDoctrineSchemaManager()->listTableNames(); // получить список всех таблиц
//    prn( $tables );

//    $tbl_info = DB::getSchemaBuilder()->getColumnListing('sensval');
//    prn( $tbl_info );

//    DB::select
//    sensval
//    $data = DB::table('sensval')->orderBy('TIMESTAMP', 'desc')->first();
//    $data = DB::table('sensval')->whereBetween('TIMESTAMP', ['2019-10-30', '2019-10-30'])->get();

//    prn( $data );
});

//Route::get('{any?}', function (){
//    return view('index');
//})->where('any', '.*');
