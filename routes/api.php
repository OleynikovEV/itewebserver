<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/aps_plan', function (Request $request) {
    $client = new \GuzzleHttp\Client([
        'base_uri' => 'http://192.168.82.174/ws/webservice.asmx/'
    ]);

    $options = [
        'json' => [
            'calcId' => '_APS_PLAN',
            'args' => $request->get('args') ?? ''
        ]
    ];

    try {
        $response = $client->request('POST', 'Execute', $options);
        if ($response->getStatusCode() != 200) {
            return null;
        } else {
            $body = $response->getBody(); // вытаскиваем дынные из запроса
            $data = json_decode($body); // преобразуем в объект
            $data = json_decode($data->d); // вытаскиваем данные повторно из объекта
            foreach ($data as $index => $row) {
                $tech = json_decode($row->TECH); // из json строки получаем объект
                foreach ($tech as $key => $val) { // и добавляем в массив
                    $data[$index]->$key = $val;
                }
                unset($data[$index]->TECH);
            }
//            echo '<pre>';
//            print_r( $data );
//            echo '</pre>';
            return json_encode($data);
        }
    } catch (RequestException $e) {
        return $e->getMessage();
    }
});
