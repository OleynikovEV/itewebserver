<?php

$menu = [
    [
        'Производство' => 'parent',
        'child' => [
            [
                'График предъявления (MPS)' => '',
                'child' => [
                    'Фибра (PF)' => route('production.aps-plan.report', 'PF'),
                    'Пряди арматурные (PS)' => route('production.aps-plan.report', 'PS'),
                    'Проволока (PW)' => route('production.aps-plan.report', 'PW'),
                ]
            ],
            [
                'Факт предъявления (по месяцам)' => '',
                'child' => [
                    'Фибра (PF)' => route('production.fact.report', 'PF'),
                    'Пряди арматурные (PS)' => route('production.fact.report', 'PS'),
                    'Проволока (PW)' => route('production.fact.report', 'PW'),
                ]
            ],
            [
                'Список заказов по месяцам (+факт)' => '',
                'child' => [
                    'Фибра (PF)' => route('orders.ordersByMonth', 'PF'),
                    'Пряди арматурные (PS)' => route('orders.ordersByMonth', 'PS'),
                    'Проволока (PW)' => route('orders.ordersByMonth', 'PW'),
                ]
            ],
        ]
    ],
];

$dev = [
    [
        'Dashboard' => 'parent',
        'child' => [
            [
                'Dashboard 1' => route('dashboard.dashboard1'),
                '----------------------------------------------------' => '',
                'Dashboard test' => route('dashboard.gantchartAPS'),
                'Test APS plan import + Gant' => route('test.importAPS_test'),
            ],
        ]
    ],
    [
        'Import' => 'parent',
        'child' => [
            [
                'Import (plan/fact)' => route('import.APSPlan'),
            ],
        ]
    ]
];

if ( env('APP_ENV') == 'dev' ) {
    $menu = array_merge($menu, $dev);
}

return $menu;