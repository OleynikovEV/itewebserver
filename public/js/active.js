'use strict'

// let event = new CustomEvent('menuClick')

$(function() {
    $('#btnReload').attr('href', setQuery({'reload': true}))

    $('#tree').treed({openedClass:'fa fa-folder', closedClass:'fa fa-folder-open'})

    // $('#bars').on('click', function() {
    //     let show = $('#menu').is(':visible')
    //     showSidebar(show)
    //     localStorage.setItem('openMenu', show)
    //     window.dispatchEvent(event)
    // })

    $('#toggle').click(function() {
        $('#nav-menu').toggleClass('m-open')
    })

    window.dpMonth = $('#dpmonth').datepicker({
        autoClose: true,
        onSelect: function onSelect(fd, date) {
            let url = setQuery({
                'year': date.getFullYear(),
                'month': date.getMonth() + 1
            })
            $('#btnSelect').attr('href', url)

            url =  setQuery({
                'year': date.getFullYear(),
                'month': date.getMonth() + 1,
                'reload': true
            })
            $('#btnReload').attr('href', url);
        }
    })

    // showSidebar( localStorage.getItem('openMenu') )

    // ждем загрузку страницы
    $('#row').removeClass('show')

    fixedHeader('reports')
})

function setQuery(row)
{
    let url = [location.protocol, '//', location.host, location.pathname].join('') // получаем url без параметров запроса после ? ****
    let param = new URLSearchParams()

    _.forEach(row, (val, key) => {
        param.set(key, val)
    })

    return `${url}?${param.toString()}`
}

// function showSidebar(visible) {
//     if ( visible == 'true' || visible == true ) { // скрываем меню
//         $('#menu').removeClass('d-md-block')
//         $('#menu').hide()
//
//         $('#main').removeClass('col-md-9 col-lg-10')
//         $('#main').addClass('col-md-12 col-lg-12')
//
//     } else { // показываем меню
//         $('#main').removeClass('col-md-12 col-lg-12')
//         $('#main').addClass('col-md-9 col-lg-10')
//
//         $('#menu').addClass('d-md-block')
//         $('#menu').show()
//     }
// }

// FIXED HEADER
function fixedHeader(IdTable) {
    let id = `#${IdTable}`
    let header = null
    let hgHeader = 300

    $(window).on('scroll', function(e) { // скролинг
        if ( $(this).scrollTop() > hgHeader ) {
            header = initFixedHeader(id) // клонируем шапку
            header.show()
        }
        else {
            if ( header != null ) {
                header.hide()
                header.remove()
            }
        }
    })

    window.addEventListener('menuClick', function(e) {
        header = initFixedHeader(id)
    }, false)
}

function initFixedHeader(IdTable) {
    let clone = null

    if ( document.getElementsByClassName('fixed-header').length > 0 ) { // если элемент существует, просто берем его
        clone = $('.fixed-header')
    } else { // если элемента не существует, клонируем шапку
        clone = $(`${IdTable} thead`).clone()
        clone.addClass('fixed-header') // добавляем класс
        clone.hide()
        clone.insertBefore( $(IdTable) )
    }

    let tblWith = getWidthTable(IdTable) // получаем размер изначальной таблицы
    clone.width(tblWith)

    let thWidth = getWidthTh(IdTable) // масив для размера ячеек изначальной шапки
    let thClone = clone.children('tr').children('th') // заполняем масив с размерами ячеек
    $(thClone).each(function(ind, el) {
        //if (ind == 3) $(el).width( thWidth[ind] - 10 )
        //else
            $(el).width( thWidth[ind] )
    })

    return clone
}

function getWidthTable(IdTable) {
    return $(IdTable).width()
}

function getWidthTh(IdTable) {
    let thWidth = []
    $(`${IdTable} thead th`).each(function(ind, el) { // заполняем масив с размерами ячеек
        thWidth.push( $(el).width() )
    })

    return thWidth
}
//-- FIXED HEADER

// меню ввиде дерева
$.fn.extend({
    treed: function (o) {

        let openedClass = 'fa fa-folder-open'
        let closedClass = 'fa fa-folder'

        if (typeof o != 'undefined'){
            if (typeof o.openedClass != 'undefined'){
                openedClass = o.openedClass
            }
            if (typeof o.closedClass != 'undefined'){
                closedClass = o.closedClass
            }
        }

        //initialize each of the top levels
        var tree = $(this)
        tree.addClass("tree")
        tree.find('li').has("ul").each(function () {
            var branch = $(this) //li with children ul
            branch.prepend("<i class='indicator " + closedClass + "'></i>")
            branch.addClass('branch')
            branch.on('click', function (e) {
                if (this == e.target) {
                    var icon = $(this).children('i:first')
                    icon.toggleClass(openedClass + " " + closedClass)
                    $(this).children().children().toggle()
                }
            })
            //branch.children().children().toggle() // автоматом все закрыто
        })
        //fire event from the dynamically added icon
        /*tree.find('.branch .indicator').each(function(){
            $(this).on('click', function () {
                $(this).closest('li').click()
            })
        })*/
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch > i').each(function () { // раскрытие/закрытие меню по щелчку на иконку
            $(this).on('click', function (e) {
                $(this).closest('li').click()
                e.preventDefault()
            })
        })
        /*tree.find('.branch > a').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click()
                e.preventDefault()
            })
        })*/
        //fire event to open branch if the li contains a button instead of text
        /*tree.find('.branch > button').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click()
                e.preventDefault()
            })
        })*/
    }
})

$.fn.datepicker.language['ru'] =  {
    days: ['Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'],
    daysShort: ['Вос','Пон','Вто','Сре','Чет','Пят','Суб'],
    daysMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
    months: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
    monthsShort: ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'],
    today: 'Сегодня',
    clear: 'Очистить',
    dateFormat: 'dd.mm.yyyy',
    timeFormat: 'hh:ii',
    firstDay: 1
};
