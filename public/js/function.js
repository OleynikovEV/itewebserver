'use strict'

Number.prototype.isInteger = function() {
    return ( parseInt(this) === this );
};

// вывод в консоль
function log(data) { console.log(data); }
function error(data) { console.error(data); }

// работа с цифрами
Number.prototype.round = function(places) { // математическое округление
    return +(Math.round(this + "e+" + places)  + "e-" + places);
}

String.prototype.isEmpty = function() {
    return (this.length === 0 || !this.trim());
};

// формат даты
function formatDate(date, separator, sql) {
    if (separator == undefined)
        separator = '.';

    if (sql == undefined)
        sql = false;

    var dd = date.getDate();
    if (dd < 10) dd = '0' + dd;

    var mm = date.getMonth() + 1;
    if (mm < 10) mm = '0' + mm;

    var yy = date.getFullYear();
    //if (yy < 10) yy = '0' + yy;

    return (sql == false) ? (dd + separator + mm + separator + yy) : (yy + separator + mm + separator + dd);
}