<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class FactLoaded extends Model
{
    protected $collection = 'facts_loaded';
}
