<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class ApsTmp extends Model
{
    protected $collection = 'aps_tmp';
}
