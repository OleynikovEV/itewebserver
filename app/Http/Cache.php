<?php

namespace App\Http;

use Illuminate\Support\Facades\Storage;

class Cache
{
    public static function file($name)
    {
        $file_exists = Storage::exists($name);
        $dif = 0;

        if ( $file_exists ) {
            $modified = Storage::lastModified($name);
            $microtime = microtime(true);
            $dif = ($microtime - $modified) / 60;
        }

        if ( $file_exists && $dif <= 60) {
            return json_decode( Storage::get($name) );
        } else {
            return false;
        }
    }
}