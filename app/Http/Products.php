<?php

namespace App\Http;

use Illuminate\Support\Facades\Config;

class Products
{
    public static function getProductName($prod_id)
    {
        $products = Config::get('settings.products');

        if (! isset($products[$prod_id]) ) { // проверяем существует ли продукция
            die( sprintf('Данный вид продукции <b>%s</b> отсутствует', $prod_id) );
        }

        return $products[$prod_id];
    }
}
