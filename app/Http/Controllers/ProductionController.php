<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Request as ITERequest;
use App\Http\Tech;
use App\Http\Products;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class ProductionController extends Controller
{
    const CACHE_TIME = 7200;

    // APS-план - график предъявления MPS
    public function apsPlan(Request $request, $prod_id, $ceh = null)
    {
        $tmp = [];
        $calendar = [];
        $month = [];

        $product_name = Products::getProductName($prod_id); //название продукции
        $reload = $request->get('reload') ?? false;

        $key = '_APS_PLAN_' . $prod_id . $ceh ?? '_all';
        if ($reload) {
            Cache::forget($key);
        }

        $data = Cache::get($key);
        if ( is_null($data) ) {
            // формируем запрос с фильтрами
            $args = [
                'prod_id' => $prod_id,
            ];

            if ($ceh !== null) $args['ceh_id'] = $ceh;

            $args = (object)$args;

            $data = ITERequest::sendRequest('_APS_PLAN3', $args, ITERequest::METHOD_POST);
            Cache::put($key, $data, static::CACHE_TIME);
        }

        if (! empty($data) ) {

            foreach ($data as $index => $row) {
                // вытаскиваем все даты для календаря - только уникальные
                switch ($row->KDMT) {
                    case '_S3C':
                        $ddm = $row->PREFERABLE_DATE; // желаемая дата выпуска
                        break;
                    default:
                        $ddm = $row->DDM;
                }

                $firm_id = $row->ORG_NAME;
                $id = $row->NDM . '-' . $row->NPP;

                // информация по фирме
                if (! isset($tmp[$firm_id]) ) {
                    $tmp[$firm_id]['info'] = [
                        'FIO_OTV' => $row->FIO_OTV,
                        'ORG_NAME' => $row->ORG_NAME,
                    ];
                }

                // заказы
                if (! isset($tmp[$firm_id]['orders'][$id]) ) {
                    $tmp[$firm_id]['orders'][$id] = $row;
                    $tmp[$firm_id]['orders'][$id]->GROUP_LIST = [];

                    // вытаскиваем из json характеристики продукции
                    if ( $row->TECH != null ) {
                        $tech = Tech::getTech($row->TECH, $row->TECH_STR);
                        foreach ($tech as $key => $val) {
                            $tmp[$firm_id]['orders'][$id]->{$key} = $val;
                        }
                    }
                }

                // формируем календарь
                if ($row->KDMT == '_S3C') {
                    addDateToCalendar($row->PREFERABLE_DATE, $calendar); // желаемая дата выпуска
                    addDateToCalendar($row->FINISHTIME, $calendar);

//                    if ($row->PREFERABLE_DATE === '2021-02-18'  || $row->PREFERABLE_DATE === '2021-02-18')

                    if (! isset($tmp[$firm_id]['orders'][$id]->CAL['_FINISH'][$row->FINISHTIME]) ) {
                        $tmp[$firm_id]['orders'][$id]->CAL['_FINISH'][$row->FINISHTIME] = round($row->KOL_PLA, 2);
                    } else {
                        $tmp[$firm_id]['orders'][$id]->CAL['_FINISH'][$row->FINISHTIME] += round($row->KOL_PLA, 2);
                    }
                    $tmp[$firm_id]['orders'][$id]->GROUP_LIST[] = $row->KZAJNPP;
                } else {
                    addDateToCalendar($row->DDM, $calendar);
                }

                if (! isset($tmp[$firm_id]['orders'][$id]->CAL[$row->KDMT][$ddm]) ) {
                    $tmp[$firm_id]['orders'][$id]->CAL[$row->KDMT][$ddm] = round($row->KOL, 2);
                }
                else {
                    $tmp[$firm_id]['orders'][$id]->KOL_PLA += $row->KOL_PLA;
                }

                if ($ddm == '') {
                    $tmp[$firm_id]['orders'][$id]->KOL_PLA = 0;
                }
            }

            ksort($calendar); // сортируем календарь
            ksort($tmp); // сортируем по фирме

            foreach ($calendar as $day) {
                if ($day['year'] === null) {
                    $month[null]['name'] = '';
                    $month[null]['colspan'] = 1;
                }
                if (! isset($month[ $day['year'] . $day['month'] ]) ) {
                    $month[ $day['year'] . $day['month'] ]['name'] = get_month_short( $day['month'] ) . '/' . Str::substr($day['year'], -2);
                    $month[ $day['year'] . $day['month'] ]['colspan'] = 0;
                }
                if ($day['month'] != '') {
                    $month[ $day['year'] . $day['month'] ]['colspan'] += 1;
                } else {
                    $month[ $day['year'] . $day['month'] ]['colspan'] = 1;
                }
            }

//            dd($month);
        }

        return view('production.report_aps_plan', [
            'orders' => $tmp,
            'product' => $product_name,
            'month' => $month,
            'calendar' => $calendar,
            'amount' => count($calendar),
            'prodId' => $prod_id,
        ]);
    }

    // Факт предъявления по месяцам
    public function factbyMonth(Request $request, $prod_id)
    {
        $year = $request->get('year') ?? date('Y');
        $month = $request->get('month') ?? date('n');

        // формируем запрос с фильтрами
        $args = (object)[
            'prod_id' => $prod_id,
            'month' => $month,
            'year' => $year,
        ];

        $data = ITERequest::sendRequest('_FACT', $args, ITERequest::METHOD_POST);
//        prn($data, true);
        $tmp = [];
        $calendar = [];

        if (! empty($data) ) {
            foreach ($data as $row) {
                $id = $row->KMAT;

                if (! isset($tmp[$id]) ) {
                    $tmp[$id] = $row;
                    $tmp[$id]->CAL = [];
                    $tmp[$id]->KOL_ORDER = 0;
                    $tmp[$id]->KOL_FACT = 0;

                    if (is_json($row->TECH) && validation_string($row->TECH_STR, 10)) {
                        $tech = Tech::getTech($row->TECH, $row->TECH_STR);
                        foreach ($tech as $key => $val) {
                            $tmp[$id]->{$key} = $val;
                        }
                    }
                }

                switch ($row->KDMT) {
                    case '_SKC':
                        $tmp[$id]->KOL_FACT += $row->KOL;
                        $tmp[$id]->CAL[$row->DDM] = $row->KOL;
                        addDateToCalendar($row->DDM, $calendar);
                        break;
                    case '_S3C':
                        $tmp[$id]->KOL_ORDER += $row->KOL;
                        break;
                }
            }

            ksort($calendar);
            ksort($tmp);
//            prn($tmp, true);
//            prn($calendar, true);
        }

         return view('production.report_fact_by_month', [
             'title' => 'Факт предъявления - ' . Products::getProductName($prod_id),
             'empty' => 'Факт предъявления за указанный период отсутствует',
             'showCalendar' => true,
             'date_time' => sprintf('%s-%s-01', $year, $month),

             'data' => $tmp,
             'calendar' => $calendar,
         ]);
    }
}
