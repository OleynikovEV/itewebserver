<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Models\ApsModel;
use App\Http\Models\FactModel;
use App\Http\Request as ITERequest;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Import\CutShift;
use App\Models\ApsV;
use App\Models\Aps;
use App\Models\FactLoaded;

class Dashboard extends Controller
{
    /** ДЛЯ ТЕСТОВ */
    public function gantchartAPS()
    {
        $i = 1;
        $tmp = [];
        $tmp_total = [];
        $arr_smena = Aps::all()->toArray();
        foreach ($arr_smena as $row) {
            $id = $row['RCENTER_ID'];
            if(! isset($tmp[$id]) ) {
                $tmp[$id] = [
                    'id' => $i,
                    'name' => $row['RCENTER_NAME'],
                    'periods' => [],
                ];

                $i++;
            }

            if (! isset($tmp_total[$id]) ) {
                $tmp_total[$id] = [
                    'machine' => $row['RCENTER_NAME'],
                    'total' => 0,
                ];
            }

            if ($row['INTERVAL_ID'] == 1) $tmp_total[$id]['total'] += $row['KOL_PLAN'];

            switch ( $row['INTERVAL_NAME'] ) {
                case 'перерыв':
                    $color = '#AFA4A4';
                    $color1 = '#AFA4A4';
                    break;
                case 'переналадка':
                    $color = '#F57650';
                    $color1 = '#F57650';
                    break;
                case 'простой':
                    $color = '#6B5D5D';
                    $color1 = '#6B5D5D';
                    break;
                default:
                    $color = '#B8AA96'; // border
                    $color1 = '#CFC0A9'; // bottom
            }

            $tmp[$id]['periods'][] = [
                'id' => $row['KZAJNPP'],
                'name' => $row['KZAJNPP'] . ' ' . $row['INTERVAL_NAME'],
                'start' => $row['DATETIME_START'],
                'end' => $row['DATETIME_FINISH'],
                'plan' => $row['KOL_PLAN'],
                'interval_id' => $row['INTERVAL_ID'],
                'stroke' => $color,
                'fill' => [
                    'angle' => 90,
                    'keys' => [
                        [
                            'color' => $color1,
                            'position' => 1,
                        ],
                    ],
                ]
            ];
        }

        sortByKeys($tmp, ['name' => 'asc']);
        sortByKeys($tmp_total, ['machine' => 'asc']);

        return view('dashboard.gantchartAPS', [
            'data' => $tmp,
            'total' => $tmp_total,
            'marks' => ApsV::all(),
        ]);
    }

    /** ДЛЯ ТЕСТОВ */
    public function importAPS_test()
    {
//        \App\Http\Controllers\Import\ImportKube::plan_smenaAPS(575);
//        $tmp = \App\Models\ApsTmp::all();
//        $tmp = $tmp->toArray();

        $tmp = Aps::all()->toArray();
//        $file = \Illuminate\Support\Facades\File::get( base_path('tests/Unit/json/aps_tmp.json'), true );
//        $tmp = json_decode($file);

        $cutAction = new CutShift();
        $cutAction->set($tmp);
//        $cutAction->cutByTimeStamp('2020-01-16 23:00');
//        $cutAction->cutByTimeStamp('2019-11-24 18:00');
//        $cutAction->cutByShift();
        $arr_smena = $cutAction->get();
//        prn($arr_smena, true);

        $tmp = [];
        $tmp_total = [];
        $i = 1;

        foreach ($arr_smena as $row) {
            $id = $row['RCENTER_ID'];
            if(! isset($tmp[$id]) ) {
                $tmp[$id] = [
                    'id' => $i,
                    'name' => $row['RCENTER_NAME'],
                    'periods' => [],
                ];

                $i++;
            }

            if (! isset($tmp_total[$id]) ) {
                $tmp_total[$id] = [
                    'machine' => $row['RCENTER_NAME'],
                    'total' => 0,
                ];
            }

            if ($row['INTERVAL_ID'] == 1) $tmp_total[$id]['total'] += $row['KOL_PLAN'];

            switch ( $row['INTERVAL_NAME'] ) {
                case 'перерыв':
                    $color = '#AFA4A4';
                    $color1 = '#AFA4A4';
                    break;
                case 'переналадка':
                    $color = '#F57650';
                    $color1 = '#F57650';
                    break;
                case 'простой':
                    $color = '#6B5D5D';
                    $color1 = '#6B5D5D';
                    break;
                default:
                    $color = '#B8AA96'; // border
                    $color1 = '#CFC0A9'; // bottom
            }

            $tmp[$id]['periods'][] = [
                'id' => $row['KZAJNPP'],
                'name' => $row['KZAJNPP'] . ' ' . $row['INTERVAL_NAME'] . ( isset($row['TEST']) ? $row['TEST'] : '' ),
                'start' => $row['DATETIME_START'],
                'end' => $row['DATETIME_FINISH'],
                'plan' => $row['KOL_PLAN'],
                'interval_id' => $row['INTERVAL_ID'],
                'stroke' => $color,
                'fill' => [
                    'angle' => 90,
                    'keys' => [
                        [
                            'color' => $color1,
                            'position' => 1,
                        ],
                    ],
                ]
            ];
        }


        sortByKeys($tmp, ['name' => 'asc']);
        sortByKeys($tmp_total, ['machine' => 'asc']);

        return view('dashboard.gantchartAPS', [
            'data' => $tmp,
            'total' => $tmp_total,
            'marks' => ApsV::all(),
        ]);
    }

    public function dashboard1($year = null, $month = null, $day = null)
    {
        $data = [];
        $year = ($year == null) ? date('Y') : $year;

        /** проверить валидность года, месяца, дня */
        $bad_param = false;
        if ( $year < 2018 || $year > 2050 ) {
            $bad_param = true;
        }
        if ($month != null && ($month > 12 || $month < 1)) {
            $bad_param = true;
        }
        if ( $day != null && ! validation_date(sprintf('%s-%s-%s', $year, $month, $day)) ) {
            $bad_param = true;
        }

        if (! $bad_param) {
            $apsM = new ApsModel();
            $factM = new FactModel();

            if ($day != null) {
                $apsM->setFilter(ApsModel::FILTER_DAY, [$year, $month, $day]);
                $factM->setFilter(FactModel::FILTER_DAY, [$year, $month, $day]);
            } elseif ($month != null) {
                $apsM->setFilter(ApsModel::FILTER_MONTH, [$year, $month]);
                $factM->setFilter(FactModel::FILTER_MONTH, [$year, $month]);
            } else {
                $apsM->setFilter(ApsModel::FILTER_YEAR, $year);
                $factM->setFilter(FactModel::FILTER_YEAR, $year);
            }
            $plan = $apsM->getSumPlan();
            $fact = $factM->getSumFact();

            $nonorm = [];
            if (isset($fact['NONRM'])) {
                $nonorm = $fact['NONRM'];
                unset($fact['NONRM']);
            }

            $tmp = [];
            foreach ($plan as $id => $row) {
                $tmp[$id] = $row;
                $tmp[$id]['KOL_FACT'] = (!isset($fact[$id])) ? 0 : $fact[$id]['KOL_FACT'];
                $tmp[$id]['KOL_DEFECT'] = (!isset($fact[$id])) ? 0 : $fact[$id]['KOL_DEFECT'];
            }
            foreach ($fact as $id => $row) {
                if (!isset($tmp[$id])) {
                    $tmp[$id] = $row;
                    $tmp[$id]['KOL_PLAN'] = 0;
                }
            }

            $data = [
                'data' => $tmp,
                'nonorm' => $nonorm,
                'sel_year' => $year,
                'sel_month' => get_month_short( ($month == null) ? date('m') : $month ),
                'sel_day' => ($day == null) ? date('d') : $day,
                'select_period' => 'sel_year',
                'text_period' => $year,
                'bad_param' => 'false',
            ];
        } else {
            $data = [
                'data' => [],
                'nonorm' => [],
                'sel_year' => date('Y'),
                'sel_month' => get_month_short( date('m') ),
                'sel_day' => date('d'),
                'select_period' => 'sel_year',
                'text_period' => date('Y'),
                'bad_param' => 'true',
            ];
        }

        return view('dashboard.dashboard1', $data);

    }

    /** Отображение формы для ручной перекачки плана */
    public function import()
    {
        /** ПЛАН */
        $apsv = ApsV::all();
        $unapsv = '';

        if ( $apsv->count() > 0 ) { // если записей в БД есть
            $unapsv = (string)$apsv->last()->unapsv;
        }

        // формируем запрос с фильтрами
        $args = (object)[
            'unapsv' => $unapsv,
        ];

        $arr_unapsv = ITERequest::sendRequest('_PLAN_GET_APSV', $args, ITERequest::METHOD_POST);

        /** ФАКТ */
        $fact = FactLoaded::where('year', '=', 2019)->get()->toArray();
        $factLoaded = [];
        foreach ($fact as $row) {
            $factLoaded[ $row['month'] ] = $row;
        }

        return view('import.import_aps_plan', [
            'unapsv' => $arr_unapsv,
            'factLoaded' => $factLoaded,
        ]);
    }
}