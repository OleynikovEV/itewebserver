<?php

namespace App\Http\Controllers\Import;

use App\Http\Controllers\Controller;
use App\Models\Aps;
use App\Models\ApsV;
use App\Models\FactLoaded;

class ImportITE extends Controller
{
    /** сохраняем план в APS из ITE */
    public function saveAPSPlan($unapsv)
    {
        $aps_tmp = ImportKube::plan_smenaAPS($unapsv);
        $cutShift = new CutShift();
        $cutShift->set($aps_tmp);
        $cutShift->trim();
        $cutShift->cutByShift();

        $date_new_plan = ImportKube::getAllocDate($unapsv);

        // если таблица ASP пуста, записать новые записи
        $first = Aps::first();

        if ( empty($first) ) { // таблица пуста и план можно просто записать в таблицу с порезкой по сменам;
            ImportKube::addToTable('aps', $cutShift->get() ); // записываем в таблицу
        } else { // кже есть план в таблице, перед сохранением необходимы предварительные действия
            if ($date_new_plan == null) {
                return false;
            }
            // отбираем задания начало и конец которых попал в эту метку для обрезки
            $old_plan = Aps::where([
                ['DATETIME_START', '<', $date_new_plan],
                ['DATETIME_FINISH', '>', $date_new_plan]
            ])->get();

            $oldShiftPlan = new CutShift();
            $oldShiftPlan->set( $old_plan->toArray() );
            $oldShiftPlan->cutByTimeStamp($date_new_plan);
            $arr_smena = $oldShiftPlan->get();

            // обрезанный план обновляем
            foreach ($arr_smena as $row) {
                $find = Aps::where('_id', $row['_id'])
                    ->update([
                        'DATETIME_START' => $row['DATETIME_START'],
                        'DATETIME_FINISH' => $row['DATETIME_FINISH'],
                        'DIF_M' => $row['DIF_M'],
                        'DIF_H' => $row['DIF_H'],
                        'SETUPTIME' => $row['SETUPTIME'],
                        'OLD' => $row['OLD'],
                    ]);
            }

            // удаляем старый план которые попал за метку актуальности нового плана
            Aps::where('DATETIME_START', '>=', $date_new_plan)->delete();

            // добавляем новый план
            ImportKube::addToTable('aps', $cutShift->get());
        }

        $apsv = new ApsV();
        $apsv->unapsv = $unapsv;
        $apsv->datestar = $date_new_plan;
        $apsv->save();

        return [true];
    }

    /** Сохраняем факт производства за месяц */
    public function saveFactProduction($year, $month)
    {
        $result = ImportKube::getFactProduction($year, $month);

        if (! empty($result) ) {
            $cutShift = new CutShift();
            $cutShift->set($result);
            $cutShift->cutByShift();

            $result = $cutShift->get();

            ImportKube::addToTable('facts', $result);

            $fact = new FactLoaded();
            $fact->year = (int)$year;
            $fact->month = (int)$month;
            $fact->save();

            return [ count($result) ];
        }

        return [false];
    }

    /** Сохраняем факт производства по дням */
    public function saveFactProductionByDay($year, $month, $day)
    {
        $result = ImportKube::getFactProductionByDay($year, $month, $day);

        if (! empty($result) ) {
            $cutShift = new CutShift();
            $cutShift->set($result);
            $cutShift->cutByShift();

            $result = $cutShift->get();

            ImportKube::addToTable('facts', $result);

            $fact = new FactLoaded();
            $fact->year = (int)$year;
            $fact->month = (int)$month;
            $fact->save();

            return count($result);
        }

        return false;
    }
}