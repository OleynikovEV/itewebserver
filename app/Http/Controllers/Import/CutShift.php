<?php

namespace App\Http\Controllers\Import;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;


/**
 *  INTERVAL_ID - действия
 *      4 - простой;
 *      3 - перерыв;
 *      2 - переналадка;
 *      1 - работа;
 */
class CutShift extends Controller
{
    protected $data = [];
    protected $timeStamp = null;

    public function set(array $data)
    {
        $this->data = $data;
    }

    /** устанавливаем время по которой разделили план */
    public function setTimeStamp($timeStamp)
    {
        $_timeStamp = 0;
        if ( is_string($timeStamp) ) {
            $_timeStamp = strtotime($timeStamp);
        } else {
            $_timeStamp = $timeStamp;
        }
        if ($_timeStamp < 1514764800) {
            $this->timeStamp = null;
            return false;
        }

        $this->timeStamp = $timeStamp;

        return $_timeStamp;
    }

    /** обрезаем план по дате */
    public function cutByTimeStamp($timeStamp = null)
    {
        if ($timeStamp == null && $this->timeStamp == null) {
            return false;
        }

        if ($timeStamp != null) {
            $result = $this->setTimeStamp($timeStamp);
            if ($result == false) return false;
        }

        $arr_smena = [];
        foreach ($this->data as $row) {
            $result = $this->cutPeriod($row, $this->timeStamp, 15, false);
            foreach ($result as $res) {
                $arr_smena[] = $res;
            }
        }
        $this->data = $arr_smena;
    }

    /** режим план по сменам */
    public function cutByShift()
    {
        $tmp = [];
        foreach ($this->data as $row) {
            $row = (array)$row;
            if ( $this->isCrossesShift($row['DATETIME_START'], $row['DATETIME_FINISH']) ) {
                foreach ($this->cutByShiftRecursive($row) as $item) {
                    $tmp[] = $item;
                }
            } else {
                $tmp[] = $row;
            }
        }

        $this->data = $tmp;
    }

    public function trim()
    {
        $this->data = $this->filter($this->data);
    }

    /** Обрезаем план по сменам */
    private function cutByShiftRecursive($data, $clear = true)
    {
        global $tmp;
        if ($clear) $tmp = [];

        $shift = $this->isCrossesShift($data['DATETIME_START'], $data['DATETIME_FINISH']);
        if ( $shift !== false ) {
            if ($shift == 1) {
                $date_stemp = $date_start = date('Y-m-d', strtotime($data['DATETIME_START']) );
                $timeStemp = $date_stemp . ' 19:30';
            } else {
                $time_start = date('H:i', strtotime($data['DATETIME_START']));

                if ($time_start < '07:30' && $time_start >= '00:01') {
                    $date_stemp = $date_start = date('Y-m-d', strtotime($data['DATETIME_START']) );
                    $timeStemp = $date_stemp . ' 07:30';
                } else {
                    $date_stemp = $date_start = date('Y-m-d', strtotime($data['DATETIME_START'] . ' +1 day'));
                    $timeStemp = $date_stemp . ' 07:30';
                }
            }
            $result = $this->cutPeriod($data, $timeStemp, 15, true);
            $tmp[] = $result[0];
            if ( isset($result[1]) ) {
                $this->cutByShiftRecursive($result[1], false);
            }
        } else {
            $tmp[] = $data;
        }

        return $tmp;
    }

    public function get()
    {
        return $this->data;
    }

    /** фильтруем данные */
    protected function filter($data)
    {
        $tmp = [];
        foreach ($data as $row) {
            $row = (array)$row;
            $id = $row['RCENTER_ID'];
            if (   $row['DATETIME_START'] == ''
                || $row['RCENTER_NAME'] == 'Контролер ОТК'
                || $row['RCENTER_NAME'] == 'Упаковка'
                || $row['RCENTER_NAME'] == 'Упаковка фибры'
                || $row['RCENTER_NAME'] == 'Выпуск' ) {
                continue;
            }

            $datefrom = strtotime($row['ALLOCDATE']);
            $datestart = strtotime($row['DATETIME_START']);
            $datefinish = strtotime($row['DATETIME_FINISH']);
            $lng = $datefinish - $datestart; // миллесек. / 60 = мин

            // INTERVAL_ID = 3 - перерыв
            // удаляем запланированные перерывы в начале плана
            if ( $datestart < $datefrom && ($row['INTERVAL_ID'] == 3 || $lng <= 60) ) {
                continue;
            }

//            unset($row['_id']);
            $tmp[$id][] = $row;
        }

        // удаляю перерывы в конце плана
        $new_tmp = [];
        foreach ($tmp as $id => $machine) {
            $tmp_machine = array_reverse($machine);

            foreach ($tmp_machine as $num => $row) {
//                if ($row['INTERVAL_ID'] != 3) break;
                if ($row['INTERVAL_ID'] == 3 || $row['INTERVAL_ID'] == 4) unset($tmp_machine[$num]);
            }

            $revers = array_reverse($tmp_machine);
            foreach ($revers as $row) {
                $new_tmp[] = $row;
            }
        }

        return $new_tmp;
    }

    /** пересекает ли задание начало смены? */
    protected function isCrossesShift($start, $finish)
    {
        $_start = strtotime($start);
        $date_start = date('Y-m-d', $_start);
        $time_start = date('H:i', $_start);

        $_finish = strtotime($finish);
        $date_finish = date('Y-m-d', $_finish);
        $time_finish = date('H:i', $_finish);
        $next_day_start = date('Y-m-d', strtotime($date_start . ' +1 day'));
        $prev_day_start = date('Y-m-d', strtotime($date_start . ' -1 day'));

//        echo 'Start - ' . $start . ' - ' . $_start . PHP_EOL;
//        echo 'Finish - ' . $finish . ' - ' . $_finish . PHP_EOL;

        /** START SHIFT */
        if ($time_start >= '00:01' && $time_start < '07:30') {
            $_time_start = '19:30';
            $smena_date_start = strtotime( $prev_day_start . ' 19:30' );
//            echo 'Shift start (prev) - ' . $prev_day_start . ' 19:30 '. $smena_date_start . PHP_EOL;
        } elseif ($time_start >= '07:30' && $time_start < '19:30') { // начало дневной смены
            $_time_start = '07:30';
            $smena_date_start = strtotime( $date_start . ' 07:30' );
//            echo 'Shift start - ' . $date_start . ' 07:30 '. $smena_date_start . PHP_EOL;
        } else {
            $_time_start = '19:30';
            $smena_date_start = strtotime( $date_start . ' 19:30' );
//            echo 'Shift start - ' . $date_start . ' 19:30 ' . $smena_date_start . PHP_EOL;
        }

        /** FINISH SHIFT */
//        if ($date_finish != $date_start && $time_start > '19:30') {
        if ($_time_start == '19:30' && $time_start >= '00:01' && $time_start < '07:30') {
            $smena_date_finish = strtotime( $date_start . ' 07:30' );
//            echo 'Shift finish - ' . $date_start . ' 07:30 ' . $smena_date_finish . PHP_EOL;
        } elseif ($_time_start == '19:30') {
            $smena_date_finish = strtotime( $next_day_start . ' 07:30' );
//            echo 'Shift finish (next) - ' . $next_day_start . ' 07:30 ' . $smena_date_finish . PHP_EOL;
        }else {
            $smena_date_finish = strtotime( $date_start . ' 19:30' );
//            echo 'Shift finish - ' . $date_start . ' 19:30 ' . $smena_date_finish . PHP_EOL;
        }
//        echo $_time_start . PHP_EOL;
//        echo $smena_date_start . PHP_EOL;
//        echo $smena_date_finish . PHP_EOL;

        if ($_start > $smena_date_start && $smena_date_finish < $_finish && $_time_start == '07:30') return 1;
        if ($_start > $smena_date_start && $smena_date_finish < $_finish && $_time_start == '19:30') return 2;

        // если смены попадают в точки начала и конца
        if ($date_finish != $date_start && $time_start == '19:30' && ($time_finish >= '07:30' || $time_finish >= '19:30')) return 2;
        if ($date_finish != $date_start && $time_start == '07:30' && ($time_finish >= '07:30' || $time_finish >= '19:30')) return 1;

        if ($time_start == '07:30' && $time_finish > '19:30') return 1;

        return false;
    }

    /** Разрезаем задание на две части в указанное время */
    protected function cutPeriod($data, $time_point, $ost_min = 15, $get_right = false)
    {
        $data = (array)$data;
        $date_start = strtotime($data['DATETIME_START']);
        $date_finish = strtotime($data['DATETIME_FINISH']);
        $int_time_point = strtotime($time_point);

        if (! isset($data['ID_CUT']) ) {
            $ID_CUT = $date_start;
        } else {
            $ID_CUT = $data['ID_CUT'];
        }

        if ( ($int_time_point < $date_start) || ($int_time_point > $date_finish) ) {
            return [ $data ];
        }

        // Если между началом задания (конецо задания) и началом нового плана разница в 15 мин,
        // все задание переносим в большую сторону
        if ($int_time_point - $date_start <= ($ost_min * 60)) { // большая часть задания попадает до
            if (! isset($data['OLD']) ) {
                $data['OLD'] = [];
            }
            $data['OLD']['DATETIME_START'] = $data['DATETIME_START'];
            $data['DATETIME_START'] = $time_point;

            return [ $data ];
        } elseif ($date_finish - $int_time_point <= ($ost_min * 60)) { // большая часть задания попадает после
            if (! isset($data['OLD']) ) {
                $data['OLD'] = [];
            }
            // если часть этого плана нужна, расскоментировать
            $data['OLD']['DATETIME_FINISH'] = $data['DATETIME_FINISH'];
            $data['DATETIME_FINISH'] = $time_point;

            return [ $data ];
        } else {
            $tmp1 = $data;

            // левая часть
            $tmp1['ID_CUT'] = $ID_CUT;
            $tmp1['DATETIME_FINISH'] = $time_point;
            $tmp1['DIF_M'] = ($int_time_point - $date_start) / 60;
            $tmp1['DIF_H'] = ($tmp1['DIF_M'] / 60 ) % 24 . '.' . $tmp1['DIF_M'] % 60;
            $tmp1['SETUPTIME'] = 0;

            if ( isset($data['KOL_PLAN']) ) {
                $production_for_min = $data['KOL_PLAN'] / $data['DIF_M'];
                $tmp1['KOL_PLAN'] = round($production_for_min * $tmp1['DIF_M'], 4);
            }

            if ( isset($data['KOL_FACT']) ) {
                $production_for_min = $data['KOL_FACT'] / $data['DIF_M'];
                $tmp1['KOL_FACT'] = round($production_for_min * $tmp1['DIF_M'], 4);
            }

            $tmp1['OLD'] = [
                'DATETIME_FINISH' => $data['DATETIME_FINISH'],
                'DIF_M' => $data['DIF_M'],
                'DIF_H' => $data['DIF_H'],
            ];

            if ( isset($data['SETUPTIME']) ) {
                $tmp1['OLD']['SETUPTIME'] = $data['SETUPTIME'];
            }

            if ( isset($data['KOL_PLAN']) ) {
                $tmp1['OLD']['KOL_PLAN'] = $data['KOL_PLAN'];
            }

            if ( isset($data['KOL_FACT']) ) {
                $tmp1['OLD']['KOL_FACT'] = $data['KOL_FACT'];
            }

            // правая часть
            if ($get_right) {
                $tmp2 = $data;
                $tmp2['ID_CUT'] = $ID_CUT;
                $tmp2['DATETIME_START'] = $time_point;
                $tmp2['DIF_M'] = $data['DIF_M'] - $tmp1['DIF_M'];
                $tmp2['DIF_H'] = ($tmp2['DIF_M'] / 60 ) % 24 . '.' . $tmp2['DIF_M'] % 60;

                if ( isset($data['KOL_PLAN']) ) {
                    $tmp2['KOL_PLAN'] = round($data['KOL_PLAN'] - $tmp1['KOL_PLAN'], 4);
                }

                if ( isset($data['KOL_FACT']) ) {
                    $tmp2['KOL_FACT'] = round($data['KOL_FACT'] - $tmp1['KOL_FACT'], 4);
                }

                $tmp2['OLD'] = [
                    'DATETIME_START' => $data['DATETIME_START'],
                    'DIF_M' => $data['DIF_M'],
                    'DIF_H' => $data['DIF_H'],
                ];

                if ( isset($data['KOL_PLAN']) ) {
                    $tmp2['OLD']['KOL_PLAN'] = $data['KOL_PLAN'];
                }

                if ( isset($data['KOL_FACT']) ) {
                    $tmp2['OLD']['KOL_FACT'] = $data['KOL_FACT'];
                }

                return [ $tmp1, $tmp2 ];
            }

            return [ $tmp1 ];
        }
    }
}