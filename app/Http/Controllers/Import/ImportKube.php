<?php
namespace App\Http\Controllers\Import;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Request as ITERequest;

class ImportKube extends Controller
{
    static function addToTable($table, $data)
    {
        foreach ($data as &$row) {
            $row = (array)$row;
        }

        DB::table($table)->insert($data);
    }

    /** Импортируем заказы по месяуац итоги (введено/запланировано/предъявлено) */
    static public function orders($year, $month)
    {
        $products = ['PW', 'PS', 'PF'];

        $tmp = [
            'year' => (int)$year,
            'month' => (int)$month,
            'KOL_ORDER' => 0, // заказов в этом месяце
            'KOL_FACT' => 0, // всего сдано
            'KOL_FACT_PRED' => 0, // сдано с прошлого месяца
        ];

        foreach ($products as $prod) {
            // формируем запрос с фильтрами
            $args = (object)[
                'prod_id' => $prod,
                'month' => $month,
                'year' => $year,
            ];

            $data = ITERequest::sendRequest('_ORDERS', $args, ITERequest::METHOD_POST);
            if (! empty($data) ) {
                foreach ($data as $row) {
                    if ($row->NRSS_SHOT == 'Отменен' || $row->NRSS_SHOT == 'Регистрац.') continue;
                    if ($row->KDMT == '_S3C') {
                        $tmp['KOL_ORDER'] += ($row->DIF == 0) ? $row->KOL : 0;
                    }
                    else {
                        $tmp['KOL_FACT'] += $row->KOL_FACT;
                        $tmp['KOL_FACT_PRED'] += ($row->DIF < 0) ? $row->KOL_FACT : 0;
                    }
                }
            }
        }

        DB::table('orders')
            ->where('year', $year)
            ->where('month', $month)
            ->delete();
        DB::table('orders')->insert($tmp);
    }

    /** Актуальные сменные задания */
    static public function plan_action_smena()
    {
        $data = ITERequest::sendRequest('_PLAN_ACTION_SMEN', new \stdClass, ITERequest::METHOD_POST);

        if (! empty($data) ) {
            foreach ($data as &$row) {
                $row = (array)$row;
            }

            DB::table('smena')->truncate();
            DB::table('smena')->insert($data);
        }
    }

    /** импорт данных из ASP актуального плана во временную таблицу */
    static public function plan_action_smenaAPS()
    {
        $data = ITERequest::sendRequest('_PLAN_ACTION_APS', new \stdClass, ITERequest::METHOD_POST);

        if (! empty($data) ) {
            foreach ($data as &$row) {
                $row = (array)$row;
            }

            DB::table('aps_tmp')->truncate();
            DB::table('aps_tmp')->insert($data);

            return $data;
        }

        return null;
    }

    /** импорт данных из ASP по номеру плана во временную таблицу */
    static public function plan_smenaAPS($num)
    {
        $args = (object)[
            'num' => (string)$num,
        ];
        $data = ITERequest::sendRequest('_PLAN_APS_BY_NUM', $args, ITERequest::METHOD_POST);

        if (! empty($data) ) {
            foreach ($data as &$row) {
                $row = (array)$row;
            }

            DB::table('aps_tmp')->truncate();
            DB::table('aps_tmp')->insert($data);

            return $data;
        }

        return null;
    }

    /** по версии плана, получаем дату и время начала плана */
    static public function getAllocDate($unapsv)
    {
        $args = (object)[
            'unapsv' => (string)$unapsv,
        ];
        $data = ITERequest::sendRequest('_PLAN_GET_ALLOCDATE', $args, ITERequest::METHOD_POST);

        if (! empty($data) ) {
            return $data[0]->ALLOCDATE;
        }

        return null;
    }

    static public function getFactProduction($year, $month)
    {
        $args = (object)[
            'month' => (string) (strlen($month) == 1) ? '0' . $month : $month,
            'year' => (string)$year,
        ];
        $data = ITERequest::sendRequest('_PRODUCTION', $args, ITERequest::METHOD_POST);

        if (! empty($data) ) {
            return $data;
        }

        return null;
    }

    static public function getFactProductionByDay($year, $month, $day)
    {
        $args = (object)[
            'day' => (string) (strlen($day) == 1) ? '0' . $day : $day,
            'month' => (string) (strlen($month) == 1) ? '0' . $month : $month,
            'year' => (string)$year,
        ];
        $data = ITERequest::sendRequest('_PRODUCTION', $args, ITERequest::METHOD_POST);

        if (! empty($data) ) {
            return $data;
        }

        return null;
    }
}