<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Request as ITERequest;
use App\Http\Products;
use App\Http\Tech;
use App\Http\OrderDone;
use Illuminate\Support\Facades\Cache;

class OrdersController extends Controller
{
    const CACHE_TIME = 7200;

    public function gerOredersByMonth(Request $request, $prod_id)
    {
        $year = $request->get('year') ?? date('Y');
        $month = $request->get('month') ?? date('n');
        $reload = $request->get('reload') ?? false;

        $key = '_ORDERS_' . $prod_id . $year . $month;

        if ($reload) {
            Cache::forget($key);
        }

        $data = Cache::get($key);
        if ( is_null($data) ) {
            // формируем запрос с фильтрами
            $args = (object)[
                'prod_id' => $prod_id,
                'month' => $month,
                'year' => $year,
            ];

            $data = ITERequest::sendRequest('_ORDERS', $args, ITERequest::METHOD_POST);
            Cache::put($key, $data);
        }

        $tmp = [];

        if (! empty($data) ) {
//            dd($data);
            foreach ($data as $row) {

                if ($row->NRSS_SHOT == 'Отменен' || $row->NRSS_SHOT == 'Регистрац.') continue;
                // инициализируем ID
                $firm_id = $row->ORG_NAME;
                $num = $row->NDM . '/' . $row->NPP;
                $lot = $row->NDM . '/' . $row->KDS1;
                $id = $num;

                // инфа по фирме/менеджеру
                if (! isset($tmp[$firm_id]) ) {
                    $tmp[$firm_id]['info'] = [
                        'FIO_OTV' => $row->FIO_OTV,
                        'ORG_NAME' => $row->ORG_NAME,
                    ];
                }

                $date = ( strlen($row->D_PRPR2) > 0 ) ? $row->D_PRPR2 : $row->DATE_DIR;
                // инфа по заказам
                if (! isset($tmp[$firm_id]['orders'][$id]) ) {
                    $tmp[$firm_id]['orders'][$id] = $row;
                    $tmp[$firm_id]['orders'][$id]->INCORRECT_FACT = false;
                    $tmp[$firm_id]['orders'][$id]->NUM = $num;
                    $tmp[$firm_id]['orders'][$id]->LOT = $lot;
                    $tmp[$firm_id]['orders'][$id]->DDM = date('d-m-y', strtotime($row->DDM) );
                    $m = (int)date('n', strtotime($date) );
                    $tmp[$firm_id]['orders'][$id]->PLAN_MONTH_FUTURE = ( strlen($date) > 0 && $m != $month ) ? true : false;
                    $tmp[$firm_id]['orders'][$id]->PLAN = ($date == '') ? '' : date('d-m-y', strtotime($date) );
                    $tmp[$firm_id]['orders'][$id]->FINISHTIME = ($date == '') ? '' : date('Y-m-d', strtotime($date) );
                    $tmp[$firm_id]['orders'][$id]->AMOUNT = 0;

                    // вытаскиваем из json характеристики продукции
                    if (is_json($row->TECH) && validation_string($row->TECH_STR, 30)) {
                        $tech = Tech::getTech($row->TECH, $row->TECH_STR);
                        foreach ($tech as $key => $val) {
                            $tmp[$firm_id]['orders'][$id]->{$key} = $val;

                        }
                    }
                }
                $tmp[$firm_id]['orders'][$id]->PREV = ($row->DIF < 0) ? '-' : '+'; // Признак переходящие -/+

                if ( $row->KDMT == '_SKC' ) {
                    $tmp[$firm_id]['orders'][$id]->AMOUNT += ($row->TOTAL_FACT > 0) ? $row->TOTAL_FACT : $row->KOL_FACT;
                }

                $tmp[$firm_id]['orders'][$id]->DONE = OrderDone::done(
                    $row->NRSS_SHOT,
                    $row->PR_IZG_F,
                    $row->PR_DO,
                    $tmp[$firm_id]['orders'][$id]->AMOUNT,
                    $row->KOL
                );

                if ( $row->KDMT== '_SKC' ) {
                    $tmp[$firm_id]['orders'][$id]->AMOUNT += ($row->TOTAL_FACT > 0) ? $row->TOTAL_FACT : $row->KOL_FACT;
                    $tmp[$firm_id]['orders'][$id]->TOTAL_FACT = $row->TOTAL_FACT;
                    // проверяем как предъявлен продукт - если было отклонение мин в 5%, ошибка
                    $kol_procent = round($tmp[$firm_id]['orders'][$id]->KOL*5/100, 2);
                    $difference = $tmp[$firm_id]['orders'][$id]->KOL - $row->TOTAL_FACT;
                    $difference = ($difference < 0) ? $difference * -1 : $difference;
                    if ( $row->TOTAL_FACT != 0 && $tmp[$firm_id]['orders'][$id]->DONE == 'Вып' && $difference >= $kol_procent ) {
                        $tmp[$firm_id]['orders'][$id]->INCORRECT_FACT = true;
                    }
                }

                if (! isset($tmp[$firm_id]['orders'][$id]->CAL) ) {
                    $tmp[$firm_id]['orders'][$id]->CAL = [];
                }

                $order_month = (strlen($date) == 0) ? $month : explode('-', $date)[1];
                if ($row->KDMT == '_S3C') {
                    if (! isset($tmp[$firm_id]['orders'][$id]->CAL['_S3C']) ) {
                        $tmp[$firm_id]['orders'][$id]->CAL['_S3C'][$date] = ($order_month == $month) ? $row->KOL : 0;
                    }

                    // желаемая дата
                    if (! isset($tmp[$firm_id]['orders'][$id]->CAL['PREFERABLE_DATE'])) {
                      $tmp[$firm_id]['orders'][$id]->CAL['PREFERABLE_DATE'][$row->PREFERABLE_DATE] = ($order_month == $month) ? $row->KOL : 0;
                    }
                } else {
                    if (! isset($tmp[$firm_id]['orders'][$id]->CAL['_SKC'][$row->DATE_DIR]) ) {
                        $tmp[$firm_id]['orders'][$id]->CAL['_SKC'][$row->DATE_DIR] = 0;
                    }
                    $tmp[$firm_id]['orders'][$id]->CAL['_SKC'][$row->DATE_DIR] += $row->KOL_FACT;
                    if (! isset($tmp[$firm_id]['orders'][$id]->CAL['_S3C']) ) {
                        $tmp[$firm_id]['orders'][$id]->CAL['_S3C'][$row->D_PRPR2] = ($order_month == $month) ? $row->KOL : 0;
                    }
                  // желаемая дата
                  if (! isset($tmp[$firm_id]['orders'][$id]->CAL['PREFERABLE_DATE'])) {
                    $tmp[$firm_id]['orders'][$id]->CAL['PREFERABLE_DATE'][$row->PREFERABLE_DATE] = ($order_month == $month) ? $row->KOL : 0;
                  }
                }
            }
            ksort($tmp);
        }

        $calendar = getMonthList($year, $month);

        return view('orders.orders-by-month', [
            'title' => 'Заказы - ' . Products::getProductName($prod_id),
            'empty' => 'Заказы за выбранный период отсутствуют',
            'showCalendar' => true,
            'date_time' => sprintf('%s-%s-01', $year, $month),
            'month' => $month,
            'data' => $tmp,
            'calendar' => $calendar,
            'amountDays' => count($calendar),
        ]);
    }
}
