<?php

namespace App\Http\Models;

use App\Models\Fact;

class FactModel
{
    const FILTER_YEAR = 'DATE_YEAR';
    const FILTER_MONTH = 'DATE_MONTH';
    const FILTER_DAY = ['DATETIME_START', 'DATETIME_FINISH'];

    protected $filter = self::FILTER_YEAR;
    protected $filter_data = null;

    protected $fact = null;
    protected $result = null;

//    protected $groupBy = 'RCENTER_ID';
//    protected $groupByName = 'RCENTER_NAME';

    protected $groupBy = 'GUILD_ID';
    protected $groupByName = 'GUILD';

    public function __construct()
    {
        $this->fact = new Fact();
        $this->setFilter(self::FILTER_YEAR, date('Y'));
    }

    public function setFilter($filter_name, $data)
    {
        $this->filter = $filter_name;
        $this->filter_data = $data;

        switch ($filter_name) {
            case self::FILTER_YEAR:
                $this->result = $this->fact->where(self::FILTER_YEAR, '=', (int)$data);
                break;
            case self::FILTER_MONTH:
                $this->result = $this->fact->where([
                    [self::FILTER_YEAR, '=', (int)$data[0]],
                    [self::FILTER_MONTH, '=', (int)$data[1]]
                ]);
                break;
            case self::FILTER_DAY:
                $date = $data[0] . '-' . $data[1] . '-' . (($data[2] < 10) ? ('0' . $data[2]) : $data[2]);
                $start = $date . ' 07:30';
                $finish =  date( 'Y-m-d', strtotime($date . ' +1 day' ) ) . ' 07:30';

                $this->result = $this->fact->where([
                    [self::FILTER_DAY[0], '>=', $start],
                    [self::FILTER_DAY[1], '<=', $finish],
                ]);
                break;
        }
    }

    public function getSumFact()
    {
//        $start = $this->filter_shift . ' 07:30';
//        $finish =  date( 'Y-m-d', strtotime($this->filter_shift . ' +1 day' ) ) . ' 07:30';
//
//        $result = $this->fact->where([
//            ['DATETIME_START', '>=', $start],
//            ['DATETIME_FINISH', '<=', $finish],
//        ]);

//        $result = $this->fact->where([
//            ['DATE_YEAR', '=', $this->filter_year],
//            ['DATE_MONTH', '=', $this->filter_month],
//        ]);

//        $this->result = $this->fact->where([
//            ['DATE_YEAR', '=', 2019],
//            ['DATE_MONTH', '=', 9],
//        ]);
        $data = [];

        foreach ($this->result->get() as $row) {
            $id =  $row[ $this->groupBy ];
            if(! isset($data[$id]) ) {
                $data[$id] = [
                    'ID' => $id,
                    'ID_NAME' => $this->groupBy,
                    'NAME' => $row[ $this->groupByName ],
                    'KOL_DEFECT' => 0,
                    'KOL_FACT' => 0,
                ];
            }

            // если нет норм
            if ($row['VNRM'] == 0) {
                $data['NONRM'] = [
                    'DATE_YEAR' => $row->DATE_YEAR,
                    'DATE_MONTH' => $row->DATE_MONTH,
                    'DATE_DAY' => $row->DATE_DAY,
                    'PROD_GROUP' => $row->PROD_GROUP,
                    'RESOURCE_CODE' => $row->RESOURCE_CODE,
                    'KZAJNPP' => $row->KZAJNPP,
                    'KMAT' => $row->KMAT,
                    'KMAT' => $row->KMAT,
                    'TECH_PROCESS' => $row->TECH_PROCESS,
                    'RCENTER_ID' => $row->RCENTER_ID,
                    'KOL_TOTAL' => $row->KOL_TOTAL,
                    'VNRM' => $row->VNRM,
                ];
            }

//            $data[$id]['KOL_FACT'] += $row['KOL_TOTAL'];
            $data[$id]['KOL_FACT'] += ($row['EDI'] == 168)
                ? $row['KOL_TOTAL']
                : (($row['VNRM'] == 0)
                    ? 0
                    : $row['KOL_TOTAL'] / $row['VNRM']);

            $data[$id]['KOL_DEFECT'] += ($row['NEDI'] == 168)
                ? $row['KOL_DEFECT']
                : (($row['VNRM'] == 0)
                    ? 0
                    : $row['KOL_DEFECT'] / $row['VNRM']);
        }

//        sortByKeys($data, ['NAME' => 'asc']);

        return $data;
    }
}