<?php

namespace App\Http\Models;

use App\Models\Aps;

class ApsModel
{
    const FILTER_YEAR = 'YEAR_START';
    const FILTER_MONTH = 'MONTH_START';
    const FILTER_DAY = ['DATETIME_START', 'DATETIME_FINISH'];

    protected $filter = self::FILTER_YEAR;
    protected $filter_data = null;

//    protected $groupBy = 'RCENTER_ID';
//    protected $groupByName = 'RCENTER_NAME';

    protected $groupBy = 'GUILD_ID';
    protected $groupByName = 'GUILD';

    protected $aps = null;
    protected $result = null;

    public function __construct($filter_name = null, $date = null)
    {
        $this->aps = new Aps();

        if ($filter_name != null && $date != null) {
            $this->setFilter($filter_name, $date);
        }
    }

    public function setFilter($filter_name, $data)
    {
        $this->filter = $filter_name;
        $this->filter_data = $data;

        switch ($filter_name) {
            case self::FILTER_YEAR:
                $this->result = $this->aps->where(self::FILTER_YEAR, '=', (int)$data);
                break;
            case self::FILTER_MONTH:
                $this->result = $this->aps->where([
                    [self::FILTER_YEAR, '=', (int)$data[0]],
                    [self::FILTER_MONTH, '=', (int)$data[1]]
                ]);
                break;
            case self::FILTER_DAY:
                $date = $data[0] . '-' . $data[1] . '-' . (($data[2] < 10) ? ('0' . $data[2]) : $data[2]);
                $start = $date . ' 07:30';
                $finish =  date( 'Y-m-d', strtotime($date . ' +1 day' ) ) . ' 07:30';

                $this->result = $this->aps->where([
                    [self::FILTER_DAY[0], '>=', $start],
                    [self::FILTER_DAY[1], '<=', $finish],
                ]);
                break;
        }
    }

    public function getSumPlan()
    {
//        $start = $this->filter_shift . ' 07:30';
//        $finish =  date( 'Y-m-d', strtotime($this->filter_shift . ' +1 day' ) ) . ' 07:30';

//        $result = $this->aps->where([
//            ['DATETIME_START', '>=', $start],
//            ['DATETIME_FINISH', '<=', $finish],
//        ]);

//        $result = $this->aps->where([
//            ['YEAR_START', '=', $this->filter_year],
//            ['MONTH_START', '=', $this->filter_month],
//        ]);

//        $this->result = $this->aps->where([
//            ['YEAR_START', '=', 2020]
//        ]);

        $data = [];
        foreach ($this->result->get() as $row) {
            $id =  $row[ $this->groupBy ];
            if(! isset($data[$id]) ) {
                $data[$id] = [
                    'ID' => $id,
                    'ID_NAME' => $this->groupBy,
                    'NAME' => $row[ $this->groupByName ],
                    'KOL_PLAN' => 0,
                ];
            }
            $data[$id]['KOL_PLAN'] += $row['KOL_PLAN'];
        }

//        sortByKeys($data, ['NAME' => 'asc']);

        return $data;
    }
}