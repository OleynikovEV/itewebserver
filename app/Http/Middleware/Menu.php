<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\View;

class Menu
{
    public function handle($request, Closure $next)
    {
        $arr = include( base_path('routes/menu.php') );
        $iterator = new \RecursiveArrayIterator($arr);
        $menu = '';
        iterator_apply( $iterator, 'BuildMenu', array($iterator, &$menu) );
        View::share('menu', $menu);

        return $next($request);
    }
}
