<?php

namespace App\Http;

class OrderDone
{
    public static function done($NRSS, $PR_IZG_F, $PR_DO, $AMOUNT, $KOL): string
    {
        // признак выполнения заказа
        if ( $NRSS == 'Выполнен' || $PR_IZG_F == '+' || $PR_DO == 3 || $AMOUNT >= $KOL ) {
            return 'Вып';
        }
        elseif ( $NRSS == 'ВключВПлан' ) {
            return 'План';
        }
        else {
            return $NRSS;
        }
    }
}