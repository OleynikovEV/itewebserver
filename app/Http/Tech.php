<?php

namespace App\Http;

class Tech
{
    public static function getTech($tech, $tech_str)
    {
        $tech = json_decode($tech, true); // из json строки получаем объект

        $obj = new \stdClass();
        $obj->DIAM = $tech['ДИА1'] ?? null; // диаметр
        $obj->COVER = $tech['ПОКР'] ?? null; // покрытие

        // преобразуем строку вывода характеристик
        $TECH1 = $tech_str; // маска формирования строки характеристик
        $TECH1 = str_replace('iif({ПОКР}=="оц","Zn ",(iif({ПОКР}=="","",{ПОКР}+" ")))', '{ПОКР}', $TECH1);
        $TECH1 = preg_replace(['/\"/', '/\&/'], '', $TECH1); // удаляем ненужные символы
        $TECH1 = explode('+', $TECH1); // преобразуем в массив

        $newTech = []; // новая строка для формирования характеристики
        $i = 0;
        foreach ($TECH1 as $item) {
            if ( stripos($item, '}') == true ) { // элемент с переменной
                $item = preg_replace(['/\{/', '/\}/'], '', $item); // удаляем { } признаки переменной
                $find = array_filter($tech, function($val, $key) use($item){ // поиск значения для переменной
                    return ($key == $item && trim($val) != '');
                }, ARRAY_FILTER_USE_BOTH);
                if ( count($find) > 0) { // если значение есть, формируем строку
                    $key = key($find);
                    $newTech[$key] = $find[$key];
                }
            } elseif ( trim($item) <> '' ) {
                if ($i == 0 ) $newTech['PROD_NAME'] = $item;
                elseif ( ($i == 1 && $item != '/') || $i == 2 ) {
                    $gost = explode('(', $item);
                    $newTech['GOST'] = $gost[0];
                    if ( isset($gost[1]) ) {
                        $newTech['GOST1'] = '(' . $gost[1];
                    }
                }
                elseif ($item != '/') $newTech[$i] = $item;
                $i++;
            }
        }
        $obj->TECH1 = $newTech;

        return $obj;
    }
}
