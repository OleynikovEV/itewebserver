<?php

namespace App\Http;

class Request
{
    const METHOD_POST = 'POST';
    const METHOD_GET = 'GET';

    public static function sendRequest(string $function, object $args, $method = self::METHOD_POST)
    {
        $ip_ite = env('IP_ITE', null);

        if ($ip_ite == null) {
            die('В файле .env отсутствует IP_ITE');
        }

        $client = new \GuzzleHttp\Client([
            'base_uri' => 'http://'. $ip_ite .'/ws/webservice.asmx/'
        ]);

        $options = [
            'json' => [
                'calcId' => $function,
                'args' => json_encode($args)
            ]
        ];

        try {
            $response = $client->request($method, 'Execute', $options);
        } catch (\Exception $e) {
            die($e->getMessage());
        }

        if ($response->getStatusCode() != 200) {
            return null;
        } else {
            $body = $response->getBody(); // вытаскиваем дынные из запроса
            $data = json_decode($body); // преобразуем в объект
            return json_decode($data->d); // вытаскиваем данные повторно из объекта
        }
    }
}
