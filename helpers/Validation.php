<?php
    /**
     *  валидация строки
     */
    function validation_string($input_string, int $min_lng = null, int $max_lng = null) : bool
    {
        if ( !is_string($input_string) ) return false;

        $lng = strlen($input_string);

        if ( $min_lng !== null && $min_lng > $lng ) return false;

        if ( $max_lng !== null && $lng > $max_lng ) return false;

        return true;
    }

    /**
     *  валидация числа
     */
    function validation_number($input_number, bool $strict = true) : bool
    {
        if ( $strict ) {
            return (gettype($input_number) == 'integer' || gettype($input_number) == 'double');
        } else {
            return is_numeric( $input_number );
        }
    }

    /**
     *  валидация пароля
     */
    function validation_password($password, int $min_lng = 3, int $max_lng = null) : bool
    {
        if ( empty($password) ) return false;

        // проверка на наличие пробелов и табуляций
        if ( !ctype_graph($password) ) return false;

        if ( !validation_string($password, $min_lng, $max_lng) ) return false;

        // at least 1 Number
        //if ( ! preg_match("#[0-9]+#", $password) ) return false;

        // at last 1 capital letter
        //if ( ! preg_match("#[A-Z]+#", $password) ) return false;

        // at last 1 lowercase letter
        //if ( ! preg_match("#[a-z]+#", $password) ) return false;

        return true;
    }

    /**
     * валидация даты
     */
    function validation_date($date) : bool
    {
        return !strtotime($date) ? false : true;
    }

    /**
     *  валидация email
     *  doc: https://www.php.net/manual/ru/filter.examples.validation.php
     */
    function validation_email(string $email) : bool
    {
        if ( !filter_var($email, FILTER_VALIDATE_EMAIL) ) return false;

        return true;
    }
	
    /**
     *  строгое сравнение двух значений
     */
    function validation_identical($data1, $data2) : bool
    {
        return ( $data1 === $data2 );
    }

    /**
     *  валидация URL адреса
     */
    function validation_url(string $url): bool
    {
        $parse_url = parse_url($url);

        if ( isset($parse_url['host']) ) {
            $url = $parse_url['host'];
        } elseif ( isset($parse_url['path']) ) {
            $url = $parse_url['path'];
        } else {
            return false;
        }

        if ( preg_match('/^\w+\.[\w\.]+(\/.*)?$/', $url) ) {
            return true;
        } else {
            return false;
         }
    }
	
	/**
	* валидация json
	*/
	function is_json($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

    /**
     *  проверка на пустое значение в переменной
     */
    function isEmpty($data) : bool
    {
        if ( empty($data) ) return true;

        if ( is_null($data) ) return true;

        if ( gettype($data) == 'string' && trim($data) == '' ) return true;

        if ( (gettype($data) == 'array' || gettype($data) == 'object') && count($data) == 0 ) return true;

        return false;
    }

    /**
     *  проверка является ли строка типа date или datetime
     */
    function is_datetime($data)
    {
        if (! $data) {
            return false;
        }

        try {
            new \DateTime($data);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }