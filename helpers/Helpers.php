<?php
/** PATH */

/**
 * DEBUG
 * вывод данных в удобочитаемом виде
 * $data - данные для отображения
 * $die - выход из скрипта после выполнения функции
 */
function prn($data, bool $die = false)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';

    if ( $die ) die();
}

function vrd($data, bool $die = false)
{
    echo '<pre>';
    var_dump($data);
    echo '</pre>';

    if ( $die ) die();
}

/** ARRAY/OBJECT */

/**
 * Выборка данных из массива
 * $items - массив или объект
 * $func - анонимная функция
 *
 * Как пользоваться:
 * $emails = map($users, function($user) {
 * 		return $user['email'];
 * });
 */
function map($items, $func)
{
    $results = [];

    foreach ( $items as $item ) {
        $results[] = $func($item);
    }

    return $results;
}

/**
 * Фильтрация масива (оставить данные по условию)
 * $items - массив или объект
 * $func - анонимная функция
 *
 * Как пользоваться:
 * $emails = filter($users, function($user) {
 * 		return $user['banned']; // если $user['banned'] == true
 * });
 */
function filter($items, $func)
{
    $results = [];

    foreach ( $items as $item ) {
        if ( $func($item) ) {
            $results[] = $item;
        }
    }

    return $results;
}

/**
 * Фильтрация масива (отсечь данные по условию)
 * $items - массив или объект
 * $func - анонимная функция
 *
 * Как пользоваться:
 * $emails = reject($users, function($user) {
 * 		return $user['banned']; // если $user['banned'] == false
 * });
 */
function reject($items, $func)
{
    $results = [];

    foreach ( $items as $item ) {
        if ( !$func($item) ) {
            $results[] = $item;
        }
    }

    return $results;
}

/** DATE/TIME */
// высчитываем разницу между датами
function diffDate($start, $end = null)
{
    $date = new DateTime($start);
    if ( !$end ) $end = new DateTime();
    else $end = new DateTime($end);

    return $date->diff($end);
}

/** OTHER */
// работа с суфиксом
function declOfNum($val, array $suffix)
{
    $mod = $val % 10;

    if ( $mod == 0 || $val == 0 ) {
        return $suffix[2];
    }

    if ( $mod == 1 && $val != 11 ) {
        return $suffix[0];
    }

    if ( $mod >= 2 && $mod <= 4 && ($val < 12 || $val > 14) ) {
        return $suffix[1];
    }

    if ( $mod >= 5 && $mod <= 9 || ($val >= 5 && $val <= 20) ) {
        return $suffix[2];
    }
}

function get_month_name($mNum) {
    $month = array(
        1 => "январь",
        "01" => "январь",
        2 => "февраль",
        "02" => "февраль",
        3 => "март",
        "03" => "март",
        4 => "апрель",
        "04" => "апрель",
        5 => "май",
        "05" => "май",
        6 => "июнь",
        "06" => "июнь",
        7 => "июль",
        "07" => "июль",
        8 => "август",
        "08" => "август",
        9 => "сентябрь",
        "09" => "сентябрь",
        10 => "октябрь",
        11 => "ноябрь",
        12 => "декабрь",
    );

    if($mNum <= 12 && $mNum >=1)
        return $month[$mNum];
    else return null;
}

function get_month_short($mNum) {
    $month = array(
        1 => "янв",
        "01" => "янв",
        2 => "фев",
        "02" => "фев",
        3 => "март",
        "03" => "март",
        4 => "апр",
        "04" => "апр",
        5 => "май",
        "05" => "май",
        6 => "июнь",
        "06" => "июнь",
        7 => "июль",
        "07" => "июль",
        8 => "авг",
        "08" => "авг",
        9 => "сен",
        "09" => "сен",
        10 => "окт",
        11 => "нояб",
        12 => "дек",
    );

    if($mNum <= 12 && $mNum >=1)
        return $month[$mNum];
    else return null;
}

function addDateToCalendar($date, &$calendar)
{
    if (! isset($calendar[$date]) ) {
        if ( $date == null ) {
            $calendar[$date] = [
                'year' => null,
                'month' => null,
                'day' => null,
            ];
        } else {
            $expDate = explode('-', $date);
            $calendar[$date] = [
                'year' => $expDate[0],
                'month' => $expDate[1],
                'day' => $expDate[2],
            ];
        }
    }
}

function getMonthList1($calendar)
{
    foreach ($calendar as $day) {
        if (! isset($month[ $day['month'] ]) ) {
            $month[ $day['month'] ]['name'] = get_month_short( $day['month'] );
            $month[ $day['month'] ]['colspan'] = 0;
            $month[ $day['month'] ]['num'] = $day['month'];
        }
        //$month[ $day['month'] ]['colspan'] += ($day['month'] == '') ? 0 : 1;
        $month[ $day['month'] ]['colspan'] += 1;
    }
    return $month;
}

function getMonthList($year, $month)
{
    $calendar = [ '' => '' ];
    $d = new \DateTime( $year . '-' . $month . '-01' );
    $lastDay = $d->format( 't' );
    for ($day = 1; $day <= $lastDay; $day++) {
        $calendar[ date('y-m-d', strtotime( sprintf('%s-%s-%s', $year, $month, $day ) ) ) ] = $day;
    }

    return $calendar;
}

function BuildMenu($iterator, &$menu)
{
    while ( $iterator->valid() ) {
        if ( $iterator->hasChildren() ) {
            $menu .= '<ul>';
            BuildMenu( $iterator->getChildren(), $menu );
            $menu .= '</ul>';
        } else {
            if ( $iterator->current() == '' || $iterator->current() == 'parent' ) {
                $class = ($iterator->current() == 'parent') ? 'class="parent"' : '';
                $menu .= '<li><span '. $class . '>' . $iterator->key() . '</span>';
            } else {
                $menu .= '<li><a href="' . $iterator->current() . '">' . $iterator->key() . '</a>';
            }
        }

        $iterator->next();
    }
}

function sortByKeys(&$data, $sort, $arr = true)
{
    if ($arr) { sortByKeysArray($data, $sort); }
    else { sortByKeysObject($data, $sort); }
}

function sortByKeysObject(&$data, $sort)
{
    // $sort = array('dateStart_print' => 'desc', 'machine_strand' => 'asc', 'priority' => 'asc', 'id_strand_r' => 'asc', 'id_strand_operation_info' => 'asc')
    usort($data, function ($a, $b) use ($sort) {
        $result = 0;
        foreach ($sort as $key => $value) {
            if ($a->$key == $b->$key) continue;
            $result = ($a->$key < $b->$key) ? -1 : 1;
            if ($value == 'desc') $result = -$result;
            break;
        }
        return $result;
    });
}

function sortByKeysArray(&$data, $sort)
{
    // $sort = array('dateStart_print' => 'desc', 'machine_strand' => 'asc', 'priority' => 'asc', 'id_strand_r' => 'asc', 'id_strand_operation_info' => 'asc')
    usort($data, function ($a, $b) use ($sort) {
        $result = 0;
        foreach ($sort as $key => $value) {
            if ($a[$key] == $b[$key]) continue;
            $result = ($a[$key] < $b[$key]) ? -1 : 1;
            if ($value == 'desc') $result = -$result;
            break;
        }
        return $result;
    });
}

function sortByDatetime(&$data, $sort)
{
    // $sort = array('dateStart_print' => 'desc', 'machine_strand' => 'asc', 'priority' => 'asc', 'id_strand_r' => 'asc', 'id_strand_operation_info' => 'asc')
    usort($data, function ($a, $b) use ($sort) {
        $result = 0;
        foreach ($sort as $key => $value) {
            $tmp_a = (is_datetime($a[$key])) ? strtotime($a[$key]) : $a[$key];
            $tmp_b = (is_datetime($b[$key])) ? strtotime($b[$key]) : $b[$key];
            if ($tmp_a == $tmp_b) continue;
            $result = ($tmp_a < $tmp_b) ? -1 : 1;
            if ($value == 'desc') $result = -$result;
            break;
        }
        return $result;
    });
}